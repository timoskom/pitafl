package com.timkom.pitafl

import android.app.Activity
import android.content.Context
import com.google.android.ump.ConsentDebugSettings
import com.google.android.ump.ConsentForm
import com.google.android.ump.ConsentInformation
import com.google.android.ump.ConsentRequestParameters
import com.google.android.ump.UserMessagingPlatform
import com.timkom.pitafl.activity.MainActivity

private class AdsConsentManagerImpl(context: Context) : AdsConsentManager {

    private val consentInformation: ConsentInformation =
        UserMessagingPlatform.getConsentInformation(context)

    override val canRequestAds: Boolean
        get() = consentInformation.canRequestAds()

    override val isPrivacyOptionsRequired: Boolean
        get() =
            consentInformation.privacyOptionsRequirementStatus ==
                    ConsentInformation.PrivacyOptionsRequirementStatus.REQUIRED

    override fun gatherConsent(
        activity: Activity,
        onConsentGatheringCompleteListener: AdsConsentManager.OnConsentGatheringCompleteListener,
    ) {
        //val debugSettings =  ConsentDebugSettings.Builder(activity)
            // .setDebugGeography(ConsentDebugSettings.DebugGeography.DEBUG_GEOGRAPHY_EEA)
            // .addTestDeviceHashedId(MainActivity.TEST_DEVICE_HASHED_ID)
            //.build()

        val params = ConsentRequestParameters.Builder()./*setConsentDebugSettings(debugSettings).*/build()

        consentInformation.requestConsentInfoUpdate(
            activity,
            params,
            {
                loadAndShowConsentFormIfRequired(activity, onConsentGatheringCompleteListener)
            },
            { requestConsentError ->
                onConsentGatheringCompleteListener.consentGatheringComplete(requestConsentError)
            },
        )
    }

    private fun loadAndShowConsentFormIfRequired(
        activity: Activity,
        onConsentGatheringCompleteListener: AdsConsentManager.OnConsentGatheringCompleteListener,
    ) {
        UserMessagingPlatform.loadAndShowConsentFormIfRequired(activity) { formError ->
            onConsentGatheringCompleteListener.consentGatheringComplete(formError)
        }
    }

    override fun showPrivacyOptionsForm(
        activity: MainActivity,
        onConsentFormDismissedListener: ConsentForm.OnConsentFormDismissedListener,
    ) {
        UserMessagingPlatform.showPrivacyOptionsForm(activity, onConsentFormDismissedListener)
    }

}

internal fun AdsConsentManager.Companion.provideInstance(context: Context): AdsConsentManager {
    return AdsConsentManagerImpl(context)
}