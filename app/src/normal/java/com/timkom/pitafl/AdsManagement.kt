package com.timkom.pitafl

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowMetrics
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.annotation.IdRes
import androidx.annotation.UiThread
import androidx.lifecycle.Lifecycle
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds
import com.timkom.pitafl.activity.AboutActivity
import com.timkom.pitafl.activity.MainActivity
import com.timkom.pitafl.activity.ProductsActivity
import com.timkom.pitafl.adapter.AdSupportingAdapter
import com.timkom.pitafl.adapter.AdSupportingAdapter.Item
import com.timkom.pitafl.adapter.AdSupportingAdapter.ViewHolder
import com.timkom.pitafl.adapter.ProductsAdapter
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks
import com.timkom.pitafl.task.coroutine.base.Result
import kotlin.reflect.KClass

@UiThread
private fun ComponentActivity.handleAdsState(id: Int, adViewMethod: AdView.() -> Unit) {
    val view: View = findViewById(id)
    if (view is AdView) adViewMethod.invoke(view)
}

@UiThread
fun ComponentActivity.handleAdsForLifecycleState(event: Lifecycle.Event) {
    when (event) {
        Lifecycle.Event.ON_CREATE -> {
            if (adViewsIds?.isNotEmpty() == true) adViewsIds = mutableListOf()
        }
        Lifecycle.Event.ON_START -> {}
        Lifecycle.Event.ON_RESUME -> adViewsIds?.forEach { handleAdsState(it, AdView::resume) }
        Lifecycle.Event.ON_PAUSE -> adViewsIds?.forEach { handleAdsState(it, AdView::pause) }
        Lifecycle.Event.ON_STOP -> {}
        Lifecycle.Event.ON_DESTROY -> adViewsIds?.forEach {
            handleAdsState(it, AdView::destroy)
            adViewsIds?.clear()
        }
        Lifecycle.Event.ON_ANY -> {}
    }
}

private val adViewsIdsMap: MutableMap<KClass<*>, MutableList<Int>?> = mutableMapOf()

private inline var <reified T : ComponentActivity> T.adViewsIds: MutableList<Int>?
    get() = adViewsIdsMap[T::class]
    set(value) {
        adViewsIdsMap[T::class] = value
    }

// Get the ad size with screen width.
@get:UiThread
private val ComponentActivity.adSize: AdSize
    get() {
        val displayMetrics = resources.displayMetrics
        val adWidthPixels =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val windowMetrics: WindowMetrics = this.windowManager.currentWindowMetrics
                windowMetrics.bounds.width()
            } else {
                displayMetrics.widthPixels
            }
        val density = displayMetrics.density
        val adWidth = (adWidthPixels / density).toInt()
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth)
    }

@UiThread
private inline fun <reified T: ComponentActivity> T.addBannerAd(
    @IdRes containerIdRes: Int,
    admobId: String
) {
    try {
        val adViewContainer: FrameLayout? = findViewById(containerIdRes)
        adViewContainer?.let {
            val adView = AdView(this)
            val id = View.generateViewId()
            adView.id = id
            adView.adUnitId = admobId
            adView.setAdSize(adSize)

            it.removeAllViews()
            it.addView(adView)
            adViewsIds?.add(id)

            val adRequest: AdRequest = AdRequest.Builder().build()
            adView.loadAd(adRequest)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

@UiThread
fun MainActivity.initializeAds() {
    if (BuildConfig.ADS_APPLICATION_ID.isNotBlank()) {
        CoroutineTasks.createWithContext(
            this,
            { ctx: Context ->
                try {
                    MobileAds.initialize(ctx)
                    Result.Success(Unit)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Result.Error(e)
                }
            },
            { result ->
                if (result is Result.Success) {
                    if (adViewsIds?.isNotEmpty() == true) adViewsIds = mutableListOf()
                    this.runOnUiThread {
                        // don't show the navigation drawer ad in landscape mode
                        if (resources.configuration.orientation != Configuration.ORIENTATION_LANDSCAPE)
                            this.addBannerAd(R.id.ad_view_container, BuildConfig.AD_NAV_DRAWER_BOTTOM_BANNER_ID)
                    }
                }
            }
        ).execute(true)
    }
}

@UiThread
fun AboutActivity.initializeAds() {
    Log.d("@about", "initializeAds")
    addBannerAd(R.id.ad_view_container_top, BuildConfig.AD_ABOUT_SCREEN_TOP_BANNER_ID)
    addBannerAd(R.id.ad_view_container_bottom, BuildConfig.AD_ABOUT_SCREEN_BOTTOM_BANNER_ID)
}

fun ProductsAdapter.bindAdView(holder: ViewHolder.AdViewHolder, item: Item.AdItem<Product>) {
    val adView = item.value as AdView
    val adCardView: ViewGroup = holder.itemView as ViewGroup
    if (adCardView.childCount > 0) {
        // The AdViewHolder recycled by the RecyclerView may be a different
        // instance than the one used previously for this position. Clear the
        // AdViewHolder of any subviews in case it has a different
        // AdView associated with it, and make sure the AdView for this position doesn't
        // already have a parent of a different recycled AdViewHolder.
        adCardView.removeAllViews()
    }
    if (adView.parent != null) {
        (adView.parent as ViewGroup).removeView(adView)
    }

    // Add the banner to the adView
    adCardView.addView(adView)
}

fun <T, VW: ViewHolder, A : AdSupportingAdapter<T, VW>> AdSupportingAdapter.Companion.createAdView(
    context: Context,
    forAdapter: KClass<A>
): Item.AdItem<T> {
   val adView = AdView(context)
    adView.setAdSize(AdSize.BANNER)
    adView.adUnitId = when (forAdapter) {
        ProductsAdapter::class -> BuildConfig.AD_PRODUCTS_ADAPTER_BANNERS_ID
        else -> throw IllegalStateException("Unknown forAdapter in createAdView")
    }
    val adRequest: AdRequest = AdRequest.Builder().build()
    adView.loadAd(adRequest)
    return Item.AdItem(adView)
}

fun <T, VW: ViewHolder> AdSupportingAdapter<T, VW>.handleAdsForLifecycleStateImpl(event: Lifecycle.Event, view: View) {
    if (view is AdView) {
        when (event) {
            Lifecycle.Event.ON_CREATE -> { }
            Lifecycle.Event.ON_START -> {}
            Lifecycle.Event.ON_RESUME -> view.resume()
            Lifecycle.Event.ON_PAUSE -> view.pause()
            Lifecycle.Event.ON_STOP -> {}
            Lifecycle.Event.ON_DESTROY -> view.destroy()
            Lifecycle.Event.ON_ANY -> {}
        }
    }
}