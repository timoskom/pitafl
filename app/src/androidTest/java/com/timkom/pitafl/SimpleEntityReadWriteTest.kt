package com.timkom.pitafl

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.timkom.pitafl.data.projections.ProductProjection
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.db.room.ProductCategoryDao
import com.timkom.pitafl.db.room.ProductCategoryEntity
import com.timkom.pitafl.db.room.ProductDao
import com.timkom.pitafl.db.room.ProductEntity
import com.timkom.pitafl.db.room.ShoppingListDao
import com.timkom.pitafl.db.room.ShoppingListEntity
import com.timkom.pitafl.db.room.ShoppingListItemDao
import com.timkom.pitafl.db.room.ShoppingListItemEntity
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException

class SimpleEntityReadWriteTest {
    private lateinit var productCategoryDao: ProductCategoryDao
    private lateinit var productDao: ProductDao
    private lateinit var shoppingListDao: ShoppingListDao
    private lateinit var shoppingListItemDao: ShoppingListItemDao
    private lateinit var db: LocalDB

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, LocalDB::class.java).build()
        productCategoryDao = db.productCategoryDao()
        productDao = db.productDao()
        shoppingListDao = db.shoppingListDao()
        shoppingListItemDao = db.shoppingListItemDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeProductCategoryAndReadInList() {
        val productCategory = ProductCategoryEntity(
            id = 1,
            name = "testProductCategory #1",
            picture = "/TEST_PATH_1/"
        )
        productCategoryDao.insert(productCategory)
        val byName = productCategoryDao.getAll()
        assertThat(byName[0], equalTo(productCategory))
    }

    @Test
    @Throws(Exception::class)
    fun writeProductAndReadInList() {
        val productCategory = ProductCategoryEntity(
            id = 1,
            name = "testProductCategory #1",
            picture = "/TEST_PATH_1/"
        )
        productCategoryDao.insert(productCategory)

        val product = ProductEntity(
            id = 1,
            name = "testProduct #1",
            category = "testProductCategory #1",
            picture = "/TEST_PATH_1/",
            barcode = "24329329",
            expirationDate = "19/10/2024",
            quantity = 1,
            unitOfQuantity = ProductUnit.PACK.name,
            expirationMark = ProductProjection.ExpirationMark.EXPIRED.name
        )
        productDao.insert(product)
        val byName = productDao.getAll()
        assertThat(byName[0], equalTo(product))
    }

    @Test
    @Throws(Exception::class)
    fun writeShoppingListAndReadInList() {
        val shoppingList = ShoppingListEntity(
            id = 1,
            name = "testShoppingList #1",
        )
        shoppingListDao.insert(shoppingList)
        val byName = shoppingListDao.getAll()
        assertThat(byName[0], equalTo(shoppingList))
    }

    @Test
    @Throws(Exception::class)
    fun writeShoppingListItemAndReadInList() {
        val shoppingListItem = ShoppingListItemEntity(
            id = 1,
            name = "testShoppingListItem #1",
            isChecked = true,
            list = "testShoppingList #1"
        )
        shoppingListItemDao.insert(shoppingListItem)
        val byName = shoppingListItemDao.getAll()
        assertThat(byName[0], equalTo(shoppingListItem))
    }
}