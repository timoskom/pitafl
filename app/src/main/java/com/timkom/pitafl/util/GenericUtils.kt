package com.timkom.pitafl.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Debug
import androidx.annotation.RequiresApi
import androidx.annotation.UiThread
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.timkom.pitafl.BuildConfig
import com.timkom.pitafl.activity.BarcodeScannerActivity
import com.timkom.pitafl.activity.ProductsActivity
import com.timkom.pitafl.activity.TextRecognitionActivity
import com.timkom.pitafl.dialog.AddProductDialogFragment
import com.timkom.pitafl.dialog.ChangeNameDialogFragment
import com.timkom.pitafl.dialog.ChangePictureDialogFragment
import com.timkom.pitafl.dialog.ChangeProductBarcodeDialogFragment
import com.timkom.pitafl.dialog.DatePickerDialogFragment
import com.timkom.pitafl.dialog.ErrorDialogFragment
import java.lang.ref.WeakReference

/**
 * Helper functions, classes and constants
 */
object GenericUtils {

    /**
     * Find if night mode (dark mode) is enabled
     * @param ctx the app context
     * @return true if night mode is enabled otherwise false
     */
    @JvmStatic
    fun isNightModeEnabled(ctx: Context): Boolean {
        val nightModeFlags = ctx.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        return nightModeFlags == Configuration.UI_MODE_NIGHT_YES
    }

    /**
     * Find whether in debug mode or if in debug mode and also actively debugging
     * @param alsoDebugging If it should check for debugger connection
     * @return True if in debug mode (and actively debugging if alsoDebugging is true), false otherwise
     */
    @JvmStatic
    fun isDebug(alsoDebugging: Boolean): Boolean {
        return if (!alsoDebugging)
            BuildConfig.DEBUG
        else
            BuildConfig.DEBUG && (Debug.isDebuggerConnected() || Debug.waitingForDebugger())
    }

    @JvmStatic
    @UiThread
    @Synchronized
    fun createUnsupportedGalleryPickingFunctionalityDialog(ctx: Context) {
        MaterialAlertDialogBuilder(ctx)
            .setMessage("Image picking from gallery is not yet supported on devices running Android 12 or later!")
            .setNeutralButton(
                android.R.string.ok
            ) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    @JvmStatic
    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    @UiThread
    @Synchronized
    fun checkPostNotificationsPermission(ctx: Context): Boolean =
        ContextCompat.checkSelfPermission(
            ctx,
            Manifest.permission.POST_NOTIFICATIONS
        ) == PackageManager.PERMISSION_GRANTED

    @JvmStatic
    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    @Synchronized
    fun requestPostNotificationsPermission(activity: WeakReference<Activity>, requestCode: Int) =
        activity.get()?.requestPermissions(
            arrayOf(Manifest.permission.POST_NOTIFICATIONS),
            requestCode
        )

    @JvmStatic
    @UiThread
    fun showErrorDialog(
        supportFragmentManager: FragmentManager,
        e: Exception
    ) {
        e.printStackTrace()
        ErrorDialogFragment.newInstance().apply {
            val exceptionClass: Class<out Exception> = e.javaClass
            val errorArgs = Bundle().apply {
                putString(
                    IntentBundleKeys.ERROR_DIALOG_FRAGMENT_CLASS_TYPE_NAME_KEY,
                    exceptionClass.getTypeName()
                )
                putString(IntentBundleKeys.ERROR_DIALOG_FRAGMENT_MESSAGE_KEY, e.message)
            }
            setArguments(errorArgs)
        }.show(supportFragmentManager, "ERROR_DIALOG_FRAGMENT")
    }

    @JvmStatic
    fun checkForInternet(context: Context): Boolean {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = manager.activeNetwork?: return false
        val activeNetwork = manager.getNetworkCapabilities(network) ?: return false

        return when {
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            else -> false
        }
    }

    @JvmStatic
    fun canUseAds(context: Context? = null): Boolean {
        val baseCheck = BuildConfig.IS_MONET &&
                Build.VERSION.SDK_INT < Build.VERSION_CODES.VANILLA_ICE_CREAM
        return context?.let { baseCheck && checkForInternet(it) } ?: baseCheck
    }

    /**
     * Keys for bundled arguments that are passed on intents
     * or returned from intents
     */
    object IntentBundleKeys {
        /**
         * [AddProductDialogFragment]'s category key
         */
        const val ADD_PRODUCT_DIALOG_FRAGMENT_CATEGORY_KEY: String = "CATEGORY_OF_PRODUCT"

        /**
         * [BarcodeScannerActivity]'s barcode display value key
         */
        const val BARCODE_SCANNER_ACTIVITY_BARCODE_DISPLAY_VALUE_KEY: String =
            "BARCODE_DISPLAY_VALUE"

        /**
         * [BarcodeScannerActivity]'s barcode format key
         */
        const val BARCODE_SCANNER_ACTIVITY_BARCODE_FORMAT_KEY: String = "BARCODE_FORMAT"

        /**
         * [BarcodeScannerActivity]'s barcode raw bytes key
         */
        const val BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_BYTES_KEY: String = "BARCODE_RAW_BYTES"

        /**
         * [BarcodeScannerActivity]'s barcode raw value key
         */
        const val BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_VALUE_KEY: String = "BARCODE_RAW_VALUE"

        /**
         * [ChangePictureDialogFragment]'s call source (`fromWhat`)
         */
        const val CHANGE_PICTURE_DIALOG_FRAGMENT_CALL_SRC: String = "CALL_SOURCE"

        /**
         * [ChangePictureDialogFragment]'s table source (`fromTable`)
         */
        const val CHANGE_PICTURE_DIALOG_FRAGMENT_TABLE_SRC: String = "TABLE_SOURCE"

        /**
         * [ErrorDialogFragment]'s error [Class] type name key
         */
        const val ERROR_DIALOG_FRAGMENT_CLASS_TYPE_NAME_KEY: String = "ERROR_TYPE_CLASS_NAME"

        /**
         * [ErrorDialogFragment]'s error message key
         */
        const val ERROR_DIALOG_FRAGMENT_MESSAGE_KEY: String = "ERROR_MESSAGE"

        /**
         * [ChangeProductBarcodeDialogFragment]'s call source (`fromWhat`)
         */
        const val CHANGE_PRODUCT_BARCODE_DIALOG_FRAGMENT_CALL_SRC: String = "CALL_SOURCE"

        /**
         * [DatePickerDialogFragment]'s call source (`fromWhat`)
         */
        const val DATE_PICKER_DIALOG_FRAGMENT_CALL_SRC: String = "CALL_SOURCE"

        /**
         * [DatePickerDialogFragment]'s request key
         */
        const val DATE_PICKER_DIALOG_FRAGMENT_REQUEST_KEY: String = "DATE_PICKER_REQUEST"

        /**
         * [DatePickerDialogFragment]'s result key
         */
        const val DATE_PICKER_DIALOG_FRAGMENT_RESULT_KEY: String = "DATE_PICKER_RESULT"

        /**
         * [DatePickerDialogFragment]'s true result day key
         */
        const val DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_DAY_KEY: String =
            "DATE_PICKER_TRUE_RESULT_DAY"

        /**
         * [DatePickerDialogFragment]'s true result month key
         */
        const val DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_MONTH_KEY: String =
            "DATE_PICKER_TRUE_RESULT_MONTH"

        /**
         * [DatePickerDialogFragment]'s true result year key
         */
        const val DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_YEAR_KEY: String =
            "DATE_PICKER_TRUE_RESULT_YEAR"

        /**
         * [ProductsActivity]'s category key
         */
        const val PRODUCTS_ACTIVITY_CATEGORY_KEY: String = "CATEGORY"

        /**
         * [TextRecognitionActivity]'s text key
         */
        const val TEXT_RECOGNITION_ACTIVITY_TEXT_KEY: String = "TEXT_TEXT"

        /**
         * [ChangeNameDialogFragment]'s call source (`fromWhat`)
         */
        const val CHANGE_NAME_DIALOG_FRAGMENT_CALL_SRC: String = "CHANGE_NAME_CALL_SOURCE"

        /**
         * [ChangeNameDialogFragment]'s current name key
         */
        const val CHANGE_NAME_DIALOG_FRAGMENT_CURRENT_NAME_KEY: String = "CHANGE_NAME_CURRENT_NAME"
    }

}