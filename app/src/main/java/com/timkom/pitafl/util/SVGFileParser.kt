package com.timkom.pitafl.util

import android.graphics.Picture
import android.graphics.drawable.PictureDrawable
import com.larvalabs.svgandroid.SVG
import com.larvalabs.svgandroid.SVGBuilder
import com.larvalabs.svgandroid.SVGDrawable
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.nio.file.Path

/**
 * Class to parse svg images.
 */
class SVGFileParser(private val imagePath: Path) {

    /**
     * Gets the svg picture from the current [Path].
     * @return [SVG]
     * @throws FileNotFoundException from [FileInputStream]
     */
    @Throws(FileNotFoundException::class)
    private fun getSVG(): SVG {
        val builder = SVGBuilder()
        val fileInputStream = FileInputStream(imagePath.toAbsolutePath().toFile())
        return builder
            .readFromInputStream(fileInputStream)
            .setCloseInputStreamWhenDone(true)
            .build()
    }

    /**
     * Gets the [SVG] (svg picture from the current [Path]) as [SVGDrawable].
     * @return [SVGDrawable]
     * @throws FileNotFoundException from [getSVG]
     */
    @Throws(FileNotFoundException::class)
    fun getAsSvgDrawable(): SVGDrawable = SVGDrawable(getSVG())

    /**
     * Gets the [SVG] (svg picture from the current [Path]) as [PictureDrawable].
     * @return [PictureDrawable]
     * @throws FileNotFoundException from [getSVG]
     */
    @Throws(FileNotFoundException::class)
    fun getAsPictureDrawable(): PictureDrawable = getSVG().drawable

    /**
     * Gets the [SVG] (svg picture from the current [Path]) as [Picture].
     * @return [Picture]
     * @throws FileNotFoundException from [getSVG]
     */
    @Throws(FileNotFoundException::class)
    fun getAsPicture(): Picture = getSVG().picture

}
