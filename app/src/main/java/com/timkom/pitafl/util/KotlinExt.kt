package com.timkom.pitafl.util

import kotlin.reflect.KClass

/**
 * Calls the specified function block. If an exception is thrown and is one of the specified
 * [exceptions] then the [onCatch] blocked is invoked, else the exception is re-thrown.
 * @param R The return type of the function block.
 * @param onCatch The function to invoke if an exception is thrown and is one of the specified.
 * @param exceptions The exceptions to handle.
 * @return The result of the function block.
 */
@Suppress("unused")
inline fun <R> (() -> R).multiCatch(onCatch: (Throwable) -> R, vararg exceptions: KClass<out Throwable>): R {
    return try {
        this()
    } catch (e: Throwable) {
        if (e::class in exceptions) onCatch(e) else throw e
    }
}

/**
 * Calls the specified suspend function block. If an exception is thrown and is one of the specified
 * [exceptions] then the [onCatch] blocked is invoked, else the exception is re-thrown.
 * @param R The return type of the suspend function block.
 * @param onCatch The function to invoke if an exception is thrown and is one of the specified.
 * @param exceptions The exceptions to handle.
 * @return The result of the suspend function block.
 */
@Suppress("unused")
suspend inline fun <R> (suspend () -> R).multiCatch(onCatch: (Throwable) -> R, vararg exceptions: KClass<out Throwable>): R {
    return try {
        this()
    } catch (e: Throwable) {
        if (e::class in exceptions) onCatch(e) else throw e
    }
}

/**
 * Invokes the [runThis] function. If an exception is thrown and is one of the specified [exceptions]
 * then the [onCatch] blocked is invoked, else the exception is re-thrown.
 * @param R The return type of the function.
 * @param onCatch The function to invoke if an exception is thrown and is one of the specified.
 * @param exceptions The exceptions to handle.
 * @return The result of the function.
 */
@Suppress("unused")
inline fun <R> tryMultiCatch(noinline runThis: () -> R, onCatch: (Throwable) -> R, vararg exceptions: KClass<out Throwable>): R {
    return runThis.multiCatch(
        onCatch,
        *exceptions
    )
}

/**
 * Invokes the [runThis] suspend function. If an exception is thrown and is one of the specified
 * [exceptions] then the [onCatch] blocked is invoked, else the exception is re-thrown.
 * @param R The return type of the suspend function.
 * @param onCatch The function to invoke if an exception is thrown and is one of the specified.
 * @param exceptions The exceptions to handle.
 * @return The result of the suspend function.
 */
@Suppress("unused")
suspend inline fun <R> tryMultiCatchSuspend(noinline runThis: suspend () -> R, onCatch: (Throwable) -> R, vararg exceptions: KClass<out Throwable>): R {
    return runThis.multiCatch(
        onCatch,
        *exceptions
    )
}