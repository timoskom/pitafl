@file:Suppress("DEPRECATION", "MemberVisibilityCanBePrivate")

package com.timkom.pitafl.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.hardware.Camera
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import java.io.IOException

/**
 * Extensions methods that fix problems with [Bitmap]s taken from the camera
 */
object CameraBitmapFixingExtensions {

    /**
     * Finds the main shooter from a pool of back cameras
     * @return [Int]
     */
    fun findMainShooter(): Int {
        var cameraId = -1
        val numberOfCameras = Camera.getNumberOfCameras()
        for (i in 0 until numberOfCameras) {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(i, info)
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                if (GenericUtils.isDebug(false)) {
                    Log.d("DEBUG_TAG", "Camera found")
                }
                cameraId = i
                break
            }
        }
        return cameraId
    }

    /**
     * Finds the rotation of the image
     * @param context The [Context]
     * @param imageUri The [Uri] of the image
     * @return [Int]
     */
    fun getImageRotation(context: Context, imageUri: Uri): Int {
        try {
            val exif = ExifInterface(imageUri.path!!)
            val rotation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            return if (rotation == ExifInterface.ORIENTATION_UNDEFINED)
                getRotationFromMediaStore(context, imageUri)
            else
                exifToDegrees(rotation)
        } catch (e: IOException) {
            return 0
        }
    }

    /**
     * Finds the rotation of the image from the [MediaStore.Images.Media.ORIENTATION] info
     * @param context The [Context]
     * @param imageUri The [Uri] of the image
     * @return [Int]
     */
    fun getRotationFromMediaStore(context: Context, imageUri: Uri): Int {
        val columns = arrayOf(MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION)
        val cursor = context.contentResolver
            .query(imageUri, columns, null, null, null) ?: return 0

        cursor.moveToFirst()

        val orientationColumnIndex = cursor.getColumnIndex(columns[1])
        val rot = cursor.getInt(orientationColumnIndex)
        cursor.close()
        return rot
    }

    /**
     * Translates exif info for orientation to a real value
     * @param exifOrientation The exif orientation info
     * @return `int`
     */
    private fun exifToDegrees(exifOrientation: Int): Int {
        return when (exifOrientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            else -> 0
        }
    }

    /**
     * Rotates the [Bitmap] by `degrees`
     * @param bitmap The [Bitmap] to rotate
     * @param degrees Degrees to rotate
     * @return The rotated [Bitmap]
     */
    fun rotate(bitmap: Bitmap, degrees: Int): Bitmap {
        val w = bitmap.width
        val h = bitmap.height

        val mtx = Matrix()
        mtx.postRotate(degrees.toFloat())

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true)
    }

}