package com.timkom.pitafl.util

import androidx.annotation.IntDef
import java.util.Random

object PseudoBarcodeGenerator {

    const val PSEUDO_BARCODE_FORMAT_EAN_8: Int = 8
    const val PSEUDO_BARCODE_FORMAT_EAN_13: Int = 13
    const val PSEUDO_BARCODE_FORMAT_UPC_A: Int = 12

    fun generateNew(@PseudoBarcodeFormat format: Int): String {
        val retBuilder = StringBuilder()
        when (format) {
            PSEUDO_BARCODE_FORMAT_EAN_13 -> {
                retBuilder.append(100 + Random().nextInt(899))
                retBuilder.append(1000 + Random().nextInt(8999))
                retBuilder.append(1000 + Random().nextInt(8999))
            }

            PSEUDO_BARCODE_FORMAT_EAN_8 -> {
                retBuilder.append(1000 + Random().nextInt(8999))
                retBuilder.append(1000 + Random().nextInt(8999))
            }

            PSEUDO_BARCODE_FORMAT_UPC_A -> {
                retBuilder.append(1000 + Random().nextInt(8999))
                retBuilder.append(1000 + Random().nextInt(8999))
                retBuilder.append(1000 + Random().nextInt(8999))
            }
        }
        return retBuilder.toString()
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(
        PSEUDO_BARCODE_FORMAT_EAN_8,
        PSEUDO_BARCODE_FORMAT_EAN_13,
        PSEUDO_BARCODE_FORMAT_UPC_A
    )
    annotation class PseudoBarcodeFormat

}