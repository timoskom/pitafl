package com.timkom.pitafl.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.PictureDrawable
import android.graphics.drawable.VectorDrawable
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

/**
 * Extension methods that translate [Drawable]s into [Bitmap]s
 */
@Suppress("MemberVisibilityCanBePrivate")
object BitmapDrawableExtensions {

    /**
     * Translates a [PictureDrawable] into a [Bitmap] by
     * drawing it to a [Canvas]
     * @param drawable The [PictureDrawable] to make into [Bitmap]
     * @return [Bitmap]
     */
    fun getBitmap(drawable: PictureDrawable): Bitmap {
        val bm = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bm)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bm
    }

    /**
     * Translates a [VectorDrawable] into a [Bitmap] by
     * drawing it to a [Canvas]
     * @param drawable The [VectorDrawable] to make into [Bitmap]
     * @return [Bitmap]
     */
    fun getBitmap(drawable: VectorDrawable): Bitmap {
        val bm = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bm)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bm
    }

    /**
     * Decides [DrawableType] based on the result of the `instanceof` operator
     * @param drawable The [Drawable] to check
     * @return [DrawableType]
     */
    fun whatDrawable(drawable: Drawable?): DrawableType {
        return when (drawable) {
            is BitmapDrawable -> DrawableType.BitmapDrawableType
            is VectorDrawable -> DrawableType.VectorDrawableType
            else -> DrawableType.UnknownDrawableType
        }
    }

    /**
     * Decides [DrawableType] by calling [.whatDrawable]
     * @param context The [Context] used to get the [Drawable]
     * @param drawableResId The [androidx.annotation.DrawableRes] of the [Drawable]
     * @return [DrawableType]
     */
    fun whatDrawable(context: Context, @DrawableRes drawableResId: Int): DrawableType {
        val drawable = ContextCompat.getDrawable(context, drawableResId)
        return whatDrawable(drawable)
    }

    /**
     * Translates [Drawable] into a [Bitmap] based on its type
     * @param context The [Context] used to get the [Drawable]
     * @param drawableResId The [androidx.annotation.DrawableRes] of the [Drawable]
     * @return [DrawableType]
     *
     * @throws IllegalArgumentException in case of [DrawableType.UnknownDrawableType]
     */
    fun getBitmap(context: Context, @DrawableRes drawableResId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableResId)
        return when (whatDrawable(context, drawableResId)) {
            DrawableType.BitmapDrawableType -> BitmapFactory.decodeResource(context.resources, drawableResId)
            DrawableType.VectorDrawableType -> drawable?.let {
                getBitmap(it as VectorDrawable)
            } ?: throw IllegalArgumentException("")
            else -> throw IllegalArgumentException("unsupported drawable")
        }
    }

    /**
     * Type of [Drawable]
     */
    enum class DrawableType {
        BitmapDrawableType,
        VectorDrawableType,
        UnknownDrawableType
    }

}