package com.timkom.pitafl.dialog

import android.os.Bundle
import android.os.Process
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.Group
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.google.android.material.textview.MaterialTextView
import com.timkom.pitafl.R
import com.timkom.pitafl.util.GenericUtils
import kotlin.system.exitProcess

class ErrorDialogFragment : NonCancelableDialogFragment() {

    private lateinit var cardView: MaterialCardView
    private lateinit var imageIV: ImageView
    private lateinit var mainMessageTV: MaterialTextView
    private lateinit var acceptBtn: MaterialButton
    private lateinit var expandIV: ImageView
    private lateinit var errorMessageTV: MaterialTextView
    private lateinit var hiddenGroup: Group
    private var errorMessage: String? = null
    private var errorClassTypeName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val bundle = requireArguments()
        errorMessage =
            bundle.getString(GenericUtils.IntentBundleKeys.ERROR_DIALOG_FRAGMENT_MESSAGE_KEY)
        errorClassTypeName =
            bundle.getString(GenericUtils.IntentBundleKeys.ERROR_DIALOG_FRAGMENT_CLASS_TYPE_NAME_KEY)
        return inflater.inflate(R.layout.error_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardView = view.findViewById(R.id.error_dialog_card)
        imageIV = view.findViewById(R.id.error_dialog_image_imageview)
        mainMessageTV = view.findViewById(R.id.error_dialog_main_message_textview)
        acceptBtn = view.findViewById(R.id.error_dialog_accept_button)
        expandIV = view.findViewById(R.id.error_dialog_expand_imageview)
        errorMessageTV = view.findViewById(R.id.error_dialog_error_message_textview)
        hiddenGroup = view.findViewById(R.id.error_dialog_hidden_group)

        acceptBtn.setOnClickListener {
            requireActivity().finishAffinity()
            Process.killProcess(Process.myPid())
            // if process could not be killed then exit
            exitProcess(0)
        }

        expandIV.setOnClickListener {
            if (hiddenGroup.visibility == View.VISIBLE) {
                TransitionManager.beginDelayedTransition(cardView, AutoTransition())
                expandIV.setImageResource(R.drawable.baseline_expand_more_black_24)
                hiddenGroup.setVisibility(View.GONE)
            } else {
                TransitionManager.beginDelayedTransition(cardView, AutoTransition())
                expandIV.setImageResource(R.drawable.baseline_expand_less_black_24)
                hiddenGroup.setVisibility(View.VISIBLE)
            }
        }

        hiddenGroup.setVisibility(View.GONE)

        errorMessageTV.text = getString(
            R.string.error_dialog_error_message_textview_text,
            errorClassTypeName,
            errorMessage
        )
    }

    companion object {
        @JvmStatic
        fun newInstance(): ErrorDialogFragment =
            ErrorDialogFragment().apply {
                val args = Bundle()
                arguments = args
            }
    }

}
