package com.timkom.pitafl.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import com.larvalabs.svgandroid.SVGBuilder
import com.timkom.pitafl.R
import com.timkom.pitafl.util.CameraBitmapFixingExtensions
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class CreateUserDialogFragment : DialogFragmentWithCameraPermissions() {

    private lateinit var imageOfUserIV: ImageView
    private lateinit var loadImageUserIB: ImageButton
    private lateinit var takeImageUserIB: ImageButton
    private lateinit var nameOfUserET: EditText
    private lateinit var surnameOfUserET: EditText
    private lateinit var usernameOfUserET: EditText
    private lateinit var createUserIB: ImageButton
    private lateinit var cancelCreateUserIB: ImageButton
    private var imageOfUser: Bitmap? = null
    private var nameOfUser: String? = null
    private var surnameOfUser: String? = null
    private var usernameOfUser: String? = null
    private var isUsernameSet = false
    private lateinit var pickMedia: ActivityResultLauncher<PickVisualMediaRequest>


    interface CreateUserListener {
        fun onFinishCreateUserDialog(
            userName: String, userSurname: String,
            userUsername: String,
            userImage: Bitmap?
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pickMedia = registerForActivityResult<PickVisualMediaRequest, Uri>(
            ActivityResultContracts.PickVisualMedia()
        ) { uri: Uri? ->
            if (uri != null) {
                try {
                    val thisContext = this.requireContext()
                    val rot = CameraBitmapFixingExtensions.getImageRotation(thisContext, uri)
                    @Suppress("DEPRECATION")
                    imageOfUser =
                        MediaStore.Images.Media.getBitmap(thisContext.contentResolver, uri)
                    imageOfUser?.let {
                        imageOfUser = CameraBitmapFixingExtensions.rotate(it, rot)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                imageOfUserIV.setImageBitmap(imageOfUser)
                if (isUsernameSet) {
                    createUserIB.setImageResource(R.drawable.baseline_check_circle_green_48)
                    createUserIB.alpha = 1f
                    createUserIB.isEnabled = true
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        isCancelable = false
        return inflater.inflate(R.layout.create_user_dialog, container)
    }

    @Suppress("DEPRECATION")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageOfUserIV = view.findViewById(R.id.image_of_user)
        loadImageUserIB = view.findViewById(R.id.load_image_user)
        takeImageUserIB = view.findViewById(R.id.take_image_user)
        nameOfUserET = view.findViewById(R.id.name_of_user)
        surnameOfUserET = view.findViewById(R.id.surname_of_user)
        usernameOfUserET = view.findViewById(R.id.username_of_user)
        createUserIB = view.findViewById(R.id.create_user)
        cancelCreateUserIB = view.findViewById(R.id.cancel_create_user)

        try {
            val svg = SVGBuilder().readFromAsset(
                requireActivity().assets,
                getString(R.string.assets_icon_user_avatar)
            ).build()
            imageOfUserIV.setImageDrawable(svg.drawable)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        imageOfUserIV.setOnClickListener { v: View ->
            val items = arrayOf<CharSequence>(
                "Take Photo",
                "Choose from Gallery",
                "Cancel"
            )
            val builder = AlertDialog.Builder(this.context)
                .setItems(items) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                    if (items[which] == "Take Photo") {
                        dialog.dismiss()
                        val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (!hasCameraPermission()) {
                            requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
                        } else {
                            if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                            }
                        }
                    } else if (items[which] == "Choose from Gallery") {
                        dialog.dismiss()
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                            val loadPicIntent =
                                Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                )
                            if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                            }
                        } else {
                            pickMedia.launch(
                                PickVisualMediaRequest.Builder()
                                    .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                                    .build()
                            )
                        }
                    } else {
                        dialog.cancel()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }

        loadImageUserIB.setOnClickListener { v: View ->
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                val loadPicIntent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                }
            } else {
                pickMedia.launch(
                    PickVisualMediaRequest.Builder()
                        .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                        .build()
                )
            }
        }

        takeImageUserIB.setOnClickListener { v: View ->
            val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (!hasCameraPermission()) {
                requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
            } else {
                if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }

        usernameOfUserET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                if (s.isNotBlank()) {
                    if (imageOfUser != null) {
                        createUserIB.apply {
                            setImageResource(R.drawable.baseline_check_circle_green_48)
                            setAlpha(1f)
                            setEnabled(true)
                        }
                    }
                    isUsernameSet = true
                } else {
                    createUserIB.apply {
                        setImageResource(R.drawable.baseline_check_circle_gray_48)
                        setAlpha(.5f)
                        setEnabled(false)
                    }
                    isUsernameSet = false
                }
            }
        })

        createUserIB.setAlpha(.5f)
        createUserIB.setEnabled(false)

        createUserIB.setOnClickListener {
            nameOfUser = nameOfUserET.getText().toString().trim()
            surnameOfUser = surnameOfUserET.getText().toString().trim()
            usernameOfUser = usernameOfUserET.getText().toString().trim()

            if (imageOfUser == null) {
                try {
                    val assets = requireActivity().assets
                    val image = File(
                        requireActivity().getDir(
                            getString(R.string.app_data_folder),
                            Context.MODE_PRIVATE
                        ),
                        getString(R.string.predefined_icon_user_avatar)
                    )
                    Files.copy(
                        assets.open(getString(R.string.assets_icon_user_avatar)),
                        image.toPath(),
                        StandardCopyOption.REPLACE_EXISTING
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            val listener = requireActivity() as CreateUserListener

            listener.onFinishCreateUserDialog(
                if (nameOfUser?.isNotBlank() == true) (nameOfUser ?: "demo") else "demo",
                if (surnameOfUser?.isNotBlank() == true) (surnameOfUser ?: "demo") else "demo",
                if (usernameOfUser?.isNotBlank() == true) (usernameOfUser ?: "demo") else "demo",
                imageOfUser
            )
            dismiss()
        }

        cancelCreateUserIB.setOnClickListener {
            val listener = requireActivity() as CreateUserListener


            try {
                val assets = requireActivity().assets
                val image = File(
                    requireActivity().getDir(
                        getString(R.string.app_data_folder),
                        Context.MODE_PRIVATE
                    ),
                    getString(R.string.predefined_icon_user_avatar)
                )
                Files.copy(
                    assets.open(getString(R.string.assets_icon_user_avatar)),
                    image.toPath(),
                    StandardCopyOption.REPLACE_EXISTING
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }

            listener.onFinishCreateUserDialog(
                "demo",
                "demo",
                "demo",
                null
            )
            dismiss()
        }
    }

    @Suppress("OVERRIDE_DEPRECATION", "DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null && requestCode == PICK_A_PHOTO_CODE) {
            val photoUri = data.data
            if (photoUri != null) {
                try {
                    val thisContext = requireContext()
                    val rot = CameraBitmapFixingExtensions.getImageRotation(thisContext, photoUri)
                    imageOfUser =
                        MediaStore.Images.Media.getBitmap(thisContext.contentResolver, photoUri)
                    imageOfUser?.let {
                        imageOfUser = CameraBitmapFixingExtensions.rotate(it, rot)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                imageOfUserIV.setImageBitmap(imageOfUser)
                if (isUsernameSet) {
                    createUserIB.apply {
                        setImageResource(R.drawable.baseline_check_circle_green_48)
                        alpha = 1f
                        isEnabled = true
                    }
                }
            }
        } else if ((requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) && data != null) {
            val extras = data.extras
            if (extras != null) {
                imageOfUser = extras["data"] as Bitmap?
                imageOfUserIV.setImageBitmap(imageOfUser)
                if (isUsernameSet) {
                    createUserIB.apply {
                        setImageResource(R.drawable.baseline_check_circle_green_48)
                        alpha = 1f
                        isEnabled = true
                    }
                }
            }
        }
    }

    companion object {
        const val PICK_A_PHOTO_CODE: Int = 1001
        const val REQUEST_IMAGE_CAPTURE: Int = 1002

        @JvmStatic
        fun newInstance(): CreateUserDialogFragment =
            CreateUserDialogFragment().apply {
                val args = Bundle()
                arguments = args
            }
    }

}
