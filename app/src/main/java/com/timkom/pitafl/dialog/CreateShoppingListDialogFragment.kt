package com.timkom.pitafl.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.widget.addTextChangedListener
import com.timkom.pitafl.R
import com.timkom.pitafl.task.coroutine.CheckIfShoppingListExistsTask
import com.timkom.pitafl.task.coroutine.base.Result

class CreateShoppingListDialogFragment : NonCancelableDialogFragment() {

    interface CreateShoppingListListener {
        fun onFinishCreateShoppingListDialog(listName: String)
    }

    private lateinit var nameOfListET: EditText
    private lateinit var cancelCreateListIB: AppCompatImageButton
    private lateinit var createListIB: AppCompatImageButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.create_shopping_list_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nameOfListET = view.findViewById(R.id.name_of_list_edit_text)
        cancelCreateListIB = view.findViewById(R.id.cancel_create_list_image_button)
        createListIB = view.findViewById(R.id.create_list_image_button)

        val thisDialog = requireDialog()
        nameOfListET.requestFocus()
        thisDialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        nameOfListET.addTextChangedListener {
            if (it?.isNotBlank() == true) {
                CheckIfShoppingListExistsTask(
                    this@CreateShoppingListDialogFragment.requireContext(),
                    it.toString().trim()
                ) { result ->
                    if (result is Result.Success<Boolean>) {
                        if (!result.data) {
                            createListIB.apply {
                                setImageResource(R.drawable.baseline_add_circle_green_48)
                                alpha = 1f
                                isEnabled = true
                            }
                        } else {
                            createListIB.apply {
                                setImageResource(R.drawable.baseline_add_circle_gray_48)
                                alpha = .5f
                                isEnabled = false
                            }
                            nameOfListET.error =
                                "Shopping list " + it.toString().trim() + " already exists!"
                        }
                    } else if (result is Result.Error) {
                        result.exception.printStackTrace()
                    }
                }.execute(true)
            } else {
                createListIB.apply {
                    setImageResource(R.drawable.baseline_add_circle_gray_48)
                    alpha = .5f
                    isEnabled = false
                }
            }
        }

        cancelCreateListIB.setOnClickListener { thisDialog.cancel() }

        createListIB.setOnClickListener {
            val nameOfList = nameOfListET.getText().toString().trim()
            if (nameOfList.isNotBlank()) {
                val listener = requireActivity() as CreateShoppingListListener
                listener.onFinishCreateShoppingListDialog(nameOfList)
            }
            dismiss()
        }

        val name = requireArguments().getString(FILTER_ARG_TAG)
        nameOfListET.setText(name)
    }


    companion object {
        private const val FILTER_ARG_TAG = "FILTER_ARG"

        @JvmStatic
        fun newInstance(filter: String): CreateShoppingListDialogFragment =
            CreateShoppingListDialogFragment().apply {
                val args = Bundle()
                args.putString(FILTER_ARG_TAG, filter)
                arguments = args
            }
    }
}