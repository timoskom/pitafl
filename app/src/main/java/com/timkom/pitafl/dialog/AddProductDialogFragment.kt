package com.timkom.pitafl.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.NumberPicker
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.Toolbar
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.google.android.material.textfield.TextInputLayout
import com.google.mlkit.vision.barcode.common.Barcode
import com.timkom.pitafl.ProductUnit
import com.timkom.pitafl.ProductUnit.Companion.getAllStrings
import com.timkom.pitafl.ProductUnit.Companion.resolveFromString
import com.timkom.pitafl.R
import com.timkom.pitafl.activity.BarcodeScannerActivity
import com.timkom.pitafl.activity.TextRecognitionActivity
import com.timkom.pitafl.db.room.LocalDB.Companion.getInstance
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks.createResultListener
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks.createWithContext
import com.timkom.pitafl.task.coroutine.base.Result
import com.timkom.pitafl.util.BitmapDrawableExtensions
import com.timkom.pitafl.util.CameraBitmapFixingExtensions
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.SVGFileParser
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.Exception
import java.util.Calendar

@Suppress("OVERRIDE_DEPRECATION", "DEPRECATION")
class AddProductDialogFragment : DialogFragmentWithCameraPermissions() {
    private lateinit var addProductToolbar: Toolbar
    private lateinit var imageOfNewProductIV: ImageView
    private lateinit var loadImageNewCategoryIB: ImageButton
    private lateinit var takeImageNewCategoryIB: ImageButton
    private lateinit var nameOfNewProductET: EditText
    private lateinit var nameOfNewProductTextRecognitionIB: ImageButton
    private lateinit var categoryOfNewProductET: EditText
    private lateinit var expDateOfNewProductET: EditText
    private lateinit var quantityOfNewProductNP: NumberPicker
    private lateinit var decreaseQuantityIB: ImageButton
    private lateinit var increaseQuantityIB: ImageButton
    private lateinit var unitTIL: TextInputLayout
    private lateinit var unitTV: MaterialAutoCompleteTextView
    private lateinit var barcodeOfNewProductET: EditText
    private lateinit var barcodeOfNewProductScannerIB: ImageButton
    private var imageOfProduct: Bitmap? = null
    private var nameOfProduct: String? = null
    private var categoryOfProduct: String? = null
    private var expDateOfProduct: String? = null
    private var quantityOfProduct: Int? = null
    private var unitOfQuantityOfProduct: ProductUnit? = null
    private var barcodeOfProduct: String? = null
    private var year: Int? = null
    private var month: Int? = null
    private var day: Int? = null
    private var barcodeDisplayValue: String? = null
    private var barcodeRawValue: String? = null
    private val barcodeRawBytes = ArrayList<Byte>()
    private var barcodeFormat = 0
    private var defaultImage: Bitmap? = null
    private lateinit var pickMedia: ActivityResultLauncher<PickVisualMediaRequest>

    interface AddProductListener {
        fun onFinishAddProductDialog(
            productImage: Bitmap, productName: String,
            productCategory: String, productExpDate: String,
            productQuantity: Int, productUnitOfQuantity: ProductUnit,
            productBarcode: String
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.add_product_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val ctx = requireContext()

        addProductToolbar = view.findViewById(R.id.addProductToolbar)
        addProductToolbar.setNavigationOnClickListener { dismiss() }
        addProductToolbar.setOnMenuItemClickListener { item: MenuItem ->
            if (item.itemId == R.id.add_product) {
                nameOfProduct = nameOfNewProductET.text.toString().trim()
                if (day != null && month != null && year != null) {
                    expDateOfProduct = (day.toString() + "/" + month + "/" + year).trim()
                }
                quantityOfProduct = quantityOfNewProductNP.value
                unitOfQuantityOfProduct = resolveFromString(unitTV.text.toString(), ctx)
                barcodeOfProduct = barcodeOfNewProductET.text.toString().trim()
                if (nameOfProduct!!.isNotBlank()) {
                    if (expDateOfProduct == null || expDateOfProduct!!.isEmpty()) {
                        val calendar = Calendar.getInstance()
                        val day = calendar[Calendar.DAY_OF_MONTH]
                        val month = calendar[Calendar.MONTH]
                        val year = calendar[Calendar.YEAR]
                        expDateOfProduct = "$day/$month/$year"
                    }
                    val listener = requireActivity() as AddProductListener
                    if (imageOfProduct == null) {
                        imageOfProduct = defaultImage
                    }

                    // make all values immutable
                    val immImageOfProduct = imageOfProduct
                    val immNameOfProduct = nameOfProduct
                    val immCategoryOfProduct = categoryOfProduct
                    val immExpDateOfProduct = expDateOfProduct
                    val immQuantityOfProduct = quantityOfProduct
                    val immUnitOfQuantityOfProduct = unitOfQuantityOfProduct
                    val immBarcodeOfProduct = barcodeOfProduct

                    // then check all of them for null
                    if (immImageOfProduct != null &&
                        immNameOfProduct != null &&
                        immCategoryOfProduct != null &&
                        immExpDateOfProduct != null &&
                        immQuantityOfProduct != null  &&
                        immUnitOfQuantityOfProduct != null &&
                        immBarcodeOfProduct != null) {

                        // and then call the listener method with the immutable values
                        listener.onFinishAddProductDialog(
                            immImageOfProduct, immNameOfProduct, immCategoryOfProduct,
                            immExpDateOfProduct, immQuantityOfProduct, immUnitOfQuantityOfProduct,
                            immBarcodeOfProduct
                        )
                    }
                }
                dismiss()
                return@setOnMenuItemClickListener true
            } else {
                return@setOnMenuItemClickListener false
            }
        }
        imageOfNewProductIV = view.findViewById(R.id.image_of_new_product)

        createWithContext(
            requireContext(),
            { context: Context ->
                categoryOfProduct?.let {
                    val db = getInstance(context)
                    val picture =
                        db.productCategoryDao().getPictureOfCategory(it)
                    Result.Success(picture.picture)
                } ?: Result.Error(Exception("categoryOfProduct is null"))
            },
            createResultListener({ picturePath: String ->
                val image = File(picturePath)
                if (image.toPath().toString().endsWith(".svg")) {
                    val svgParse = SVGFileParser(image.toPath())
                    try {
                        val pic = svgParse.getAsPictureDrawable()
                        defaultImage = BitmapDrawableExtensions.getBitmap(pic)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }
                } else {
                    defaultImage = BitmapFactory.decodeFile(image.absolutePath, BitmapFactory.Options())
                }
                imageOfNewProductIV.setImageBitmap(defaultImage)
            })
        ).execute(true)

        imageOfNewProductIV.setOnClickListener { v: View ->
            val items = arrayOf<CharSequence>(
                "Take Photo",
                "Choose from Gallery",
                "Cancel"
            )
            val builder = AlertDialog.Builder(this.context)
                .setItems(items) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                    if (items[which] == "Take Photo") {
                        dialog.dismiss()
                        val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (!hasCameraPermission()) {
                            requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
                        } else {
                            if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                            }
                        }
                    } else if (items[which] == "Choose from Gallery") {
                        dialog.dismiss()
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                            val loadPicIntent = Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                            )
                            if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                            }
                        } else {
                            pickMedia.launch(
                                PickVisualMediaRequest.Builder()
                                    .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                                    .build()
                            )
                        }
                    } else {
                        dialog.cancel()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }
        loadImageNewCategoryIB = view.findViewById(R.id.load_image_new_product)
        loadImageNewCategoryIB.setOnClickListener { v: View ->
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                val loadPicIntent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                }
            } else {
                pickMedia.launch(
                    PickVisualMediaRequest.Builder()
                        .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                        .build()
                )
            }
        }
        takeImageNewCategoryIB = view.findViewById(R.id.take_image_new_product)
        takeImageNewCategoryIB.setOnClickListener { v: View ->
            val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (!hasCameraPermission()) {
                requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
            } else {
                if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
        nameOfNewProductET = view.findViewById(R.id.name_of_new_product)
        nameOfNewProductTextRecognitionIB =
            view.findViewById(R.id.name_of_new_product_text_recognition)
        nameOfNewProductTextRecognitionIB.setOnClickListener {
            val textRecognitionIntent = Intent(
                requireActivity().applicationContext,
                TextRecognitionActivity::class.java
            )
            if (!hasCameraPermission()) {
                requestCameraPermissionForIntent(
                    textRecognitionIntent,
                    TEXT_RECOGNITION_REQUEST_CODE
                )
            } else {
                startActivityForResult(textRecognitionIntent, TEXT_RECOGNITION_REQUEST_CODE)
            }
        }
        categoryOfNewProductET = view.findViewById(R.id.category_of_new_product)
        categoryOfNewProductET.setText(categoryOfProduct)
        expDateOfNewProductET = view.findViewById(R.id.exp_date_of_new_product)
        expDateOfNewProductET.setOnClickListener {
            DatePickerDialogFragment().show(parentFragmentManager, "datePicker")
        }
        quantityOfNewProductNP = view.findViewById(R.id.quantity_of_new_product)
        quantityOfNewProductNP.apply {
            setMinValue(0)
            setMaxValue(Int.MAX_VALUE - 1)
            value = 1
            setOnValueChangedListener { picker: NumberPicker, _: Int, _: Int ->
                if (picker.value == 0) {
                    picker.value = 1
                }
            }
        }
        decreaseQuantityIB = view.findViewById(R.id.decrease_quantity)
        decreaseQuantityIB.setOnClickListener {
            var q = quantityOfNewProductNP.value
            if (q > 1) quantityOfNewProductNP.value = --q
        }
        increaseQuantityIB = view.findViewById(R.id.increase_quantity)
        increaseQuantityIB.setOnClickListener {
            var q = quantityOfNewProductNP.value
            quantityOfNewProductNP.value = ++q
        }
        unitTIL = view.findViewById(R.id.unit_text_input_layout)
        unitTV = view.findViewById(R.id.unit_text_view)
        unitTV.setAdapter(
            ArrayAdapter(
                ctx,
                R.layout.small_list_item,
                getAllStrings(ctx).toList()
            )
        )
        unitTV.setText(ctx.getString(R.string.Unit__PIECE), false)
        barcodeOfNewProductET = view.findViewById(R.id.barcode_of_new_product)
        barcodeOfNewProductScannerIB = view.findViewById(R.id.barcode_of_new_product_scanner)
        barcodeOfNewProductScannerIB.setOnClickListener {
            val barcodeScanIntent = Intent(
                requireActivity().applicationContext,
                BarcodeScannerActivity::class.java
            )
            if (!hasCameraPermission()) {
                requestCameraPermissionForIntent(barcodeScanIntent, BARCODE_SCAN_REQUEST_CODE)
            } else {
                startActivityForResult(barcodeScanIntent, BARCODE_SCAN_REQUEST_CODE)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pickMedia = registerForActivityResult<PickVisualMediaRequest, Uri>(
            ActivityResultContracts.PickVisualMedia()
        ) { uri: Uri? ->
            if (uri != null) {
                try {
                    val thisContext = this.requireContext()
                    val rot = CameraBitmapFixingExtensions.getImageRotation(thisContext, uri)
                    @Suppress("DEPRECATION")
                    imageOfProduct = MediaStore.Images.Media.getBitmap(thisContext.contentResolver, uri)
                    imageOfProduct?.let {
                        imageOfProduct = CameraBitmapFixingExtensions.rotate(it, rot)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                imageOfNewProductIV.setImageBitmap(imageOfProduct)
            }
        }

        categoryOfProduct = requireArguments().getString(GenericUtils.IntentBundleKeys.ADD_PRODUCT_DIALOG_FRAGMENT_CATEGORY_KEY)
        setHasOptionsMenu(true)
        parentFragmentManager.setFragmentResultListener(
            GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_REQUEST_KEY,
            this
        ) { _: String?, result: Bundle ->
            val trueResult =
                result.getBundle(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_RESULT_KEY)
            if (trueResult != null) {
                year = trueResult.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_YEAR_KEY)
                month = trueResult.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_MONTH_KEY)
                day = trueResult.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_DAY_KEY)
                val expDate = (day.toString() + "/" + (month!! + 1) + "/" + year)
                expDateOfNewProductET.setText(expDate)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        requireDialog().window?.apply {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            setLayout(width, height)
            setWindowAnimations(android.R.style.Animation_Dialog)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if ((requestCode != PICK_A_PHOTO_CODE) && (requestCode != REQUEST_IMAGE_CAPTURE) && (requestCode != BARCODE_SCAN_REQUEST_CODE) && (requestCode != TEXT_RECOGNITION_REQUEST_CODE)) {
            if (data != null) {
                data.putExtra("name", nameOfNewProductET.text.toString())
                val expDate = (day.toString() + "/" + month + "/" + year)
                data.putExtra("exp_date", expDate)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == PICK_A_PHOTO_CODE) {
                val photoUri = data.data
                if (photoUri != null) {
                    try {
                        val thisContext = requireContext()
                        val rot = CameraBitmapFixingExtensions.getImageRotation(thisContext, photoUri)
                        imageOfProduct = MediaStore.Images.Media.getBitmap(thisContext.contentResolver, photoUri)
                        imageOfProduct?.let {
                            imageOfProduct = CameraBitmapFixingExtensions.rotate(it, rot)
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    imageOfNewProductIV.setImageBitmap(imageOfProduct)
                }
            } else if ((requestCode == REQUEST_IMAGE_CAPTURE) && (resultCode == Activity.RESULT_OK)) {
                val extras = data.extras
                if (extras != null) {
                    imageOfProduct = extras["data"] as Bitmap?
                }
                imageOfNewProductIV.setImageBitmap(imageOfProduct)
            } else if ((requestCode == BARCODE_SCAN_REQUEST_CODE) && (resultCode == BarcodeScannerActivity.BARCODE_SCANNER_ACTIVITY_RESULT_CODE)) {
                barcodeDisplayValue = data.getStringExtra(GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_DISPLAY_VALUE_KEY)
                barcodeRawValue = data.getStringExtra(GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_VALUE_KEY)
                val rawBytes = data.getByteArrayExtra(GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_BYTES_KEY)
                if (rawBytes != null) {
                    for (rawByte in rawBytes) {
                        barcodeRawBytes.add(rawByte)
                    }
                }
                barcodeFormat = data.getIntExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_FORMAT_KEY,
                    Barcode.FORMAT_UNKNOWN
                )
                barcodeOfNewProductET.setText(barcodeDisplayValue)
            } else if ((requestCode == TEXT_RECOGNITION_REQUEST_CODE) && (resultCode == TextRecognitionActivity.TEXT_RECOGNITION_ACTIVITY_RESULT_CODE)) {
                nameOfProduct = data.getStringExtra(GenericUtils.IntentBundleKeys.TEXT_RECOGNITION_ACTIVITY_TEXT_KEY)
                nameOfNewProductET.setText(nameOfProduct)
            }
        }
    }

    companion object {
        const val PICK_A_PHOTO_CODE: Int = 1001
        const val REQUEST_IMAGE_CAPTURE: Int = 1002
        @Suppress("unused") const val CAMERA_PERMISSION_REQUEST_CODE: Int = 1003
        const val BARCODE_SCAN_REQUEST_CODE: Int = 1004
        const val TEXT_RECOGNITION_REQUEST_CODE: Int = 1005

        @JvmStatic
        fun newInstance(): AddProductDialogFragment =
            AddProductDialogFragment().apply {
                val args = Bundle()
                arguments = args
                setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay_Material)
            }
    }

}