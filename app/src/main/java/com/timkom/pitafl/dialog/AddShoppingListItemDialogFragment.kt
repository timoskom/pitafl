package com.timkom.pitafl.dialog

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.ImageButton
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.timkom.pitafl.R
import com.timkom.pitafl.data.room.ShoppingList
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.db.room.ProductEntity
import com.timkom.pitafl.db.room.ShoppingListItemEntity
import com.timkom.pitafl.task.coroutine.CheckIfShoppingListItemExistsTask
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks
import com.timkom.pitafl.task.coroutine.base.Result

class AddShoppingListItemDialogFragment : NonCancelableDialogFragment() {

    interface AddShoppingListItemListener {
        fun onFinishAddShoppingListItemDialog(itemName: String, list: String)
    }

    private lateinit var nameOfNewItemTV: MaterialAutoCompleteTextView
    private lateinit var cancelAddNewItemIB: ImageButton
    private lateinit var addNewItemIB: ImageButton
    private var isNameSet = false
    private var shoppingList: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        shoppingList = requireArguments().getString(SHOPPING_LIST_ARG_TAG)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.add_shopping_list_item_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nameOfNewItemTV = view.findViewById(R.id.name_of_new_item)
        cancelAddNewItemIB = view.findViewById(R.id.cancel_add_new_item)
        addNewItemIB = view.findViewById(R.id.add_new_item)

        val thisDialog = requireDialog()
        nameOfNewItemTV.requestFocus()
        thisDialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        addNewItemIB.apply {
            alpha = .5f
            isEnabled = false
            setOnClickListener {
                val nameOfNewItem = nameOfNewItemTV.getText().toString().trim()
                if (nameOfNewItem.isNotBlank()) {
                    val listener = requireActivity() as AddShoppingListItemListener
                    listener.onFinishAddShoppingListItemDialog(nameOfNewItem, shoppingList!!)
                }
                dismiss()
            }
        }

        nameOfNewItemTV.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                if (s.isNotBlank()) {
                    CheckIfShoppingListItemExistsTask(
                        this@AddShoppingListItemDialogFragment.requireContext(),
                        s.toString().trim(),
                        shoppingList!!,
                        CoroutineTasks.createResultListener(onSuccess = { exists: Boolean ->
                            if (!exists) {
                                addNewItemIB.apply {
                                    setImageResource(R.drawable.baseline_add_circle_green_48)
                                    alpha = 1f
                                    isEnabled = true
                                }
                                isNameSet = true
                            } else {
                                addNewItemIB.apply {
                                    setImageResource(R.drawable.baseline_add_circle_gray_48)
                                    alpha = .5f
                                    isEnabled = false
                                }
                                nameOfNewItemTV.error =
                                    "Item " + s.toString().trim() + " already exists!"
                                isNameSet = false
                            }
                        })
                    ).execute(true)
                } else {
                    addNewItemIB.apply {
                        setImageResource(R.drawable.baseline_add_circle_gray_48)
                        alpha = .5f
                        isEnabled = false
                    }
                    isNameSet = false
                }
            }
        })

        cancelAddNewItemIB.setOnClickListener { thisDialog.cancel() }

        CoroutineTasks.createWithContext(
            requireContext(),
            { ctx: Context ->
                val db = LocalDB.getInstance(ctx)
                val itemsNotInList = db.shoppingListItemDao().getAllNotInList(shoppingList!!)
                val expiredProducts = db.productDao().getExpired()
                val result = ArrayList<String>()
                result.addAll(itemsNotInList.map(ShoppingListItemEntity::name))
                result.addAll(expiredProducts.map(ProductEntity::name))
                Result.Success(result)
            },
            CoroutineTasks.createResultListener(onSuccess = { items: ArrayList<String> ->
                nameOfNewItemTV.setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        android.R.layout.simple_selectable_list_item,
                        items
                    )
                )
            })
        ).execute(true)
    }

    companion object {
        private const val SHOPPING_LIST_ARG_TAG = "SHOPPING_LIST_ARG"

        @JvmStatic
        fun newInstance(shoppingList: String): AddShoppingListItemDialogFragment =
            AddShoppingListItemDialogFragment().apply {
                val args = Bundle()
                args.putString(SHOPPING_LIST_ARG_TAG, shoppingList)
                arguments = args
            }

        @JvmStatic
        fun newInstance(shoppingList: ShoppingList): AddShoppingListItemDialogFragment =
            newInstance(shoppingList.name)
    }
}