package com.timkom.pitafl.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.core.widget.addTextChangedListener
import com.google.mlkit.vision.barcode.common.Barcode
import com.timkom.pitafl.activity.BarcodeScannerActivity
import com.timkom.pitafl.R
import com.timkom.pitafl.util.GenericUtils

class ChangeProductBarcodeDialogFragment : DialogFragmentWithCameraPermissions() {

    interface ChangeProductBarcodeListener {
        fun onFinishChangeProductBarcodeDialog(newBarcode: String, fromWhat: String)
    }

    private lateinit var newBarcodeOfProductET: EditText
    private lateinit var newBarcodeOfProductScannerIV: ImageView
    private lateinit var cancelNewBarcodeOfProductIB: ImageButton
    private lateinit var acceptNewBarcodeOfProductIB: ImageButton
    private var fromWhat: String? = null
    private var barcodeDisplayValue: String? = null
    private var barcodeRawValue: String? = null
    private val barcodeRawBytes = ArrayList<Byte>()
    private var barcodeFormat = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.change_barcode_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fromWhat = requireArguments().getString(
            GenericUtils.IntentBundleKeys.CHANGE_PRODUCT_BARCODE_DIALOG_FRAGMENT_CALL_SRC
        )
        newBarcodeOfProductET = view.findViewById(R.id.new_barcode_of_product)
        newBarcodeOfProductScannerIV = view.findViewById(R.id.new_barcode_of_product_scanner)
        cancelNewBarcodeOfProductIB = view.findViewById(R.id.cancel_new_barcode_of_product)
        acceptNewBarcodeOfProductIB = view.findViewById(R.id.accept_new_barcode_of_product)

        newBarcodeOfProductET.addTextChangedListener {
            val trimmed = it?.toString()?.trim()

            if (trimmed?.isNotBlank() == true) {
                acceptNewBarcodeOfProductIB.apply {
                    setImageResource(R.drawable.baseline_check_circle_green_48)
                    alpha = 1f
                    isEnabled = true
                }
            } else {
                acceptNewBarcodeOfProductIB.apply {
                    setImageResource(R.drawable.baseline_check_circle_gray_48)
                    alpha = .5f
                    isEnabled = false
                }
            }
        }

        newBarcodeOfProductScannerIV.setOnClickListener {
            val barcodeScanIntent = Intent(
                requireActivity().applicationContext,
                BarcodeScannerActivity::class.java
            )
            if (!hasCameraPermission()) {
                requestCameraPermissionForIntent(barcodeScanIntent, BARCODE_SCAN_REQUEST_CODE)
            } else {
                startActivityForResult(barcodeScanIntent, BARCODE_SCAN_REQUEST_CODE)
            }
        }

        cancelNewBarcodeOfProductIB.setOnClickListener { requireDialog().cancel() }

        acceptNewBarcodeOfProductIB.apply {
            setOnClickListener {
                val barcode = newBarcodeOfProductET.getText().toString().trim()
                if (barcode.isNotBlank()) {
                    val listener = requireActivity() as ChangeProductBarcodeListener
                    listener.onFinishChangeProductBarcodeDialog(barcode, fromWhat!!)
                }
                dismiss()
            }
            alpha = .5f
            isEnabled = false
        }
    }

    @Deprecated("Deprecated in Java")
    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if ((requestCode == BARCODE_SCAN_REQUEST_CODE) &&
                resultCode == BarcodeScannerActivity.BARCODE_SCANNER_ACTIVITY_RESULT_CODE) {
                barcodeDisplayValue = data.getStringExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_DISPLAY_VALUE_KEY
                )
                barcodeRawValue = data.getStringExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_VALUE_KEY
                )
                val rawBytes = data.getByteArrayExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_BYTES_KEY
                )
                for (rawByte in rawBytes!!) {
                    barcodeRawBytes.add(rawByte)
                }
                barcodeFormat = data.getIntExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_FORMAT_KEY,
                    Barcode.FORMAT_UNKNOWN
                )
                newBarcodeOfProductET.setText(barcodeDisplayValue)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): ChangeProductBarcodeDialogFragment =
            ChangeProductBarcodeDialogFragment().apply {
                val args = Bundle()
                arguments = args
            }
    }
}