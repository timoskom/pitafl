package com.timkom.pitafl.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.DrawableRes
import androidx.fragment.app.DialogFragment
import com.timkom.pitafl.R
import com.timkom.pitafl.task.coroutine.CheckIfCategoryExistsTask
import com.timkom.pitafl.task.coroutine.base.Result
import com.timkom.pitafl.util.BitmapDrawableExtensions
import com.timkom.pitafl.util.CameraBitmapFixingExtensions
import java.io.IOException

/**
 * The [DialogFragment] to add (create) a new category.
 */
class AddCategoryDialogFragment : DialogFragmentWithCameraPermissions() {

    @DrawableRes
    private val defaultResId = R.drawable.ic_launcher_background
    private lateinit var imageOfNewCategoryIV: ImageView
    private lateinit var nameOfNewCategoryET: EditText
    private lateinit var loadImageNewCategoryIB: ImageButton
    private lateinit var takeImageNewCategoryIB: ImageButton
    private lateinit var cancelNewCategoryIB: ImageButton
    private lateinit var addNewCategoryIB: ImageButton
    private var imageOfCategory: Bitmap? = null
    private var nameOfCategory: String? = null
    private var isNameSet = false
    private lateinit var pickMedia: ActivityResultLauncher<PickVisualMediaRequest>

    interface AddCategoryListener {
        fun onFinishAddCategoryDialog(categoryImage: Bitmap, categoryName: String)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pickMedia = registerForActivityResult<PickVisualMediaRequest, Uri>(
            ActivityResultContracts.PickVisualMedia()
        ) { uri: Uri? ->
            if (uri != null) {
                try {
                    val thisContext = this.requireContext()
                    val rot = CameraBitmapFixingExtensions.getImageRotation(thisContext, uri)
                    @Suppress("DEPRECATION")
                    imageOfCategory =
                        MediaStore.Images.Media.getBitmap(thisContext.contentResolver, uri)
                    imageOfCategory?.let {
                        imageOfCategory = CameraBitmapFixingExtensions.rotate(it, rot)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                imageOfNewCategoryIV.setImageBitmap(imageOfCategory)
                if (isNameSet) {
                    addNewCategoryIB.apply {
                        setImageResource(R.drawable.baseline_add_circle_green_48)
                        alpha = 1f
                        isEnabled = true
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.add_category_dialog, container)
    }

    @Suppress("DEPRECATION")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nameOfNewCategoryET = view.findViewById(R.id.name_of_new_category)
        imageOfNewCategoryIV = view.findViewById(R.id.image_of_new_category)
        imageOfNewCategoryIV.setImageResource(R.drawable.ic_add_category_placeholder_c10)
        loadImageNewCategoryIB = view.findViewById(R.id.load_image_new_category)
        takeImageNewCategoryIB = view.findViewById(R.id.take_image_new_category)
        cancelNewCategoryIB = view.findViewById(R.id.cancel_new_category)
        addNewCategoryIB = view.findViewById(R.id.add_new_category)

        val thisDialog = requireDialog()
        nameOfNewCategoryET.requestFocus()
        thisDialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        addNewCategoryIB.setAlpha(.5f)
        addNewCategoryIB.setEnabled(false)

        nameOfNewCategoryET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                if (s.isNotBlank()) {
                    CheckIfCategoryExistsTask(
                        this@AddCategoryDialogFragment.requireContext(),
                        s.toString().trim()
                    ) { result ->
                        if (result is Result.Success<Boolean>) {
                            if (!result.data) {
                                if (imageOfCategory != null) {
                                    addNewCategoryIB.apply {
                                        setImageResource(R.drawable.baseline_add_circle_green_48)
                                        setAlpha(1f)
                                        setEnabled(true)
                                    }
                                }
                                isNameSet = true
                            } else {
                                addNewCategoryIB.apply {
                                    setImageResource(R.drawable.baseline_add_circle_gray_48)
                                    setAlpha(.5f)
                                    setEnabled(false)
                                }
                                nameOfNewCategoryET.error =
                                    "Category ${s.toString().trim()} already exists!"
                                isNameSet = false
                            }
                        } else if (result is Result.Error) {
                            result.exception.printStackTrace()
                        }
                    }.execute(true)
                } else {
                    addNewCategoryIB.apply {
                        setImageResource(R.drawable.baseline_add_circle_gray_48)
                        setAlpha(.5f)
                        setEnabled(false)
                    }
                    isNameSet = false
                }
            }
        })
        imageOfNewCategoryIV.setOnClickListener { v: View ->
            val items = arrayOf<CharSequence>(
                "Take Photo",
                "Choose from Gallery",
                "Cancel"
            )
            val builder = AlertDialog.Builder(this.context)
                .setItems(items) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                    if (items[which] == "Take Photo") {
                        dialog.dismiss()
                        val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (!hasCameraPermission()) {
                            requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
                        } else {
                            if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                            }
                        }
                    } else if (items[which] == "Choose from Gallery") {
                        dialog.dismiss()
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                            val loadPicIntent =
                                Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                )
                            if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                            }
                        } else {
                            pickMedia.launch(
                                PickVisualMediaRequest.Builder()
                                    .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                                    .build()
                            )
                        }
                    } else {
                        dialog.cancel()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }
        loadImageNewCategoryIB.setOnClickListener { v: View ->
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                val loadPicIntent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                }
            } else {
                pickMedia.launch(
                    PickVisualMediaRequest.Builder()
                        .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                        .build()
                )
            }
        }
        takeImageNewCategoryIB.setOnClickListener { v: View ->
            val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (!hasCameraPermission()) {
                requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
            } else {
                if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
        cancelNewCategoryIB.setOnClickListener { thisDialog.cancel() }
        addNewCategoryIB.setOnClickListener {
            nameOfCategory = nameOfNewCategoryET.getText().toString().trim()
            nameOfCategory?.let { name ->
                if (name.isNotBlank()) {
                    val listener = requireActivity() as AddCategoryListener
                    listener.onFinishAddCategoryDialog(
                        imageOfCategory ?: BitmapDrawableExtensions
                            .getBitmap(requireContext().applicationContext, defaultResId),
                        name
                    )
                }
            }
            dismiss()
        }
    }

    @Suppress("OVERRIDE_DEPRECATION", "DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == PICK_A_PHOTO_CODE) {
                val photoUri = data.data
                if (photoUri != null) {
                    try {
                        val thisContext = this.requireContext()
                        val rot =
                            CameraBitmapFixingExtensions.getImageRotation(thisContext, photoUri)
                        imageOfCategory =
                            MediaStore.Images.Media.getBitmap(thisContext.contentResolver, photoUri)
                        imageOfCategory?.let {
                            imageOfCategory = CameraBitmapFixingExtensions.rotate(it, rot)
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    imageOfNewCategoryIV.setImageBitmap(imageOfCategory)
                    if (isNameSet) {
                        addNewCategoryIB.apply {
                            setImageResource(R.drawable.baseline_add_circle_green_48)
                            alpha = 1f
                            isEnabled = true
                        }
                    }
                }
            } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
                val extras = data.extras
                if (extras != null) {
                    imageOfCategory = extras["data"] as Bitmap?
                    imageOfNewCategoryIV.setImageBitmap(imageOfCategory)
                    if (isNameSet) {
                        addNewCategoryIB.apply {
                            setImageResource(R.drawable.baseline_add_circle_green_48)
                            alpha = 1f
                            isEnabled = true
                        }
                    }
                }
            }
        }
    }

    companion object {
        const val PICK_A_PHOTO_CODE: Int = 1001
        const val REQUEST_IMAGE_CAPTURE: Int = 1002

        @JvmStatic
        fun newInstance(): AddCategoryDialogFragment =
            AddCategoryDialogFragment().apply {
                val args = Bundle()
                arguments = args
            }
    }

}