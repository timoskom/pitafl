package com.timkom.pitafl.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.widget.addTextChangedListener
import com.timkom.pitafl.R
import com.timkom.pitafl.util.GenericUtils

class ChangeNameDialogFragment : NonCancelableDialogFragment() {

    interface ChangeNameListener {
        fun onFinishChangeNameListener(newName: String, fromWhat: Int)
    }

    private lateinit var newNameET: EditText
    private lateinit var cancelChangeNameIB: AppCompatImageButton
    private lateinit var acceptNewNameIB: AppCompatImageButton
    private var fromWhat: Int = 0
    private var currentName: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.change_name_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = requireArguments()
        fromWhat = args.getInt(
            GenericUtils.IntentBundleKeys.CHANGE_NAME_DIALOG_FRAGMENT_CALL_SRC
        )
        currentName = args.getString(
            GenericUtils.IntentBundleKeys.CHANGE_NAME_DIALOG_FRAGMENT_CURRENT_NAME_KEY
        ) ?: ""
        newNameET = view.findViewById(R.id.new_name_edit_text)
        cancelChangeNameIB = view.findViewById(R.id.cancel_change_name_image_button)
        acceptNewNameIB = view.findViewById(R.id.accept_new_name_image_button)

        requireDialog().window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        newNameET.requestFocus()

        if (currentName.isNotBlank()) {
            newNameET.setText(currentName)
        }

        newNameET.addTextChangedListener {
            val trimmed = it?.toString()?.trim()

            if (trimmed?.isBlank() == true || trimmed == currentName) {
                acceptNewNameIB.apply {
                    setImageResource(R.drawable.baseline_check_circle_gray_48)
                    alpha = .5f
                    isEnabled = false
                }
            } else {
                acceptNewNameIB.apply {
                    setImageResource(R.drawable.baseline_check_circle_green_48)
                    alpha = 1f
                    isEnabled = true
                }
            }

            if (trimmed == currentName) {
                newNameET.error = "Please, type a different name than the original!"
            }
        }

        cancelChangeNameIB.setOnClickListener { requireDialog().cancel() }

        acceptNewNameIB.setOnClickListener {
            val newName = newNameET.text.toString().trim()
            if (newName.isNotBlank()) {
                val listener = requireActivity() as ChangeNameListener
                listener.onFinishChangeNameListener(newName, fromWhat)
            }
            dismiss()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): ChangeNameDialogFragment =
            ChangeNameDialogFragment().apply {
                val args = Bundle()
                arguments = args
            }
    }

}