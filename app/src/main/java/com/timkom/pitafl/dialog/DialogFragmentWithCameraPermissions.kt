package com.timkom.pitafl.dialog

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.ref.WeakReference

@Suppress("DEPRECATION", "MemberVisibilityCanBePrivate", "OVERRIDE_DEPRECATION")
open class DialogFragmentWithCameraPermissions : NonCancelableDialogFragment() {

    private var requestIntent: WeakReference<Intent>? = null
    private var intentRequestCode = 0

    protected fun hasCameraPermission(): Boolean =
        ContextCompat.checkSelfPermission(
            this.requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED

    protected fun requestCameraPermission() =
        requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_REQUEST_CODE)

    protected fun requestCameraPermissionForIntent(pmIntent: Intent, pmIntentRequestCode: Int) {
        requestIntent = WeakReference(pmIntent)
        intentRequestCode = pmIntentRequestCode
        requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestIntent?.get()?.let { intent ->
                    if (intentRequestCode == 0)
                        startActivity(intent)
                    else
                        startActivityForResult(intent, intentRequestCode)
                }
            } else {
                MaterialAlertDialogBuilder(this.requireContext())
                    .setTitle("Permission Required")
                    .setMessage("PITAFL needs to access the camera to process barcodes")
                    .setPositiveButton(
                        android.R.string.ok
                    ) { _: DialogInterface, _: Int -> requestCameraPermission() }
                    .setNegativeButton(
                        android.R.string.cancel
                    ) { dialog: DialogInterface, _: Int -> dialog.cancel() }
                    .create().show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    companion object {
        const val CAMERA_PERMISSION_REQUEST_CODE: Int = 11001
        const val BARCODE_SCAN_REQUEST_CODE: Int = 11002
    }

}