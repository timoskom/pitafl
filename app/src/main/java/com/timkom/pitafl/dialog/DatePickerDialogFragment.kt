package com.timkom.pitafl.dialog

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import com.timkom.pitafl.util.GenericUtils
import java.util.Calendar

class DatePickerDialogFragment : NonCancelableDialogFragment(), DatePickerDialog.OnDateSetListener {
    private var fromWhat: String? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (arguments != null) fromWhat =
            requireArguments().getString(
                GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_CALL_SRC
            )
        val calendar = Calendar.getInstance()
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH]
        val day = calendar[Calendar.DAY_OF_MONTH]

        return DatePickerDialog(requireActivity(), this, year, month, day)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        val result = createResultBundle(year, month, dayOfMonth)
        if (fromWhat != null) result.putString(
            GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_CALL_SRC,
            fromWhat
        )
        parentFragmentManager.setFragmentResult(
            GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_REQUEST_KEY,
            result
        )
    }

    companion object {
        @JvmStatic
        private fun createResultBundle(year: Int, month: Int, dayOfMonth: Int): Bundle {
            val result = Bundle()
            val trueResult = Bundle()
            trueResult.putInt(
                GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_YEAR_KEY,
                year
            )
            trueResult.putInt(
                GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_MONTH_KEY,
                month
            )
            trueResult.putInt(
                GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_DAY_KEY,
                dayOfMonth
            )
            result.putBundle(
                GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_RESULT_KEY,
                trueResult
            )
            return result
        }
    }
}
