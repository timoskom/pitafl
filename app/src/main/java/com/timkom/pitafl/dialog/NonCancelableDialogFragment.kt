package com.timkom.pitafl.dialog

import androidx.fragment.app.DialogFragment

/**
 * A non-cancelable [DialogFragment] (no direct use, inherited only)
 */
open class NonCancelableDialogFragment : DialogFragment() {

    init {
        super.setCancelable(false) // Specify that the dialog is not cancelable
    }

    final override fun setCancelable(cancelable: Boolean) {
        super.setCancelable(false) // Force non-cancelable
    }

}
