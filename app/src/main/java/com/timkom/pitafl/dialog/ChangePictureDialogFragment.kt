package com.timkom.pitafl.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.DialogFragment
import com.timkom.pitafl.R
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.db.room.LocalDB.Companion.getInstance
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks.createResultListener
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks.createWithContext
import com.timkom.pitafl.task.coroutine.base.Result
import com.timkom.pitafl.util.BitmapDrawableExtensions
import com.timkom.pitafl.util.CameraBitmapFixingExtensions
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.SVGFileParser
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

/**
 * The [DialogFragment] to change the picture of a category/product.
 */
class ChangePictureDialogFragment : DialogFragmentWithCameraPermissions() {

    private lateinit var newImageIV: ImageView
    private lateinit var loadNewImageIB: ImageButton
    private lateinit var takeNewImageIB: ImageButton
    private lateinit var cancelNewIB: ImageButton
    private lateinit var acceptNewIB: ImageButton
    private var newImage: Bitmap? = null
    private var fromWhat: String? = null
    private var defaultImage: Bitmap? = null
    private lateinit var pickMedia: ActivityResultLauncher<PickVisualMediaRequest>

    interface ChangePictureListener {
        fun onFinishChangePictureDialog(newImage: Bitmap, fromWhat: String)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.change_picture_dialog, container)
    }

    @Suppress("DEPRECATION")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fromWhat =
            requireArguments().getString(GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_CALL_SRC)
        newImageIV = view.findViewById(R.id.new_image)

        val fromTable = requireArguments().getString(
            GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_TABLE_SRC,
            ""
        )

        if (listOf(LocalDB.TABLE_NAME_PRODUCT_CATEGORIES, LocalDB.TABLE_NAME_PRODUCTS)
                .contains(fromTable)
        ) {
            val callSrc = requireArguments().getString(
                GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_CALL_SRC
            )
            if (callSrc != null) {
                createWithContext(
                    requireContext(),
                    { ctx: Context ->
                        val db = getInstance(ctx)
                        var picturePath: String? = null
                        if (fromTable == LocalDB.TABLE_NAME_PRODUCT_CATEGORIES) {
                            picturePath =
                                db.productCategoryDao().getPictureOfCategory(callSrc).picture
                        } else if (fromTable == LocalDB.TABLE_NAME_PRODUCTS) {
                            picturePath = db.productDao().getPictureOfProduct(callSrc).picture
                        }
                        Result.Success(picturePath)
                    },
                    createResultListener(onSuccess = { picturePath: String? ->
                        if (picturePath != null) {
                            val image = File(picturePath)
                            if (image.toPath().toString().endsWith(".svg")) {
                                val svgParser = SVGFileParser(image.toPath())
                                try {
                                    val pic = svgParser.getAsPictureDrawable()
                                    defaultImage = BitmapDrawableExtensions.getBitmap(pic)
                                } catch (e: FileNotFoundException) {
                                    e.printStackTrace()
                                }
                            } else {
                                defaultImage = BitmapFactory.decodeFile(
                                    image.absolutePath,
                                    BitmapFactory.Options()
                                )
                            }
                        }
                        newImageIV.setImageBitmap(defaultImage)
                    })
                ).execute(true)
            }
        }

        newImageIV.setOnClickListener { v: View ->
            val items = arrayOf<CharSequence>(
                "Take Photo",
                "Choose from Gallery",
                "Cancel"
            )
            val builder = AlertDialog.Builder(this.context)
                .setItems(items) { dialog: DialogInterface, which: Int ->
                    if (items[which] == "Take Photo") {
                        dialog.dismiss()
                        val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (!hasCameraPermission()) {
                            requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
                        } else {
                            if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                            }
                        }
                    } else if (items[which] == "Choose from Gallery") {
                        dialog.dismiss()
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                            val loadPicIntent =
                                Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                )
                            if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                                startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                            }
                        } else {
                            pickMedia.launch(
                                PickVisualMediaRequest.Builder()
                                    .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                                    .build()
                            )
                        }
                    } else {
                        dialog.cancel()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }
        loadNewImageIB = view.findViewById(R.id.load_new_image)
        loadNewImageIB.setOnClickListener { v: View ->
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                val loadPicIntent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                if (loadPicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(loadPicIntent, PICK_A_PHOTO_CODE)
                }
            } else {
                pickMedia.launch(
                    PickVisualMediaRequest.Builder()
                        .setMediaType(ActivityResultContracts.PickVisualMedia.ImageOnly)
                        .build()
                )
            }
        }
        takeNewImageIB = view.findViewById(R.id.take_new_image)
        takeNewImageIB.setOnClickListener { v: View ->
            val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (!hasCameraPermission()) {
                requestCameraPermissionForIntent(takePicIntent, REQUEST_IMAGE_CAPTURE)
            } else {
                if (takePicIntent.resolveActivity(v.context.packageManager) != null) {
                    startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
        cancelNewIB = view.findViewById(R.id.cancel_new_image)
        cancelNewIB.setOnClickListener { requireDialog().cancel() }
        acceptNewIB = view.findViewById(R.id.accept_new_image)
        acceptNewIB.apply {
            setOnClickListener {
                fromWhat?.let { what ->
                    val listener = requireActivity() as ChangePictureListener
                    if (newImage == null) {
                        newImage = defaultImage
                    }
                    newImage?.let { image ->
                        listener.onFinishChangePictureDialog(image, what)
                        dismiss()
                    }
                }
            }
            setAlpha(.5f)
            setEnabled(false)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pickMedia = registerForActivityResult<PickVisualMediaRequest, Uri>(
            ActivityResultContracts.PickVisualMedia()
        ) { uri: Uri? ->
            if (uri != null) {
                try {
                    val thisContext = this.requireContext()
                    val rot = CameraBitmapFixingExtensions.getImageRotation(thisContext, uri)
                    @Suppress("DEPRECATION")
                    newImage = MediaStore.Images.Media.getBitmap(thisContext.contentResolver, uri)
                    newImage?.let { newImage = CameraBitmapFixingExtensions.rotate(it, rot) }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                newImageIV.setImageBitmap(newImage)
                acceptNewIB.apply {
                    setImageResource(R.drawable.baseline_add_circle_green_48)
                    alpha = 1f
                    isEnabled = true
                }
            }
        }
    }

    @Suppress("OVERRIDE_DEPRECATION", "DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == PICK_A_PHOTO_CODE) {
                val photoUri = data.data
                if (photoUri != null) {
                    try {
                        val rot = CameraBitmapFixingExtensions.getImageRotation(
                            this.requireContext(),
                            photoUri
                        )
                        newImage = MediaStore.Images.Media.getBitmap(
                            requireContext().contentResolver,
                            photoUri
                        )
                        newImage?.let { CameraBitmapFixingExtensions.rotate(it, rot) }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    newImageIV.setImageBitmap(newImage)
                    acceptNewIB.apply {
                        setImageResource(R.drawable.baseline_add_circle_green_48)
                        alpha = 1f
                        isEnabled = true
                    }
                }
            } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
                val extras = data.extras
                if (extras != null) {
                    newImage = extras["data"] as Bitmap?
                    newImageIV.setImageBitmap(newImage)
                    acceptNewIB.apply {
                        setImageResource(R.drawable.baseline_add_circle_green_48)
                        alpha = 1f
                        isEnabled = true
                    }
                }
            }
        }
    }

    companion object {
        const val PICK_A_PHOTO_CODE: Int = 1001
        const val REQUEST_IMAGE_CAPTURE: Int = 1002

        @JvmStatic
        fun newInstance(): ChangePictureDialogFragment =
            ChangePictureDialogFragment().apply {
                val args = Bundle()
                arguments = args
            }
    }

}
