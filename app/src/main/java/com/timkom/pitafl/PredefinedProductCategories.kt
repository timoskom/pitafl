package com.timkom.pitafl

/**
 * Predefined product categories
 */
enum class PredefinedProductCategories {
    PASTA,
    MEAT_FISH,
    PRODUCE,
    LEGUMES,
    DRINKS,
    CEREALS,
    SWEET,
    VARIOUS
}
