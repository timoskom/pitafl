package com.timkom.pitafl

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timkom.pitafl.adapter.AdSupportingAdapter.ViewHolder
import com.timkom.pitafl.adapter.ProductsAdapter

fun ProductsAdapter.initializeAdView(viewGroup: ViewGroup): ViewHolder.AdViewHolder {
    val bannerLayout: View = LayoutInflater.from(viewGroup.context)
        .inflate(R.layout.admob_list_item_container, viewGroup, false)
    return ViewHolder.AdViewHolder(bannerLayout)
}