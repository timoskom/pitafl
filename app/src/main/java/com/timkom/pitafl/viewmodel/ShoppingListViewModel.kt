package com.timkom.pitafl.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.timkom.pitafl.data.room.ShoppingList
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.data.room.util.toData
import com.timkom.pitafl.db.room.LocalDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ShoppingListViewModel : ViewModel() {
    private val _shoppingLists = MutableLiveData<Map<ShoppingList, List<ShoppingListItem>>>()
    val shoppingLists: LiveData<Map<ShoppingList, List<ShoppingListItem>>> = _shoppingLists

    fun loadData(context: Context) {
        viewModelScope.launch {
            val data = withContext(Dispatchers.IO) {
                val db = LocalDB.getInstance(context.applicationContext)
                Pair(
                    db.shoppingListDao().getAll().map { it.toData() },
                    db.shoppingListItemDao().getAll().map { it.toData() }
                )
            }
            _shoppingLists.value = data.first.associateWith { data.second }
        }
    }
}