package com.timkom.pitafl.db.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.timkom.pitafl.BuildConfig
import org.jetbrains.annotations.Range
import java.util.concurrent.Executors

@Database(
    entities = [
        ProductCategoryEntity::class,
        ProductEntity::class,
        ShoppingListEntity::class,
        ShoppingListItemEntity::class
    ],
    version = BuildConfig.DB_VERSION,
    exportSchema = true
)
abstract class LocalDB : RoomDatabase() {

    abstract fun productCategoryDao(): ProductCategoryDao

    abstract fun productDao(): ProductDao

    abstract fun shoppingListDao(): ShoppingListDao

    abstract fun shoppingListItemDao(): ShoppingListItemDao

    companion object {

        class SQLiteDBMigration(
            startVersion: @Range(from = 1, to = 2) Int,
            endVersion: @Range(from = 2, to = 3) Int
        ) : Migration(startVersion, endVersion) {
            override fun migrate(db: SupportSQLiteDatabase) {
                val statements = OldDBInfo.OLD_DB_ON_UPGRADE_SQL_STATEMENTS[endVersion]
                statements?.forEach {
                    db.execSQL(it)
                }
            }
        }

        @JvmStatic
        val DB_NAME = "PITAFL_local.db"

        const val TABLE_NAME_PRODUCT_CATEGORIES = "product_categories"
        const val TABLE_NAME_PRODUCTS = "products"
        const val TABLE_NAME_SHOPPING_LISTS = "shopping_lists"
        const val TABLE_NAME_SHOPPING_LIST_ITEMS = "shopping_list_items"

        @JvmStatic
        val MIGRATION_1_2 = SQLiteDBMigration(1, 2)

        @JvmStatic
        val MIGRATION_2_3 = SQLiteDBMigration(2, 3)

        @JvmStatic
        val MIGRATION_3_4 = object : Migration(3, 4) {
            override fun migrate(db: SupportSQLiteDatabase) {
                migrateProductCategories(db)
                migrateProducts(db)
                migrateShoppingLists(db)
                migrateShoppingListItems(db)
            }

            private fun migrateProductCategories(db: SupportSQLiteDatabase) {
                val tempTableName = TABLE_NAME_PRODUCT_CATEGORIES + "_tmp"
                db.beginTransaction()
                try {
                    db.execSQL("CREATE TABLE IF NOT EXISTS `$tempTableName` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `picture` TEXT NOT NULL)")
                    db.execSQL("INSERT INTO `$tempTableName` SELECT * FROM `${OldDBInfo.OLD_PRODUCT_CATEGORIES_TABLE_NAME}`")
                    db.execSQL("DROP TABLE `${OldDBInfo.OLD_PRODUCT_CATEGORIES_TABLE_NAME}`")
                    db.execSQL("ALTER TABLE `$tempTableName` RENAME TO `${TABLE_NAME_PRODUCT_CATEGORIES}`")
                    db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS `index_product_categories_name` ON `${TABLE_NAME_PRODUCT_CATEGORIES}` (`name`)")
                    db.setTransactionSuccessful()
                } finally {
                    db.endTransaction()
                }
            }

            private fun migrateProducts(db: SupportSQLiteDatabase) {
                val tempTableName = TABLE_NAME_PRODUCTS + "_tmp"
                db.beginTransaction()
                try {
                    db.execSQL("CREATE TABLE IF NOT EXISTS `$tempTableName` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `category` TEXT NOT NULL, `barcode` TEXT NOT NULL, `expirationDate` TEXT NOT NULL, `picture` TEXT NOT NULL, `quantity` INTEGER NOT NULL, `unitOfQuantity` TEXT NOT NULL, `expirationMark` TEXT NOT NULL, FOREIGN KEY(`category`) REFERENCES `product_categories`(`name`) ON UPDATE CASCADE ON DELETE CASCADE )")
                    db.execSQL("INSERT INTO `$tempTableName` SELECT * FROM `${OldDBInfo.OLD_PRODUCTS_TABLE_NAME}`")
                    db.execSQL("DROP TABLE `${OldDBInfo.OLD_PRODUCTS_TABLE_NAME}`")
                    db.execSQL("ALTER TABLE `$tempTableName` RENAME TO `${TABLE_NAME_PRODUCTS}`")
                    db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS `index_products_name_barcode` ON `${TABLE_NAME_PRODUCTS}` (`name`, `barcode`)")
                    db.setTransactionSuccessful()
                } finally {
                    db.endTransaction()
                }
            }

            private fun migrateShoppingLists(db: SupportSQLiteDatabase) {
                val tempTableName = TABLE_NAME_SHOPPING_LISTS + "_tmp"
                db.beginTransaction()
                try {
                    db.execSQL("CREATE TABLE IF NOT EXISTS `${tempTableName}` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL)")
                    db.execSQL("INSERT INTO `$tempTableName` SELECT * FROM `${OldDBInfo.OLD_SHOPPING_LIST_ITEMS_TABLE_NAME}`")
                    db.execSQL("DROP TABLE `${OldDBInfo.OLD_SHOPPING_LIST_ITEMS_TABLE_NAME}`")
                    db.execSQL("ALTER TABLE `$tempTableName` RENAME TO `${TABLE_NAME_SHOPPING_LISTS}`")
                    db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS `index_shopping_lists_name` ON `${TABLE_NAME_SHOPPING_LISTS}` (`name`)")
                    db.setTransactionSuccessful()
                } finally {
                    db.endTransaction()
                }
            }

            private fun migrateShoppingListItems(db: SupportSQLiteDatabase) {
                val tempTableName = TABLE_NAME_SHOPPING_LIST_ITEMS + "_tmp"
                db.beginTransaction()
                try {
                    db.execSQL("CREATE TABLE IF NOT EXISTS `${tempTableName}` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `isChecked` INTEGER NOT NULL, `list` TEXT NOT NULL)")
                    db.execSQL("INSERT INTO `$tempTableName` SELECT * FROM `${OldDBInfo.OLD_SHOPPING_LIST_ITEMS_TABLE_NAME}`")
                    db.execSQL("DROP TABLE `${OldDBInfo.OLD_SHOPPING_LIST_ITEMS_TABLE_NAME}`")
                    db.execSQL("ALTER TABLE `$tempTableName` RENAME TO `${TABLE_NAME_SHOPPING_LIST_ITEMS}`")
                    db.setTransactionSuccessful()
                } finally {
                    db.endTransaction()
                }
            }
        }

        @JvmStatic
        private var INSTANCE: LocalDB? = null

        @JvmStatic
        @Synchronized
        fun getInstance(context: Context): LocalDB {
            if (INSTANCE == null) {
                val dbBuilder = Room
                    .databaseBuilder(
                        context.applicationContext,
                        LocalDB::class.java,
                        DB_NAME
                    )
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4)
                    .fallbackToDestructiveMigrationOnDowngrade()

                if (BuildConfig.DEBUG) {
                    dbBuilder
                        .setQueryCallback(
                            queryCallback =  { sqlQuery, bindArgs ->
                                println("SQL Query: $sqlQuery --> SQL Args: $bindArgs")
                            },
                            executor = Executors.newSingleThreadExecutor()
                        )
                        // TODO remove in future version
                        .allowMainThreadQueries()
                }

                INSTANCE = dbBuilder.build()
            }

            return INSTANCE!!
        }

    }

}