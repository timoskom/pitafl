package com.timkom.pitafl.db.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface ShoppingListItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(shoppingListItem: ShoppingListItemEntity)

    @Delete
    fun delete(shoppingListItem: ShoppingListItemEntity): Int

    @Query("DELETE FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS
    } WHERE name = :name AND list = :list")
    fun deleteByNameFromList(name: String, list: String)

    @Query("DELETE FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS
    } WHERE list = :list")
    fun deleteAllFromList(list: String)

    @Update
    fun update(shoppingListItem: ShoppingListItemEntity)

    @Query("SELECT EXISTS(SELECT * FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS
    } WHERE name = :name LIMIT 1)")
    fun exists(name: String): Boolean

    @Query("SELECT EXISTS(SELECT * FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS
    } WHERE name = :name AND list = :list LIMIT 1)")
    fun existsInList(name: String, list: String): Boolean

    @Query("SELECT * FROM ${LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS}")
    fun getAll(): List<ShoppingListItemEntity>

    @Query("SELECT * FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS
    } WHERE list = :list")
    fun getAllInList(list: String): List<ShoppingListItemEntity>

    @Query("SELECT * FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS
    } WHERE list != :list")
    fun getAllNotInList(list: String): List<ShoppingListItemEntity>

}