package com.timkom.pitafl.db.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface ProductDao {

    data class ProductName(val name: String)

    data class ProductExpiration(val name: String, val expirationDate: String)

    data class Picture(val picture: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(product: ProductEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMany(vararg product: ProductEntity)

    @Delete
    fun delete(product: ProductEntity): Int

    @Query("DELETE FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE name = :name AND category = :category")
    fun delete(name: String, category: String)

    @Query("DELETE FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE category = :category")
    fun deleteAllForCategory(category: String)

    @Update
    fun update(product: ProductEntity)

    @Query("UPDATE ${
        LocalDB.TABLE_NAME_PRODUCTS
    } SET name = :newName WHERE id = :id")
    fun updateName(newName: String, id: Int)

    @Query("UPDATE ${
        LocalDB.TABLE_NAME_PRODUCTS
    } SET picture = :picture WHERE name = :name")
    fun updatePicture(picture: String, name: String)

    @Query("UPDATE ${
        LocalDB.TABLE_NAME_PRODUCTS
    } SET expirationDate = :expirationDate WHERE name = :name")
    fun updateExpirationDate(expirationDate: String, name: String)

    @Query("UPDATE ${
        LocalDB.TABLE_NAME_PRODUCTS
    } SET barcode = :barcode WHERE name = :name")
    fun updateBarcode(barcode: String, name: String)

    @Query("UPDATE ${
        LocalDB.TABLE_NAME_PRODUCTS
    } SET expirationMark = :expirationMark WHERE name = :name")
    fun updateExpirationMark(expirationMark: String, name: String)

    @Query("UPDATE ${
        LocalDB.TABLE_NAME_PRODUCTS
    } SET quantity = :newQuantity WHERE name = :product")
    fun updateQuantity(newQuantity: Int, product: String)

    @Query("SELECT EXISTS(SELECT * FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE name = :name LIMIT 1)")
    fun exists(name: String): Boolean

    @Query("SELECT * FROM ${LocalDB.TABLE_NAME_PRODUCTS}")
    fun getAll(): List<ProductEntity>

    @Query("SELECT * FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE category = :category")
    fun getAllForCategory(category: String): List<ProductEntity>

    @Query("SELECT * FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE expirationMark = \"EXPIRES_SOON\"")
    fun getExpiresSoon(): List<ProductEntity>

    @Query("SELECT * FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE expirationMark = \"EXPIRED\"")
    fun getExpired(): List<ProductEntity>

    @Query("SELECT * FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE expirationMark = \"NOT_EXPIRED\"")
    fun getNotExpired(): List<ProductEntity>

    @Query("SELECT name, expirationDate FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    }")
    fun getExpirations(): List<ProductExpiration>

    @Query("SELECT name, expirationDate FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE category = :category")
    fun getExpirationsOfCategory(category: String): List<ProductExpiration>

    @Query("SELECT name FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE expirationMark = \"EXPIRED\"")
    fun getExpiredNames(): List<ProductName>

    @Query("SELECT picture FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE name = :product LIMIT 1")
    fun getPictureOfProduct(product: String): Picture

    @Query("SELECT * FROM ${
        LocalDB.TABLE_NAME_PRODUCTS
    } WHERE lower(name) LIKE lower(:query) OR lower(category) LIKE lower(:query)")
    fun search(query: String): List<ProductEntity>

}