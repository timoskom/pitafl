package com.timkom.pitafl.db.room

import com.timkom.pitafl.data.projections.ProductProjection
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

private fun ProductDao.updateExpirationMarksOf(
    expirations: List<ProductDao.ProductExpiration>,
    notifyIfDays: Int
) {
    expirations.forEach { expiration ->
        val calendar = Calendar.getInstance()
        try {
            val expirationDate = SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US)
                .parse(expiration.expirationDate)

            if (calendar.time.after(expirationDate)) {
                updateExpirationMark(
                    ProductProjection.ExpirationMark.EXPIRED.name,
                    expiration.name
                )
            } else {
                calendar.add(Calendar.DATE, notifyIfDays)
                if (calendar.time.after(expirationDate)) {
                    updateExpirationMark(
                        ProductProjection.ExpirationMark.EXPIRES_SOON.name,
                        expiration.name
                    )
                } else {
                    updateExpirationMark(
                        ProductProjection.ExpirationMark.NOT_EXPIRED.name,
                        expiration.name
                    )
                }
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }
}

fun ProductDao.updateExpirationMarks(notifyIfDays: Int) =
    updateExpirationMarksOf(getExpirations(), notifyIfDays)

fun ProductDao.updateExpirationMarksOfCategory(category: String, notifyIfDays: Int) =
    updateExpirationMarksOf(getExpirationsOfCategory(category), notifyIfDays)

fun ProductDao.getExpiresSoonUpdated(notifyIfDays: Int): List<ProductEntity> {
    updateExpirationMarks(notifyIfDays)
    return getExpiresSoon()
}