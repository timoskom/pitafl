package com.timkom.pitafl.db.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface ProductCategoryDao {

    data class Picture(val picture: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(productCategory: ProductCategoryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMany(vararg productCategory: ProductCategoryEntity)

    @Delete
    fun delete(productCategory: ProductCategoryEntity): Int

    @Query("DELETE FROM ${
        LocalDB.TABLE_NAME_PRODUCT_CATEGORIES
    } WHERE name = :name")
    fun deleteByName(name: String)

    @Update
    fun update(productCategory: ProductCategoryEntity)

    @Query("UPDATE ${
        LocalDB.TABLE_NAME_PRODUCT_CATEGORIES
    } SET picture = :picture WHERE name = :category")
    fun updatePicture(picture: String, category: String)

    @Query("SELECT EXISTS(SELECT * FROM ${
        LocalDB.TABLE_NAME_PRODUCT_CATEGORIES
    } WHERE name = :category LIMIT 1)")
    fun exists(category: String): Boolean

    @Query("SELECT * FROM ${LocalDB.TABLE_NAME_PRODUCT_CATEGORIES}")
    fun getAll(): List<ProductCategoryEntity>

    @Query("SELECT picture FROM ${
        LocalDB.TABLE_NAME_PRODUCT_CATEGORIES
    } WHERE name = :category LIMIT 1")
    fun getPictureOfCategory(category: String): Picture

}