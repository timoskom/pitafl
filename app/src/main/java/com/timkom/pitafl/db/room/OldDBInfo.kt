package com.timkom.pitafl.db.room

import android.util.ArrayMap
import com.timkom.pitafl.ProductUnit

@Suppress("unused")
object OldDBInfo {

    const val OLD_PRODUCT_CATEGORIES_TABLE_NAME = "product_categories"
    const val OLD_PRODUCTS_TABLE_NAME = "products"
    const val OLD_SHOPPING_LIST_ITEMS_TABLE_NAME = "shopping_list_items"
    val OLD_DB_ON_UPGRADE_SQL_STATEMENTS: ArrayMap<Int, Array<String>> = ArrayMap()
    
    init {
        OLD_DB_ON_UPGRADE_SQL_STATEMENTS[2] = arrayOf(
            "UPDATE $OLD_PRODUCTS_TABLE_NAME SET quantity = 1 WHERE quantity = 0"
        )
        OLD_DB_ON_UPGRADE_SQL_STATEMENTS[3] = arrayOf(
            "ALTER TABLE $OLD_PRODUCTS_TABLE_NAME ADD unit_of_quantity TEXT NOT NULL DEFAULT \"" +
                    ProductUnit.PIECE.name + "\""
        )
    }

}