package com.timkom.pitafl.db.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface ShoppingListDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(shoppingList: ShoppingListEntity)

    @Delete
    fun delete(shoppingList: ShoppingListEntity): Int

    @Query("DELETE FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LISTS
    } WHERE name = :name")
    fun deleteByName(name: String)

    @Update
    fun update(shoppingList: ShoppingListEntity)

    @Query("SELECT EXISTS(SELECT * FROM ${
        LocalDB.TABLE_NAME_SHOPPING_LISTS
    } WHERE name = :name LIMIT 1)")
    fun exists(name: String): Boolean

    @Query("SELECT * FROM ${LocalDB.TABLE_NAME_SHOPPING_LISTS}")
    fun getAll(): List<ShoppingListEntity>

}