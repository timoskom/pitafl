package com.timkom.pitafl.db.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = LocalDB.TABLE_NAME_SHOPPING_LIST_ITEMS)
data class ShoppingListItemEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(typeAffinity = ColumnInfo.INTEGER)
    val id: Int = 0,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val name: String,

    @ColumnInfo(typeAffinity = ColumnInfo.INTEGER)
    val isChecked: Boolean,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val list: String
)