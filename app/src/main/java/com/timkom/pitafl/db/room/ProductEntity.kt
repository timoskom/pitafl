package com.timkom.pitafl.db.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = LocalDB.TABLE_NAME_PRODUCTS,
    foreignKeys = [
        ForeignKey(
            entity = ProductCategoryEntity::class,
            parentColumns = ["name"],
            childColumns = ["category"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index(
            value = [
                "name",
                "barcode"
            ],
            unique = true
        )
    ]
)
data class ProductEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(typeAffinity = ColumnInfo.INTEGER)
    val id: Int = 0,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val name: String,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val category: String,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val barcode: String,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val expirationDate: String,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val picture: String,

    @ColumnInfo(typeAffinity = ColumnInfo.INTEGER)
    val quantity: Int,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val unitOfQuantity: String,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val expirationMark: String
)
