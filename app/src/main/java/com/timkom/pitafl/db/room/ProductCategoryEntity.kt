package com.timkom.pitafl.db.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = LocalDB.TABLE_NAME_PRODUCT_CATEGORIES,
    indices = [
        Index(
            value = ["name"],
            unique = true
        )
    ]
)
data class ProductCategoryEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(typeAffinity = ColumnInfo.INTEGER)
    val id: Int = 0,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val name: String,

    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val picture: String
)
