package com.timkom.pitafl.db.util

fun normalizeValue(value: String): String {
    return value
        .replace("\"", "\"\"")
        .replace("'", "''")
}