package com.timkom.pitafl.data.room.util

import com.timkom.pitafl.ExpirationMark
import com.timkom.pitafl.ProductUnit
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.data.room.ProductCategory
import com.timkom.pitafl.data.room.ShoppingList
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.db.room.ProductCategoryEntity
import com.timkom.pitafl.db.room.ProductEntity
import com.timkom.pitafl.db.room.ShoppingListEntity
import com.timkom.pitafl.db.room.ShoppingListItemEntity
import java.nio.file.Paths
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

fun Product.toEntity(): ProductEntity =
    ProductEntity(
        id,
        name,
        category,
        barcode,
        expirationDate.time.toString(),
        picture.toString(),
        quantity,
        unitOfQuantity.name,
        expirationMark.name
    )

fun ProductEntity.toData(): Product {
    val calendar = Calendar.getInstance()
    calendar.time = try {
        SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US).parse(expirationDate)!!
    } catch (e: ParseException) {
        try {
            Date(expirationDate)
        } catch (e: IllegalArgumentException) {
            calendar.time
        }
    }

    return Product(
        id,
        name,
        category,
        barcode,
        calendar,
        Paths.get(picture),
        quantity,
        ProductUnit.valueOf(unitOfQuantity),
        ExpirationMark.valueOf(expirationMark)
    )
}

fun ProductCategory.toEntity(): ProductCategoryEntity =
    ProductCategoryEntity(id, name, picture.toString())

fun ProductCategoryEntity.toData(): ProductCategory =
    ProductCategory(id, name, Paths.get(picture))

fun ShoppingList.toEntity(): ShoppingListEntity = ShoppingListEntity(id, name)

fun ShoppingListEntity.toData(): ShoppingList = ShoppingList(id, name)

fun ShoppingListItem.toEntity(): ShoppingListItemEntity =
    ShoppingListItemEntity(
        id,
        name,
        isChecked,
        list
    )

fun ShoppingListItemEntity.toData(): ShoppingListItem =
    ShoppingListItem(id, name, isChecked, list)