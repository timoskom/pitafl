package com.timkom.pitafl.data.projections

import com.timkom.pitafl.ProductUnit
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Calendar

/**
 * Projection class that represents an entry in the products table
 */
class ProductProjection @JvmOverloads constructor(
    id: Int?, val name: String, val productCategory: String,
    var barcode: String, val expirationDate: Calendar, picFullPath: String,
    var quantity: Int, val unitOfQuantity: ProductUnit, var expirationMark: ExpirationMark = ExpirationMark.NOT_EXPIRED
) : EntryProjection(id) {

    enum class ExpirationMark {
        NOT_EXPIRED,
        EXPIRES_SOON,
        EXPIRED;

        fun to(): com.timkom.pitafl.ExpirationMark {
            return when (this) {
                NOT_EXPIRED -> com.timkom.pitafl.ExpirationMark.NOT_EXPIRED
                EXPIRES_SOON -> com.timkom.pitafl.ExpirationMark.EXPIRES_SOON
                EXPIRED -> com.timkom.pitafl.ExpirationMark.EXPIRED
            }
        }

        companion object {
            @JvmStatic
            fun from(expirationMark: com.timkom.pitafl.ExpirationMark): ExpirationMark {
                return when (expirationMark) {
                    com.timkom.pitafl.ExpirationMark.NOT_EXPIRED -> NOT_EXPIRED
                    com.timkom.pitafl.ExpirationMark.EXPIRES_SOON -> EXPIRES_SOON
                    com.timkom.pitafl.ExpirationMark.EXPIRED -> EXPIRED
                }
            }
        }
    }

    var pictureFullPath: Path
        private set

    init {
        pictureFullPath = Paths.get(picFullPath)
    }

    constructor(
            name: String, productCategory: String, barcode: String,
            expirationDate: Calendar, picFullPath: String, quantity: Int,
            unitOfQuantity: ProductUnit
    ) : this(
            null, name, productCategory, barcode,
            expirationDate, picFullPath, quantity, unitOfQuantity
    )

    fun increaseQuantity() {
        ++quantity
    }

    fun decreaseQuantity() {
        --quantity
    }

    fun changePictureFullPath(picFullPath: String?) {
        pictureFullPath = Paths.get(picFullPath)
    }

    fun changeExpDate(expDate: String) {
        val day = expDate.substring(0, expDate.indexOf("/"))
        val month = expDate.substring(expDate.indexOf("/") + 1, expDate.lastIndexOf("/"))
        val year = expDate.substring(expDate.lastIndexOf("/") + 1)
        expirationDate[year.toInt(), month.toInt() - 1] = day.toInt()
    }

}
