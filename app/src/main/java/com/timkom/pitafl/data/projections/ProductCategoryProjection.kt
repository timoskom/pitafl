package com.timkom.pitafl.data.projections

import java.nio.file.Path
import java.nio.file.Paths

/**
 * Projection class that represents an entry in the product_categories table
 */
class ProductCategoryProjection(id: Int?, val categoryName: String, picFullPath: String?) : EntryProjection(id) {

    var pictureFullPath: Path
        private set

    init {
        pictureFullPath = Paths.get(picFullPath)
    }

    constructor(category: String, picFullPath: String?) : this(null, category, picFullPath)

    fun changePictureFullPath(picFullPath: String?) {
        pictureFullPath = Paths.get(picFullPath)
    }

}
