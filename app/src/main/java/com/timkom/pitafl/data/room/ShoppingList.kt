package com.timkom.pitafl.data.room

data class ShoppingList(
    val id: Int,
    val name: String
)
