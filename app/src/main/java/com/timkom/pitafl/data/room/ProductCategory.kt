package com.timkom.pitafl.data.room

import java.nio.file.Path

data class ProductCategory(
    val id: Int,
    val name: String,
    var picture: Path
)
