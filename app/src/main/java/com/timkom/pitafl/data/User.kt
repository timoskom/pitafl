package com.timkom.pitafl.data

import android.content.Context
import androidx.core.content.edit
import com.timkom.pitafl.R
import java.nio.file.Path
import java.nio.file.Paths

class User(
        val name: String, val surname: String,
        var username: String,
        picFullPath: String
) {

    val pictureFullPath: Path = Paths.get(picFullPath)

    companion object {
        fun createUser(user: User, ctx: Context): User {
            val sharedPrefs = ctx.getSharedPreferences(ctx.getString(R.string.preferences_file), Context.MODE_PRIVATE)
            sharedPrefs.edit {
                putString(ctx.getString(R.string.user_name), user.name)
                putString(ctx.getString(R.string.user_surname), user.surname)
                putString(ctx.getString(R.string.user_username), user.username)
                putString(ctx.getString(R.string.user_picture_full_path), user.pictureFullPath.toString())
                putBoolean(ctx.getString(R.string.user_was_created), true)
            }
            return user
        }

        fun getUser(ctx: Context): User? {
            val sharedPreferences = ctx.getSharedPreferences(ctx.getString(R.string.preferences_file), Context.MODE_PRIVATE)
            val name = sharedPreferences.getString(ctx.getString(R.string.user_name), ctx.getString(R.string.user_name_default))!!
            val surname = sharedPreferences.getString(ctx.getString(R.string.user_surname), ctx.getString(R.string.user_surname_default))!!
            val username = sharedPreferences.getString(ctx.getString(R.string.user_username), ctx.getString(R.string.user_username_default))!!
            val path = sharedPreferences.getString(ctx.getString(R.string.user_picture_full_path), ctx.getString(R.string.user_picture_full_path_default))!!
            val userWasCreated = sharedPreferences.getBoolean(ctx.getString(R.string.user_was_created), false)
            return if (userWasCreated) User(name, surname, username, path) else null
        }
    }

}
