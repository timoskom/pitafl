package com.timkom.pitafl.data.room

data class ShoppingListItem(
    val id: Int,
    val name: String,
    var isChecked: Boolean,
    val list: String
)
