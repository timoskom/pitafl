package com.timkom.pitafl.data.projections

/**
 * Helper-Projection class that represents an entry in any table (has the ID column)
 */
abstract class EntryProjection(
        /**
         * ID of the projection
         */
        val id: Int?
)
