package com.timkom.pitafl.data.projections

class ShoppingListItemProjection(
        id: Int?, val name: String,
        var isChecked: Boolean, val listName: String
) : EntryProjection(id) {

    constructor(
            name: String,
            checked: Boolean,
            listName: String
    ) : this(null, name, checked, listName)
    
}
