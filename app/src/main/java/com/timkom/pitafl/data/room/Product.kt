package com.timkom.pitafl.data.room

import com.timkom.pitafl.ExpirationMark
import com.timkom.pitafl.ProductUnit
import java.nio.file.Path
import java.util.Calendar

data class Product(
    val id: Int,
    var name: String,
    val category: String,
    var barcode: String,
    val expirationDate: Calendar,
    var picture: Path,
    var quantity: Int,
    val unitOfQuantity: ProductUnit,
    val expirationMark: ExpirationMark
) {

    fun changeExpirationDate(expirationDate: String) {
        val firstIndexOfSlash = expirationDate.indexOf('/')
        val lastIndexOfSlash = expirationDate.lastIndexOf('/')
        val day = expirationDate.substring(0, firstIndexOfSlash)
        val month = expirationDate.substring(firstIndexOfSlash + 1, lastIndexOfSlash)
        val year = expirationDate.substring(lastIndexOfSlash + 1)
        this.expirationDate[year.toInt(), month.toInt() - 1] = day.toInt()
    }

    fun increaseQuantity() {
        ++quantity
    }

    fun decreaseQuantity() {
        --quantity
    }

}
