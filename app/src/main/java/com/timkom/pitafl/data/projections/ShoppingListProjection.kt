package com.timkom.pitafl.data.projections

class ShoppingListProjection(id: Int?, val name: String) : EntryProjection(id) {

    constructor(name: String) : this(null, name)

}
