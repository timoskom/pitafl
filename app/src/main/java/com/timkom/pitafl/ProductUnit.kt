package com.timkom.pitafl

import android.content.Context
import androidx.annotation.StringRes

enum class ProductUnit(@field:StringRes @param:StringRes val resource: Int) {
    PIECE(R.string.Unit__PIECE),
    BOTTLE(R.string.Unit__BOTTLE),
    BOX(R.string.Unit__BOX),
    PACK(R.string.Unit__PACK),
    GRAM(R.string.Unit__GRAM),
    KILO_GRAM(R.string.Unit__KILO_GRAM),
    LITRE(R.string.Unit__LITRE),
    MILLI_LITRE(R.string.Unit__MILLI_LITRE);
    //CAN
    //CUP
    //JAR

    companion object {
        @JvmStatic
        fun getString(unit: ProductUnit, ctx: Context): String = ctx.getString(unit.resource)

        @JvmStatic
        fun getAllStrings(ctx: Context): Array<String> {
            val strings = ArrayList<String>()

            for (unit in entries) {
                strings.add(getString(unit, ctx))
            }

            return strings.toArray(arrayOf())
        }

        @JvmStatic
        fun resolveFromString(value: String, ctx: Context): ProductUnit {
            for (unit in entries) {
                if (value == ctx.getString(unit.resource)) {
                    return unit
                }
            }

            throw IllegalArgumentException(
                "String value \"$value\" can't be resolved to ${ProductUnit::class.java}"
            )
        }
    }

}
