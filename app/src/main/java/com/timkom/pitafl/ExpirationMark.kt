package com.timkom.pitafl

import com.timkom.pitafl.data.projections.ProductProjection

enum class ExpirationMark {
    NOT_EXPIRED,
    EXPIRES_SOON,
    EXPIRED;

    fun to(): ProductProjection.ExpirationMark {
        return when (this) {
            NOT_EXPIRED -> ProductProjection.ExpirationMark.NOT_EXPIRED
            EXPIRES_SOON -> ProductProjection.ExpirationMark.EXPIRES_SOON
            EXPIRED -> ProductProjection.ExpirationMark.EXPIRED
        }
    }

    companion object {
        @JvmStatic
        fun from(expirationMark: ProductProjection.ExpirationMark): ExpirationMark {
            return when (expirationMark) {
                ProductProjection.ExpirationMark.NOT_EXPIRED -> NOT_EXPIRED
                ProductProjection.ExpirationMark.EXPIRES_SOON -> EXPIRES_SOON
                ProductProjection.ExpirationMark.EXPIRED -> EXPIRED
            }
        }
    }
}