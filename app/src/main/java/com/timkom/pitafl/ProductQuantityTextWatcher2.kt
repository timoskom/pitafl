package com.timkom.pitafl

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks
import com.timkom.pitafl.task.coroutine.base.Result
import java.lang.ref.WeakReference

class ProductQuantityTextWatcher2(
    private val product: Product,
    context: Context
) : TextWatcher {

    private val context: WeakReference<Context> = WeakReference(context)

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        s?.let {
            CoroutineTasks.createWithContext(
                context = context.get()!!,
                operation = { ctx ->
                    val db = LocalDB.getInstance(ctx)
                    db.productDao().updateQuantity(it.toString().toInt(), product.name)
                    Result.Success(Unit)
                }
            ).execute(true)
        }
    }

    override fun afterTextChanged(s: Editable?) { }

}