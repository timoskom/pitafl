package com.timkom.pitafl.analyzer

import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.widget.Toast
import androidx.annotation.OptIn
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.android.gms.tasks.Task
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.timkom.pitafl.activity.TextRecognitionActivity.TextData
import com.timkom.pitafl.util.GenericUtils

class TextAnalyzer(
    private val context: Context,
    private val textData: TextData,
    private val acceptExFAB: ExtendedFloatingActionButton
) : ImageAnalysis.Analyzer {


    @OptIn(ExperimentalGetImage::class)
    override fun analyze(image: ImageProxy) {
        image.image?.let { mediaImage ->
            val inputImage = InputImage.fromMediaImage(mediaImage, image.imageInfo.rotationDegrees)
            val recognitionTask = recognizeText(inputImage)
            recognitionTask.addOnCompleteListener { image.close() }
            if (recognitionTask.isSuccessful) {
                if (GenericUtils.isDebug(true)) {
                    val result = recognitionTask.result
                    result?.let {
                        res -> Toast.makeText(context, res.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun recognizeText(image: InputImage): Task<Text?> {
        val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

        return recognizer.process(image)
            .addOnSuccessListener { text ->
                processTextBlock(text)
                acceptExFAB.setEnabled(true)
            }
            .addOnFailureListener { exc -> exc.printStackTrace() }
    }

    @Suppress("unused", "UnusedVariable")
    private fun processTextBlock(result: Text) {
        val textOfElements = ArrayList<String>()
        val cornerPointsOfElements = ArrayList<Array<Point?>?>()
        val boundingBoxOfElements = ArrayList<Rect?>()
        val resultText = result.text
        for (block in result.textBlocks) {
            val blockText = block.text
            val blockCornerPoints = block.cornerPoints
            val blockFrame = block.boundingBox
            for (line in block.lines) {
                val lineText = line.text
                val lineCornerPoints = line.cornerPoints
                val lineFrame = line.boundingBox
                for (element in line.elements) {
                    val elementText = element.text
                    val elementCornerPoints = element.cornerPoints
                    val elementFrame = element.boundingBox
                    textOfElements.add(elementText)
                    cornerPointsOfElements.add(elementCornerPoints)
                    boundingBoxOfElements.add(elementFrame)
                }
            }
        }
        textData.setAll(
            textOfElements.toTypedArray(),
            cornerPointsOfElements.toTypedArray(),
            boundingBoxOfElements.toTypedArray()
        )
    }
}