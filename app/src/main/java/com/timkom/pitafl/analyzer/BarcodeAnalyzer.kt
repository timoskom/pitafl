package com.timkom.pitafl.analyzer

import android.content.Context
import android.widget.Toast
import androidx.annotation.OptIn
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.common.InputImage
import com.timkom.pitafl.activity.BarcodeScannerActivity.BarcodeData
import com.timkom.pitafl.util.GenericUtils

class BarcodeAnalyzer(
    private val context: Context,
    private val barcodeData: BarcodeData,
    private val acceptExFAB: ExtendedFloatingActionButton
) : ImageAnalysis.Analyzer {

    @OptIn(ExperimentalGetImage::class)
    override fun analyze(image: ImageProxy) {
        image.image?.let { mediaImage ->
            val inputImage = InputImage.fromMediaImage(mediaImage, image.imageInfo.rotationDegrees)
            val scannerOptions = BarcodeScannerOptions.Builder()
                .setBarcodeFormats(
                    Barcode.FORMAT_EAN_8,
                    Barcode.FORMAT_EAN_13,
                    Barcode.FORMAT_UPC_A,
                    Barcode.FORMAT_UPC_E
                )
                .build()
            val scanner = BarcodeScanning.getClient(scannerOptions)
            scanner.process(inputImage)
                .addOnCompleteListener { image.close() }
                .addOnSuccessListener { barcodes ->
                    var enableExFAB = false
                    if (barcodes != null && barcodes.isNotEmpty()) {
                        for (barcode in barcodes) {
                            val scannedFormat = barcode.format
                            when (scannedFormat) {
                                Barcode.FORMAT_EAN_8 -> {
                                    if (GenericUtils.isDebug(true))
                                        Toast.makeText(context, "EAN 8", Toast.LENGTH_SHORT).show()
                                    enableExFAB = true
                                }

                                Barcode.FORMAT_EAN_13 -> {
                                    if (GenericUtils.isDebug(true))
                                        Toast.makeText(context, "EAN 13", Toast.LENGTH_SHORT).show()
                                    enableExFAB = true
                                }

                                Barcode.FORMAT_UPC_A -> {
                                    if (GenericUtils.isDebug(true))
                                        Toast.makeText(context, "UPC A", Toast.LENGTH_SHORT).show()
                                    enableExFAB = true
                                }

                                Barcode.FORMAT_UPC_E -> {
                                    if (GenericUtils.isDebug(true))
                                        Toast.makeText(context, "UPC E", Toast.LENGTH_SHORT).show()
                                    enableExFAB = true
                                }

                                else -> {
                                    if (GenericUtils.isDebug(true))
                                        Toast.makeText(context, "NOT SUPPORTED", Toast.LENGTH_SHORT)
                                            .show()
                                }
                            }
                            if (enableExFAB) {
                                acceptExFAB.setEnabled(true)
                            }
                            barcodeData.setAll(
                                barcode.displayValue,
                                barcode.rawValue,
                                barcode.rawBytes,
                                barcode.format
                            )
                        }
                    }
                }
                .addOnFailureListener { exc -> exc.printStackTrace() }
        }
    }

}