package com.timkom.pitafl

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.github.appintro.AppIntro
import com.github.appintro.AppIntroFragment

class PAppIntro : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addSlide(AppIntroFragment.createInstance("Welcome to PITAFL", "Slide-1"))
        addSlide(AppIntroFragment.createInstance(
                "tr",
                "sdl",
                R.drawable.baseline_add_a_photo_black_18,
                R.color.design_default_color_primary_dark
        ))
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        finish()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        finish()
    }

}