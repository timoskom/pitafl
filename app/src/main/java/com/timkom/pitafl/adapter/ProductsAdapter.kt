package com.timkom.pitafl.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.View.OnCreateContextMenuListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.timkom.pitafl.BuildConfig
import com.timkom.pitafl.ExpirationMark
import com.timkom.pitafl.ProductQuantityTextWatcher2
import com.timkom.pitafl.ProductUnit
import com.timkom.pitafl.R
import com.timkom.pitafl.bindAdView
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.initializeAdView
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.SVGFileParser
import java.io.File
import java.io.FileNotFoundException

class ProductsAdapter @JvmOverloads constructor(
    products: ArrayList<Item<Product>> = ArrayList(),
    itemsPerAdd: Int = 7
) : AdSupportingAdapter<Product, AdSupportingAdapter.ViewHolder>(products) {

    class PViewHolder(view: View) : ViewHolder.ItemViewHolder(view), OnCreateContextMenuListener {

        val expirationMarkingIV: ImageView
        val pictureIV: ImageView
        val nameTV: TextView
        val expirationDateTV: TextView
        val barcodeTV: TextView
        val quantityTV: TextView
        val decreaseIV: ImageView
        val increaseIV: ImageView
        val unitOfQuantityTV: TextView
        val detailsLayout: ConstraintLayout
        private val productItem: MaterialCardView

        init {
            view.setOnCreateContextMenuListener(this)
            expirationMarkingIV = view.findViewById(R.id.expirationMarking)
            pictureIV = view.findViewById(R.id.productPicture)
            nameTV = view.findViewById(R.id.productName)
            expirationDateTV = view.findViewById(R.id.productExpirationDate)
            barcodeTV = view.findViewById(R.id.productBarcode)
            quantityTV = view.findViewById(R.id.productQuantity)
            decreaseIV = view.findViewById(R.id.decreaseBtn)
            increaseIV = view.findViewById(R.id.increaseBtn)
            unitOfQuantityTV = view.findViewById(R.id.unitOfProductQuantity)
            detailsLayout = view.findViewById(R.id.productDetailsLayout)
            productItem = view.findViewById(R.id.productItem)
        }

        override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
            menu.add(
                Menu.NONE, R.id.changePicProductCM,
                Menu.NONE, R.string.change_image_product_cm
            )
            menu.add(
                Menu.NONE, R.id.changeExpDateProductCM,
                Menu.NONE, R.string.change_exp_date_product_cm
            )
            menu.add(
                Menu.NONE, R.id.changeBarcodeProductCM,
                Menu.NONE, R.string.change_barcode_product_cm
            )
            menu.add(
                Menu.NONE, R.id.changeNameProductCM,
                Menu.NONE, R.string.change_name_product_cm
            )
            menu.add(
                Menu.NONE, R.id.deleteProductCM,
                Menu.NONE, R.string.delete_product_cm
            )
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == AD_VIEW_TYPE) {
            if (GenericUtils.canUseAds()) {
                return initializeAdView(parent)
            }
        }
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return PViewHolder(view)
    }

    override fun <IVH : ViewHolder.ItemViewHolder> onBindItemViewHolder(
        holder: IVH,
        item: Item.NormalItem<Product>
    ) {
        item.value.let { product ->
            if (holder !is PViewHolder)
                throw IllegalStateException("holder is not a subclass of ${ViewHolder.ItemViewHolder::class.qualifiedName}")
            val expirationMark = product.expirationMark
            if (expirationMark == ExpirationMark.EXPIRED) {
                holder.expirationMarkingIV.setImageResource(R.drawable.expired_marking_ribbon)
            } else if (expirationMark == ExpirationMark.EXPIRES_SOON) {
                holder.expirationMarkingIV.setImageResource(R.drawable.expires_soon_marking_ribbon)
            }
            holder.itemView.setOnLongClickListener {
                super.position = holder.adapterPosition
                holder.adapterPosition
                false
            }
            if (product.quantity != -1) holder.quantityTV.addTextChangedListener(
                ProductQuantityTextWatcher2(product, holder.quantityTV.context)
            )
            if (!holder.decreaseIV.hasOnClickListeners() || !holder.increaseIV.hasOnClickListeners()) {
                holder.decreaseIV.setOnClickListener {
                    if (product.quantity > 1) {
                        product.decreaseQuantity()
                        val quantity = product.quantity
                        holder.quantityTV.text = quantity.toString()
                        if (quantity == 1) {
                            holder.decreaseIV.isEnabled = false
                            holder.decreaseIV.alpha = .4f
                        }
                    }
                }
            }

            if (!holder.increaseIV.hasOnClickListeners()) {
                holder.increaseIV.setOnClickListener {
                    product.increaseQuantity()
                    val quantity = product.quantity
                    holder.quantityTV.text = quantity.toString()
                    if (quantity > 1) {
                        holder.decreaseIV.isEnabled = true
                        holder.decreaseIV.alpha = 1f
                    }
                }
            }
            val image = File(product.picture.toString())

            if (image.toPath().toString().endsWith(".svg")) {
                val svgParse = SVGFileParser(image.toPath())
                try {
                    val pic = svgParse.getAsPictureDrawable()
                    pic.setBounds(
                        holder.pictureIV.left,
                        holder.pictureIV.top,
                        holder.pictureIV.right,
                        holder.pictureIV.bottom
                    )
                    holder.pictureIV.setImageDrawable(pic)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
            } else {
                val width =
                    holder.pictureIV.context.resources.getDimension(R.dimen.categories_image_item_minWidth)
                        .toInt()
                val height =
                    holder.pictureIV.context.resources.getDimension(R.dimen.categories_image_item_minHeight)
                        .toInt()
                var bm =
                    BitmapFactory.decodeFile(image.absolutePath, BitmapFactory.Options())
                if (bm != null) {
                    if (bm.width > width || bm.height > height) bm =
                        Bitmap.createScaledBitmap(
                            bm,
                            holder.pictureIV.context.resources.getDimension(R.dimen.categories_image_item_minWidth)
                                .toInt(),
                            holder.pictureIV.context.resources.getDimension(R.dimen.categories_image_item_minHeight)
                                .toInt(),
                            true
                        )
                    Glide
                        .with(holder.pictureIV.rootView.context)
                        .load(bm)
                        .centerCrop()
                        .into(holder.pictureIV)
                }
            }
            holder.nameTV.text = product.name
            val sdf = SimpleDateFormat("dd/MM/yyyy")

            val dt = sdf.format(product.expirationDate.time)
            holder.expirationDateTV.text = dt
            val barcode = product.barcode

            holder.barcodeTV.text = if ((barcode.isBlank())) "—" else barcode
            val quantity = product.quantity

            holder.quantityTV.text = quantity.toString()
            if (quantity == 1) {
                holder.decreaseIV.isEnabled = false
                holder.decreaseIV.alpha = .4f
            }

            holder.unitOfQuantityTV.text =
                ProductUnit.getString(product.unitOfQuantity, holder.unitOfQuantityTV.context)

            holder.itemView.setOnClickListener {
                //notifyItemChanged(position);
                holder.detailsLayout.visibility =
                    if ((holder.detailsLayout.visibility == View.VISIBLE)) View.GONE else View.VISIBLE
                super.position = holder.adapterPosition
            }
        }
    }

    override fun onBindAdViewHolder(holder: ViewHolder.AdViewHolder, item: Item.AdItem<Product>) {
        bindAdView(holder, item)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.itemView.setOnLongClickListener(null)
        super.onViewRecycled(holder)
    }

}
