package com.timkom.pitafl.adapter

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.View.OnCreateContextMenuListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.timkom.pitafl.activity.ProductsActivity
import com.timkom.pitafl.R
import com.timkom.pitafl.adapter.ImagedCategoriesAdapter.ICViewHolder
import com.timkom.pitafl.adapter.ImagedCategoriesAdapter.ImagedCategory
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.SVGFileParser
import java.io.File
import java.io.FileNotFoundException

class ImagedCategoriesAdapter : BaseListAdapter<ImagedCategory, ICViewHolder> {

    @JvmRecord
    data class ImagedCategory(val image: String, val name: String)

    class ICViewHolder(view: View) : RecyclerView.ViewHolder(view), OnCreateContextMenuListener {

        val imageView: ImageView
        val textView: TextView
        private val cardView: MaterialCardView
        var categoryName: String = ""

        init {
            view.setOnCreateContextMenuListener(this)
            view.setOnClickListener {
                val intent = Intent(view.context, ProductsActivity::class.java)
                intent.putExtra(
                    GenericUtils.IntentBundleKeys.PRODUCTS_ACTIVITY_CATEGORY_KEY,
                    categoryName
                )
                view.context.startActivity(intent)
            }
            imageView = view.findViewById(R.id.imagedCategoryIV)
            cardView = view.findViewById(R.id.imagedCategoryCardV)
            textView = view.findViewById(R.id.imagedCategoryTV)
        }

        fun getCardView(): CardView {
            return this.cardView
        }

        override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
            menu.add(
                Menu.NONE, R.id.changePicCategoryCM,
                Menu.NONE, R.string.change_image_category_cm
            )
            menu.add(
                Menu.NONE, R.id.deleteCategoryCM,
                Menu.NONE, R.string.delete_category_cm
            )
        }

    }

    constructor() : super()

    constructor(categories: ArrayList<ImagedCategory>) : super(categories)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ICViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.category_image, parent, false)
        return ICViewHolder(view)
    }

    override fun onBindViewHolder(holder: ICViewHolder, position: Int) {
        holder.categoryName = localItems[position].name
        holder.itemView.setOnLongClickListener {
            super.position = holder.adapterPosition
            holder.adapterPosition
            false
        }
        holder.textView.text = localItems[position].name
        val image = File(localItems[position].image)
        if (image.toPath().toString().endsWith(".svg")) {
            val svgParse = SVGFileParser(image.toPath())
            try {
                val pic = svgParse.getAsPictureDrawable()
                pic.setBounds(
                    holder.imageView.left,
                    holder.imageView.top,
                    holder.imageView.right,
                    holder.imageView.bottom
                )
                holder.imageView.setImageDrawable(pic)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
        } else {
            val width =
                holder.imageView.context.resources.getDimension(R.dimen.categories_image_item_maxWidth)
                    .toInt()
            val height =
                holder.imageView.context.resources.getDimension(R.dimen.categories_image_item_maxHeight)
                    .toInt()
            var bm = BitmapFactory.decodeFile(image.absolutePath, BitmapFactory.Options())
            if (bm.width > width || bm.height > height) bm = Bitmap.createScaledBitmap(
                bm,
                (width * holder.imageView.context.resources.displayMetrics.density).toInt(),
                (height * holder.imageView.context.resources.displayMetrics.density).toInt(),
                true
            )
            Glide
                .with(holder.imageView.rootView.context)
                .load(bm)
                .into(holder.imageView)
            //holder.getImageView().setImageBitmap(bm);
        }
    }

    override fun onViewRecycled(holder: ICViewHolder) {
        holder.itemView.setOnLongClickListener(null)
        super.onViewRecycled(holder)
    }

}
