package com.timkom.pitafl.adapter

interface ListAdapter<T> {

    fun addAll(items: ArrayList<T>)

    fun removeAll()

    fun add(item: T)

    fun remove(at: Int)

    fun insertAt(at: Int, item: T)

    fun replaceAt(at: Int, item: T)

}