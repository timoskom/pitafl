package com.timkom.pitafl.adapter

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import com.timkom.pitafl.BuildConfig
import com.timkom.pitafl.createAdView
import com.timkom.pitafl.handleAdsForLifecycleStateImpl
import com.timkom.pitafl.util.GenericUtils
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

abstract class AdSupportingAdapter<T, VH : AdSupportingAdapter.ViewHolder> @JvmOverloads constructor(
    items: ArrayList<Item<T>> = ArrayList(),
    val itemsPerAd: Int = 7
) : BaseListAdapter<AdSupportingAdapter.Item<T>, VH>(items) {

    override fun getItemViewType(position: Int): Int {
        if (GenericUtils.canUseAds()) {
            if (position % itemsPerAd == 0) {
                return AD_VIEW_TYPE
            }
        }
        return ITEM_VIEW_TYPE
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val viewType: Int = getItemViewType(position)
        val item = localItems[position]
        if (viewType == ITEM_VIEW_TYPE && item is Item.NormalItem) {
            if (holder::class.isSubclassOf(ViewHolder.ItemViewHolder::class)) {
                onBindItemViewHolder(holder as ViewHolder.ItemViewHolder, item)
            } else {
                throw IllegalStateException("Invalid view ViewHolder for items")
            }
        } else if (viewType == AD_VIEW_TYPE && item is Item.AdItem) {
            if (holder is ViewHolder.AdViewHolder) {
                onBindAdViewHolder(holder, item)
            } else {
                throw IllegalStateException("Invalid ViewHolder for ads")
            }
        } else {
            Log.d(this::class.simpleName, "{ localItems.size: ${localItems.size} }")
            localItems.forEachIndexed { index, localItem ->
                Log.d(this::class.simpleName, "{ localItems[$index]: $localItem }")
            }
            Log.d(this::class.simpleName,
                "{ position: $position, viewType: ${viewType}, item::class.simpleName: ${item::class.simpleName}, holder::class.simpleName: ${holder::class.simpleName} }")
            //throw IllegalStateException("Invalid viewType or item")
        }
    }

    fun removePeek(at: Int): Item<T> {
        val result = localItems[at]
        super.remove(at)
        return result
    }

    fun handleAdsForLifecycleState(event: Lifecycle.Event) {
        for (item in this.localItems) {
            if (item is Item.AdItem) {
                handleAdsForLifecycleStateImpl(event, item.value)
            }
        }
    }

    abstract fun <IVH : ViewHolder.ItemViewHolder> onBindItemViewHolder(holder: IVH, item: Item.NormalItem<T>)

    abstract fun onBindAdViewHolder(holder: ViewHolder.AdViewHolder, item: Item.AdItem<T>)

    sealed class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract class ItemViewHolder(view: View) : ViewHolder(view)
        class AdViewHolder(view: View) : ViewHolder(view)
    }

    sealed class Item<T> {
        data class NormalItem<T>(val value: T) : Item<T>()
        data class AdItem<T>(val value: View) : Item<T>()
    }

    companion object {
        const val ITEM_VIEW_TYPE = 0
        const val AD_VIEW_TYPE = 1

        @JvmStatic
        fun <T, VW: ViewHolder, A : AdSupportingAdapter<T, VW>> ArrayList<T>.mapToItems(
            itemsPerAd: Int,
            context: Context,
            forAdapter: KClass<A>
        ): ArrayList<Item<T>> {
            val mapped = this.map { Item.NormalItem(it) }
            if (GenericUtils.canUseAds()) {
                val result = ArrayList<Item<T>>(mapped)
                val loopedCheck = {
                    var res = false
                    looped@ for (i in result.indices) {
                        Log.d("mapToItems", result[i].toString())
                        if (i % itemsPerAd == 0) {
                            if (result[i] is Item.AdItem) continue@looped
                            Log.d(
                                "mapToItems",
                                "{ i: $i, check: ${i % itemsPerAd == 0}, result[i]: ${result[i]} }"
                            )
                            result.add(i, createAdView(context, forAdapter))
                            res = true
                            break@looped
                        }
                    }
                    res
                }
                @Suppress("ControlFlowWithEmptyBody")
                while (loopedCheck()) {}

                Log.d("mapToItems", result.size.toString())
                return result
            }

            return ArrayList(mapped)
        }
    }

}