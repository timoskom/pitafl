package com.timkom.pitafl.adapter

import androidx.recyclerview.widget.RecyclerView

abstract class BaseListAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>,
    ListAdapter<T> {

    @JvmField
    protected val localItems: ArrayList<T>

    @JvmField
    var position: Int = 0

    constructor() {
        localItems = ArrayList()
    }

    constructor(items: ArrayList<T>) {
        localItems = items
    }

    override fun getItemCount(): Int {
        return localItems.size
    }

    override fun addAll(items: ArrayList<T>) {
        val size = localItems.size
        localItems.addAll(items)
        notifyItemRangeInserted(size, items.size)
    }

    override fun removeAll() {
        val size = localItems.size
        localItems.clear()
        notifyItemRangeRemoved(0, size)
    }

    override fun add(item: T) {
        localItems.add(item)
    }

    override fun remove(at: Int) {
        localItems.removeAt(at)
        notifyItemRemoved(at)
    }

    override fun insertAt(at: Int, item: T) {
        localItems.add(at, item)
        notifyItemInserted(at)
    }

    override fun replaceAt(at: Int, item: T) {
        localItems.removeAt(at)
        notifyItemRemoved(at)
        localItems.add(at, item)
        notifyItemInserted(at)
    }

}