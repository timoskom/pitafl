package com.timkom.pitafl.adapter

import android.annotation.SuppressLint
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.timkom.pitafl.R
import com.timkom.pitafl.adapter.ShoppingListAdapter.SLIViewHolder
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.data.room.util.toEntity
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks
import com.timkom.pitafl.task.coroutine.base.Result

class ShoppingListAdapter : BaseListAdapter<ShoppingListItem, SLIViewHolder> {

    class SLIViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnCreateContextMenuListener {

        val nameTV: TextView
        val checkIV: ImageView
        @Suppress("MemberVisibilityCanBePrivate")
        val shoppingListItem: MaterialCardView

        init {
            view.setOnCreateContextMenuListener(this)
            nameTV = view.findViewById(R.id.shopping_list_item_name_text_view)
            checkIV = view.findViewById(R.id.shopping_list_item_check_image_view)
            shoppingListItem = view.findViewById(R.id.shopping_list_item)
        }

        override fun onCreateContextMenu(
            menu: ContextMenu?,
            v: View?,
            menuInfo: ContextMenu.ContextMenuInfo?
        ) {
            menu?.apply {
                clear()
                add(
                    Menu.NONE, R.id.deleteShoppingListItemCM,
                    Menu.NONE, R.string.delete_shopping_list_item_cm
                )
            }
        }

    }

    constructor() : super()

    constructor(products: ArrayList<ShoppingListItem>) : super(products)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SLIViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.shopping_list_item, parent, false)
        return SLIViewHolder(view)
    }

    @SuppressLint("StaticFieldLeak")
    override fun onBindViewHolder(
        holder: SLIViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.itemView.setOnClickListener {
            val isChecked = localItems[holder.adapterPosition].isChecked
            localItems[holder.adapterPosition].isChecked = !isChecked
            holder.checkIV.setImageResource(
                if (!isChecked)
                    R.drawable.baseline_check_box_36
                else
                    R.drawable.baseline_check_box_outline_blank_36
            )
            CoroutineTasks.createWithContext(
                context = holder.checkIV.context,
                operation = { ctx ->
                    val db = LocalDB.getInstance(ctx)
                    db.shoppingListItemDao().update(localItems[holder.adapterPosition].toEntity())
                    return@createWithContext Result.Success(Unit)
                },
                resultListener = Result.Listener.default()
            ).execute(true)
        }

        holder.itemView.setOnLongClickListener {
            super.position = holder.adapterPosition
            holder.adapterPosition
            false
        }

        holder.checkIV.setImageResource(
            if (localItems[position].isChecked)
                R.drawable.baseline_check_box_36
            else
                R.drawable.baseline_check_box_outline_blank_36
        )

        holder.nameTV.text = localItems[position].name
    }
}
