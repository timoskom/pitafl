package com.timkom.pitafl.task.coroutine.base

import android.content.Context
import androidx.activity.ComponentActivity
import androidx.annotation.MainThread
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import org.jetbrains.annotations.ApiStatus.Experimental
import java.lang.ref.WeakReference

/**
 * The base class of coroutine-based tasks that need a [Context] object.
 * @param context The [Context] to use.
 * @param resultListener The listener ([Result.Listener]) that "listens" for the result of the task.
 * @param R The type of the result.
 * @author Timoleon Komninakis
 */
@Suppress("MemberVisibilityCanBePrivate")
@Experimental
abstract class BaseTaskWithContext<out R>(
    context: Context,
    resultListener: Result.Listener<R>
) : BaseTask<R>(resultListener) {

    /**
     * A [WeakReference] to the [Context].
     */
    protected val contextRef: WeakReference<Context> = WeakReference(context)

    val context: Context
        get() = contextRef.get()!!

    /**
     * Starts the execution on the lifecycle scope of the [ComponentActivity] that the [context] is
     * attached on. It must be called on the main thread.
     * @param isIOOperation If `true` it uses the [Dispatchers.IO], else (by default) it uses
     * the [Dispatchers.Default]
     */
    @MainThread
    @JvmOverloads
    fun execute(isIOOperation: Boolean = false) {
        super.execute((context as ComponentActivity).lifecycleScope, isIOOperation)
    }

}