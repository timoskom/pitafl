package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.data.room.util.toEntity
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class AddProductTask(
    context: Context,
    private val product: Product,
    resultListener: Result.Listener<Unit>
) : BaseTaskWithContext<Unit>(context, resultListener) {

    override fun operate(): Result<Unit> {
        val db = LocalDB.getInstance(context)
        db.productDao().insert(product.toEntity())
        return Result.Success(Unit)
    }

}