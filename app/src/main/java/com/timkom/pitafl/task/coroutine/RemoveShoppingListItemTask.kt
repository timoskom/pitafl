package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class RemoveShoppingListItemTask(
    context: Context,
    private val shoppingListItem: ShoppingListItem,
    resultListener: Result.Listener<Unit>
) : BaseTaskWithContext<Unit>(context, resultListener) {

    override fun operate(): Result<Unit> {
        val db = LocalDB.getInstance(context)
        db.shoppingListItemDao().deleteByNameFromList(
            name = shoppingListItem.name,
            list = shoppingListItem.list
        )
        return Result.Success(Unit)
    }

}