package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.ShoppingList
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.data.room.util.toData
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class ShoppingListTask(
    context: Context,
    resultListener: Result.Listener<Pair<List<ShoppingList>, Map<String, List<ShoppingListItem>>>>
) : BaseTaskWithContext<Pair<List<ShoppingList>, Map<String, List<ShoppingListItem>>>>(context, resultListener) {

    override fun operate(): Result<Pair<List<ShoppingList>, Map<String, List<ShoppingListItem>>>> {
        val db = LocalDB.getInstance(context)
        val shoppingLists = db.shoppingListDao().getAll()
        val shoppingListItemDao = db.shoppingListItemDao()

        val names = shoppingLists.map { it.name }

        return Result.Success(Pair(
            shoppingLists.map { it.toData() },
            names.associateWith { listName ->
                shoppingListItemDao.getAllInList(listName).map { it.toData() }
            }
        ))
    }

    companion object {
        @JvmStatic
        fun handleResult(
            result: Result<Pair<List<ShoppingList>, Map<String, List<ShoppingListItem>>>>
        ): Pair<ArrayList<ShoppingList>, Map<String, ArrayList<ShoppingListItem>>>? {
            val value = when (result) {
                is Result.Success -> {
                    result.data.let {
                        val resultLists = ArrayList(it.first)

                        val resultListsItems = it.second.entries.associate { e ->
                            val items = ArrayList(e.value)
                            e.key to items
                        }

                        Pair(resultLists, resultListsItems)
                    }
                }
                is Result.Error -> {
                    result.exception.printStackTrace()
                    null
                }
            }
            return value
        }
    }

}