package com.timkom.pitafl.task.coroutine.base

import android.content.Context
import com.timkom.pitafl.task.coroutine.base.Result.Error
import com.timkom.pitafl.task.coroutine.base.Result.Success
import kotlinx.coroutines.CoroutineScope

/**
 * @author Timoleon Komninakis
 */
object CoroutineTasks {

    data class WrappedTask<R>(
        val task: BaseTask<R>,
        val executeFunction: (CoroutineScope) -> Unit
    )

    /**
     * Creates a new task ([BaseTask]).
     * @param operation The operation to perform.
     * @param resultListener The listener ([Result.Listener]) that "listens" for the result of the task.
     * @return A newly created task ([BaseTask]).
     * @author Timoleon Komninakis
     */
    @JvmStatic
    @JvmOverloads
    fun <R> create(
        operation: () -> Result<R>,
        resultListener: Result.Listener<R>,
        isIOOperation: Boolean = false
    ): WrappedTask<R> {
        val task = object : BaseTask<R>(resultListener) {
            override fun operate(): Result<R> {
                return operation.invoke()
            }

            fun execute(scope: CoroutineScope) {
                super.execute(scope, isIOOperation)
            }
        }
        return WrappedTask(task, task::execute)
    }

    /**
     * Creates a new task ([BaseTaskWithContext]) with a [Context] object.
     * @param context The [Context] to use.
     * @param operation The operation to perform.
     * @param resultListener The listener ([Result.Listener]) that "listens" for the result of the task.
     * @return A newly created task ([BaseTaskWithContext]).
     * @author Timoleon Komninakis
     */
    @JvmStatic
    @JvmOverloads
    fun <R> createWithContext(
        context: Context,
        operation: (Context) -> Result<R>,
        resultListener: Result.Listener<R> = Result.Listener.default()
    ): BaseTaskWithContext<R> {
        return object : BaseTaskWithContext<R>(context, resultListener) {
            override fun operate(): Result<R> {
                return operation.invoke(this.context)
            }
        }
    }

    /**
     * Creates a [Result.Listener].
     * @param onSuccess Action to perform if the result comes from a successfully completed
     * operation ([Result.Success]).
     * @param onError Action to perform if the result comes from a failed operation ([Result.Error]).
     * @return A newly created [Result.Listener]
     * @author Timoleon Komninakis
     */
    @JvmStatic
    @JvmOverloads
    fun <R> createResultListener(
        onSuccess: (R) -> Unit = {},
        onError: (Exception) -> Unit = { it.printStackTrace() }
    ): Result.Listener<R> {
        return Result.Listener { result ->
            when (result) {
                is Success -> onSuccess.invoke(result.data)
                is Error -> onError.invoke(result.exception)
            }
        }
    }

}