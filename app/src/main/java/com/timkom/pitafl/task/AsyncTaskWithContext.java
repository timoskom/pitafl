package com.timkom.pitafl.task;

import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/** @noinspection unused*/
@Deprecated
public abstract class AsyncTaskWithContext<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private final WeakReference<Context> mContext;

    public AsyncTaskWithContext(Context pmContext) {
        //noinspection deprecation
        super();
        mContext = new WeakReference<>(pmContext);
    }

    protected WeakReference<Context> getContextRef() {
        return mContext;
    }

    public Context getContext() {
        return mContext.get();
    }

}
