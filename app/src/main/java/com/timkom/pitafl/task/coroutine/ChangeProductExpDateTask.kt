package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class ChangeProductExpDateTask(
    context: Context,
    private val newExpirationDate: String,
    private val productName: String,
    resultListener: Result.Listener<Unit>
) : BaseTaskWithContext<Unit>(context, resultListener) {

    override fun operate(): Result<Unit> {
        val db = LocalDB.getInstance(context)
        db.productDao().updateExpirationDate(newExpirationDate, productName)
        return Result.Success(Unit)
    }

}