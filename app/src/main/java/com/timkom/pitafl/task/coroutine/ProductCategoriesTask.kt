package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.ProductCategory
import com.timkom.pitafl.data.room.util.toData
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class ProductCategoriesTask(
    context: Context,
    resultListener: Result.Listener<List<ProductCategory>>
) : BaseTaskWithContext<List<ProductCategory>>(context, resultListener) {

    override fun operate(): Result<List<ProductCategory>> {
        val db = LocalDB.getInstance(context)
        val categories = db.productCategoryDao().getAll()
        return Result.Success(categories.map { it.toData() })
    }

}