package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.ShoppingList
import com.timkom.pitafl.data.room.util.toEntity
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class CreateShoppingListTask(
    context: Context,
    private val shoppingList: ShoppingList,
    resultListener: Result.Listener<Unit>
) : BaseTaskWithContext<Unit>(context, resultListener) {

    override fun operate(): Result<Unit> {
        val db = LocalDB.getInstance(context)
        db.shoppingListDao().insert(shoppingList.toEntity())
        return Result.Success(Unit)
    }

}