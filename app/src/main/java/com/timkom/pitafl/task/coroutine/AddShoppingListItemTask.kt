package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.data.room.util.toEntity
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class AddShoppingListItemTask(
    context: Context,
    private val shoppingListItem: ShoppingListItem,
    resultListener: Result.Listener<Unit>
) : BaseTaskWithContext<Unit>(context, resultListener) {

    override fun operate(): Result<Unit> {
        val db = LocalDB.getInstance(context)
        val dao = db.shoppingListItemDao()
        if (!dao.existsInList(shoppingListItem.name, shoppingListItem.list)) {
            dao.insert(shoppingListItem.toEntity())
        }
        return Result.Success(Unit)
    }

}