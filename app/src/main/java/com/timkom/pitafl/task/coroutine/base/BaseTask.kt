package com.timkom.pitafl.task.coroutine.base

import androidx.annotation.MainThread
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.annotations.ApiStatus.Experimental

/**
 * The base class of coroutine-based tasks.
 * @param resultListener The listener ([Result.Listener]) that "listens" for the result of the task.
 * @param R The type of the result.
 * @author Timoleon Komninakis
 */
@Experimental
abstract class BaseTask<out R>(private val resultListener: Result.Listener<R>) {

    /**
     * The internal job.
     */
    private var job: Deferred<Result<R>>

    init {
        @Suppress("OPT_IN_USAGE")
        job = GlobalScope.async {
            Result.Error(Exception())
        }
    }

    /**
     * The operation to perform.
     * @return The result ([Result]<[R]>) of the operation.
     */
    protected abstract fun operate(): Result<R>

    /**
     * Things to be done before the execution begins.
     */
    protected open fun preExecute() { }

    /**
     * Things to be done after the execution ends.
     */
    protected open fun postExecute() { }

    /**
     * Cancels the internal job.
     */
    protected suspend fun cancel() {
        job.cancelAndJoin()
    }

    /**
     * Posts the result to the [Result.Listener].
     * @param result The [Result] to post.
     */
    protected open fun postResult(result: Result<*>) {
        try {
            @Suppress("UNCHECKED_CAST")
            resultListener.onResultSupplied(result as Result<R>)
        } catch (e: ClassCastException) {
            e.printStackTrace()
            resultListener.onResultSupplied(Result.Error(e))
        }
    }

    private suspend fun internalExecute(isIOOperation: Boolean = false) {
        preExecute()
        job = coroutineScope {
            async {
                withContext(if (!isIOOperation) Dispatchers.Default else Dispatchers.IO) {
                    operate()
                }
            }
        }
        postExecute()
        postResult(job.await())
    }

    /**
     * Starts the execution on the specified [CoroutineScope]. It must be called on the main thread.
     * @param scope The [CoroutineScope] to launch on.
     * @param isIOOperation If `true` it uses the [Dispatchers.IO], else (by default) it uses
     * the [Dispatchers.Default]
     */
    @MainThread
    protected fun execute(scope: CoroutineScope, isIOOperation: Boolean = false) {
        scope.launch { internalExecute(isIOOperation) }
    }

}