package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.ExpirationMark
import com.timkom.pitafl.PredefinedProductCategories
import com.timkom.pitafl.R
import com.timkom.pitafl.ProductUnit
import com.timkom.pitafl.db.room.ProductCategoryEntity
import com.timkom.pitafl.db.room.ProductEntity
import com.timkom.pitafl.db.room.ShoppingListEntity
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.db.room.updateExpirationMarks
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result
import com.timkom.pitafl.util.PseudoBarcodeGenerator
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.Calendar

class LocalDBInitTask @JvmOverloads constructor(
    context: Context,
    resultListener: Result.Listener<Unit> = Result.Listener.default()
) : BaseTaskWithContext<Unit>(context, resultListener) {

    override fun operate(): Result<Unit> {
        return try {
            val images = createImageFiles()
            val categories = createCategories(images)
            val db = LocalDB.getInstance(context)
            db.productCategoryDao().insertMany(*categories)
            val calendars = createCalendars()
            val products = createProducts(categories, calendars)
            val productDao = db.productDao()
            productDao.insertMany(*products)
            productDao.updateExpirationMarks(5)
            db.shoppingListDao().insert(ShoppingListEntity(1, "My Shopping List"))
            Result.Success(Unit)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
    
    private fun createImageFiles(): Array<File> {
        val dataFolder = context.getDir(
            context.getString(R.string.app_data_folder),
            Context.MODE_PRIVATE
        )
        val images = arrayOf(
            File(dataFolder, context.getString(R.string.predefined_icon_pasta_category)),
            File(dataFolder, context.getString(R.string.predefined_icon_meat_fish_category)),
            File(dataFolder, context.getString(R.string.predefined_icon_vegetables_fruits_category)),
            File(dataFolder, context.getString(R.string.predefined_icon_legumes_category)),
            File(dataFolder, context.getString(R.string.predefined_icon_drinks_category)),
            File(dataFolder, context.getString(R.string.predefined_icon_cereals_category)),
            File(dataFolder, context.getString(R.string.predefined_icon_sweet_category)),
            File(dataFolder, context.getString(R.string.predefined_icon_various_category)),
        )
        val assets = context.assets
        try {
            val inputStreams = arrayOf(
                assets.open(context.getString(R.string.assets_icon_pasta_category)),
                assets.open(context.getString(R.string.assets_icon_meat_fish_category)),
                assets.open(context.getString(R.string.assets_icon_vegetables_fruits_category)),
                assets.open(context.getString(R.string.assets_icon_legumes_category)),
                assets.open(context.getString(R.string.assets_icon_drinks_category)),
                assets.open(context.getString(R.string.assets_icon_cereals_category)),
                assets.open(context.getString(R.string.assets_icon_sweet_category)),
                assets.open(context.getString(R.string.assets_icon_various_category)),
            )
            for (i in images.indices) {
                Files.copy(inputStreams[i], images[i].toPath(), StandardCopyOption.REPLACE_EXISTING)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return images
    }

    private fun createCategories(images: Array<File>): Array<ProductCategoryEntity> {
        val initCategories = arrayOf(
            ProductCategoryEntity(
                1,
                PredefinedProductCategories.PASTA.toString(),
                images[0].absolutePath
            ),
            ProductCategoryEntity(
                2,
                PredefinedProductCategories.MEAT_FISH.toString().replace('_', '/'),
                images[1].absolutePath
            ),
            ProductCategoryEntity(
                3,
                PredefinedProductCategories.PRODUCE.toString().replace('_', '/'),
                images[2].absolutePath
            ),
            ProductCategoryEntity(
                4,
                PredefinedProductCategories.LEGUMES.toString(),
                images[3].absolutePath
            ),
            ProductCategoryEntity(
                5,
                PredefinedProductCategories.DRINKS.toString(),
                images[4].absolutePath
            ),
            ProductCategoryEntity(
                6,
                PredefinedProductCategories.CEREALS.toString(),
                images[5].absolutePath
            ),
            ProductCategoryEntity(
                7,
                PredefinedProductCategories.SWEET.toString(),
                images[6].absolutePath
            ),
            ProductCategoryEntity(
                8,
                PredefinedProductCategories.VARIOUS.toString(),
                images[7].absolutePath
            )
        )

        return initCategories
    }
    
    private fun createCalendars(): Array<Calendar> {
        val calendars: Array<Calendar> = arrayOf(
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance(),
                Calendar.getInstance()
        )


        // Make all products expired except for one
        calendars[0].add(Calendar.DATE, 2)
        calendars[1].add(Calendar.DATE, -52)
        calendars[2].add(Calendar.DATE, -34)
        calendars[3].add(Calendar.DATE, -1)
        calendars[4].add(Calendar.DATE, -11)
        calendars[5].add(Calendar.DATE, -7)
        calendars[6].add(Calendar.DATE, -10)
        calendars[7].add(Calendar.DATE, -4)
        calendars[8].add(Calendar.DATE, -9)
        calendars[9].add(Calendar.DATE, -3)
        calendars[10].add(Calendar.DATE, -10)
        calendars[11].add(Calendar.DATE, -30)
        calendars[12].add(Calendar.DATE, -3)
        calendars[13].add(Calendar.DATE, -23)
        calendars[14].add(Calendar.DATE, -14)
        calendars[15].add(Calendar.DATE, -10)
        calendars[16].add(Calendar.DATE, -1)
        calendars[17].add(Calendar.DATE, -39)
        calendars[18].add(Calendar.DATE, -6)
        calendars[19].add(Calendar.DATE, -23)
        calendars[20].add(Calendar.DATE, -13)
        calendars[21].add(Calendar.DATE, -33)
        calendars[22].add(Calendar.DATE, -1)
        calendars[23].add(Calendar.DATE, -5)
        calendars[24].add(Calendar.DATE, -91)
        calendars[25].add(Calendar.DATE, -19)
        
        return calendars
    }
    
    private fun createProducts(
        categories: Array<ProductCategoryEntity>,
        calendars: Array<Calendar>
    ): Array<ProductEntity> {
        var productId = 0
        val initProducts = arrayOf(
            ProductEntity(
                productId,
                "Spaghetti No. 10",
                categories[0].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[0].picture,
                2,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Spaghetti No. 8",
                categories[0].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[0].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Spaghetti No. 11",
                categories[0].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[0].picture,
                12,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Pork Chop",
                categories[1].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[1].picture,
                3,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Chicken Nuggets Package 300gr",
                categories[1].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[1].picture,
                3,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Salmon Fillet 200gr",
                categories[1].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[1].picture,
                2,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Tuna Steak",
                categories[1].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[1].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Tomatoes",
                categories[2].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[2].picture,
                6,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Spinach Package 500gr",
                categories[2].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[2].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Bananas 7-pack",
                categories[2].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[2].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Oranges",
                categories[2].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[2].picture,
                11,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Lentils Package 500gr",
                categories[3].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[3].picture,
                2,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Red Beans 400gr",
                categories[3].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[3].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Chickpeas 600gr",
                categories[3].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[3].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Apple Juice 1L",
                categories[4].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[4].picture,
                3,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Soda 500ml 6-pack",
                categories[4].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[4].picture,
                3,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Fruit Mix Juice 1L",
                categories[4].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[4].picture,
                4,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Mineral Water 1L 6-pack",
                categories[4].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[4].picture,
                5,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Basmati Rice 500gr",
                categories[5].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[5].picture,
                2,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Corn Flakes",
                categories[5].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[5].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Brown Rice 500gr",
                categories[5].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[5].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Barn Flakes",
                categories[5].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[5].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Chocolate Donuts",
                categories[6].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[6].picture,
                5,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "White Chocolate 100gr",
                categories[6].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[6].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Small Food Bags 10-pack",
                categories[7].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId++].time.toString(),
                categories[7].picture,
                5,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            ),
            ProductEntity(
                productId,
                "Mustard 300gr",
                categories[7].name,
                PseudoBarcodeGenerator.generateNew(PseudoBarcodeGenerator.PSEUDO_BARCODE_FORMAT_EAN_13),
                calendars[productId].time.toString(),
                categories[7].picture,
                1,
                ProductUnit.PACK.name,
                ExpirationMark.NOT_EXPIRED.name
            )
        )

        return initProducts
    }
    
}