package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.ProductCategory
import com.timkom.pitafl.data.room.util.toEntity
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class AddProductCategoryTask(
    context: Context,
    private val productCategory: ProductCategory,
    resultListener: Result.Listener<Boolean>
) : BaseTaskWithContext<Boolean>(context, resultListener) {

    override fun operate(): Result<Boolean> {
        val db = LocalDB.getInstance(context)
        val dao = db.productCategoryDao()
        val exists = dao.exists(productCategory.name)
        if (!exists) dao.insert(productCategory.toEntity())
        return Result.Success(!exists)
    }

}