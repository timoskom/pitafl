package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.data.room.util.toData
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class SearchTask(
    context: Context,
    private val query: String?,
    resultListener: Result.Listener<List<Product>>
) : BaseTaskWithContext<List<Product>>(context, resultListener) {

    override fun operate(): Result<List<Product>> {
        return query?.let {
            val db = LocalDB.getInstance(context)
            val result = db.productDao().search("%$query%")
            Result.Success(result.map { it.toData() })
        } ?: Result.Success(emptyList())
    }

}