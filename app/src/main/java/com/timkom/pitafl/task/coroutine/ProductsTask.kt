package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.data.room.util.toData
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.db.room.updateExpirationMarksOfCategory
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class ProductsTask(
    context: Context,
    private val category: String,
    private val notifyIfDays: Int,
    resultListener: Result.Listener<List<Product>>
) : BaseTaskWithContext<List<Product>>(context, resultListener) {

    override fun operate(): Result<List<Product>> {
        val db = LocalDB.getInstance(context)
        val dao = db.productDao()
        dao.updateExpirationMarksOfCategory(category, notifyIfDays)
        val products = dao.getAllForCategory(category)
        return Result.Success(products.map { it.toData() })
    }

}