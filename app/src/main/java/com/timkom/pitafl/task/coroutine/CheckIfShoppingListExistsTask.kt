package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class CheckIfShoppingListExistsTask(
    context: Context,
    private val shoppingList: String,
    resultListener: Result.Listener<Boolean>
) : BaseTaskWithContext<Boolean>(context, resultListener) {

    override fun operate(): Result<Boolean> {
        val db = LocalDB.getInstance(context)
        return Result.Success(db.shoppingListDao().exists(shoppingList))
    }

}