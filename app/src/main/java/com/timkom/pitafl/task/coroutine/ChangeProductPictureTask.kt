package com.timkom.pitafl.task.coroutine

import android.content.Context
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.task.coroutine.base.BaseTaskWithContext
import com.timkom.pitafl.task.coroutine.base.Result

class ChangeProductPictureTask(
    context: Context,
    private val newPicturePath: String,
    private val productName: String,
    resultListener: Result.Listener<Unit>
) : BaseTaskWithContext<Unit>(context, resultListener) {

    override fun operate(): Result<Unit> {
        val db = LocalDB.getInstance(context)
        db.productDao().updatePicture(newPicturePath, productName)
        return Result.Success(Unit)
    }

}