package com.timkom.pitafl.task.coroutine.base

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()

    fun interface Listener<T> {
        fun onResultSupplied(result: Result<T>)

        companion object {
            @JvmStatic
            fun <T> default(): Listener<T> = Listener { }
        }
    }
}