package com.timkom.pitafl.activity

import android.os.Bundle
import android.util.Log
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.timkom.pitafl.R
import com.timkom.pitafl.adapter.ShoppingListAdapter
import com.timkom.pitafl.data.projections.ShoppingListProjection
import com.timkom.pitafl.data.room.ShoppingList
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.dialog.AddShoppingListItemDialogFragment
import com.timkom.pitafl.dialog.AddShoppingListItemDialogFragment.AddShoppingListItemListener
import com.timkom.pitafl.dialog.CreateShoppingListDialogFragment
import com.timkom.pitafl.dialog.CreateShoppingListDialogFragment.CreateShoppingListListener
import com.timkom.pitafl.task.coroutine.AddShoppingListItemTask
import com.timkom.pitafl.task.coroutine.ClearShoppingListTask
import com.timkom.pitafl.task.coroutine.CreateShoppingListTask
import com.timkom.pitafl.task.coroutine.RemoveShoppingListItemTask
import com.timkom.pitafl.task.coroutine.RemoveShoppingListTask
import com.timkom.pitafl.task.coroutine.ShoppingListTask
import com.timkom.pitafl.util.GenericUtils
import java.util.Objects
import java.util.Optional
import java.util.concurrent.atomic.AtomicInteger

class ShoppingListActivity : AppCompatActivity(), AddShoppingListItemListener, CreateShoppingListListener {

    private lateinit var shoppingListsTV: MaterialAutoCompleteTextView
    private lateinit var createShoppingListIB: AppCompatImageButton
    private lateinit var shoppingList: RecyclerView
    private lateinit var addShoppingListItemFAB: FloatingActionButton
    private val shoppingListAdapter = ShoppingListAdapter()
    private var shoppingLists: ArrayList<ShoppingList> = ArrayList()
    private var shoppingListsNames: ArrayList<String> = ArrayList()
    private var selectiveShoppingListItems: MutableMap<String, ArrayList<ShoppingListItem>> = mutableMapOf()

    private fun initialize() {
        shoppingList.setAdapter(shoppingListAdapter)
        shoppingListsTV.setAdapter(ArrayAdapter(
            this,
            R.layout.shopping_lists_dropdown_item,
            shoppingListsNames
        ))

        val selectedList = getLastSelectedList()

        if (selectedList.isPresent) {
            val items: ArrayList<ShoppingListItem>? = selectiveShoppingListItems[selectedList.get().name]
            items?.let { shoppingListAdapter.addAll(it) }
            shoppingListsTV.setText(selectedList.get().name, false)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            setContentView(R.layout.activity_shopping_list)
            val task =
                ShoppingListTask(
                    this
                ) { result ->
                    val res = ShoppingListTask.handleResult(result)
                    if (res != null) {
                        shoppingLists = res.first
                        selectiveShoppingListItems = res.second.toMutableMap()
                        shoppingListsNames = ArrayList(selectiveShoppingListItems.keys)

                        initialize()
                    }
                }
            task.execute(true)

            setSupportActionBar(findViewById(R.id.shopping_list_top_appbar))

            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setTitle(R.string.shopping_list_activity_title)
            }

            shoppingListsTV = findViewById(R.id.shopping_lists_text_view)

            shoppingListsTV.onItemClickListener =
                OnItemClickListener { parent, _, position, _ ->
                    shoppingListAdapter.removeAll()
                    shoppingListAdapter.addAll(
                        selectiveShoppingListItems[parent.getItemAtPosition(
                            position
                        ).toString()]!!
                    )
                }
            shoppingList = findViewById(R.id.shopping_list_recycler_view)
            shoppingList.apply {
                setHasFixedSize(true)
                setLayoutManager(LinearLayoutManager(this@ShoppingListActivity))
                setItemViewCacheSize(20)
            }

            createShoppingListIB = findViewById(R.id.create_shopping_list_image_button)
            createShoppingListIB.setOnClickListener {
                val createListDF = CreateShoppingListDialogFragment.newInstance(
                    shoppingListsTV.getText().toString()
                )
                createListDF.show(supportFragmentManager, "CREATE_SHOPPING_LIST_FRAGMENT")
            }

            addShoppingListItemFAB = findViewById(R.id.add_new_shopping_list_item_fab)
            addShoppingListItemFAB.setOnClickListener {
                val addItemDF = AddShoppingListItemDialogFragment.newInstance(
                    shoppingListsTV.getText().toString()
                )
                addItemDF.show(supportFragmentManager, "ADD_SHOPPING_LIST_ITEM_FRAGMENT")
            }

            registerForContextMenu(shoppingList)
            registerForContextMenu(shoppingListsTV)
        } catch (e: Exception) {
            GenericUtils.showErrorDialog(supportFragmentManager, e)
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        if (v.id == R.id.shopping_lists_text_view) {
            menu.clear()
            menu.add(
                Menu.NONE, R.id.deleteShoppingListCM,
                Menu.NONE, R.string.delete_shopping_list_cm
            )
        }
        shoppingListsTV.showDropDown()
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        var position: Int

        try {
            position = shoppingListAdapter.position
        } catch (e: Exception) {
            e.localizedMessage?.let { Log.d("ContextMenu-EXCEPTION: ", it) }
            return super.onContextItemSelected(item)
        }
        when (item.itemId) {
            (R.id.deleteShoppingListItemCM) -> {
                shoppingListAdapter.remove(position)
                //shoppingListAdapter.notifyItemRemoved(position);
                val listItem =
                    selectiveShoppingListItems[shoppingListsTV.getText().toString()]!![position]
                val removeShoppingListItemTsk = RemoveShoppingListItemTask(
                    this,
                    listItem
                ) { _ -> }
                removeShoppingListItemTsk.execute(true)
                selectiveShoppingListItems[shoppingListsTV.getText().toString()]!!.removeAt(position)
            }

            (R.id.deleteShoppingListCM) -> {
                if (shoppingListsTV.adapter.count > 1) {
                    val shoppingListName = shoppingListsTV.getText().toString()
                    val clearShoppingListTask = ClearShoppingListTask(
                        this,
                        shoppingListName
                    ) { _ -> }
                    val removeListTsk = RemoveShoppingListTask(
                        this,
                        shoppingListsTV.getText().toString()
                    ) { _ -> }
                    clearShoppingListTask.execute(true)
                    removeListTsk.execute(true)
                    val index = shoppingListsNames.indexOf(shoppingListName)
                    selectiveShoppingListItems.remove(shoppingListsNames[index])
                    shoppingLists.removeAt(index)
                    shoppingListsNames.removeAt(index)
                    shoppingListsTV.setAdapter(
                        ArrayAdapter(
                            this,
                            R.layout.shopping_lists_dropdown_item,
                            shoppingListsNames
                        )
                    )
                    shoppingListsTV.setText(
                        shoppingListsNames[if ((index in 1..10)) index - 1 else index],
                        false
                    )
                }
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onFinishAddShoppingListItemDialog(itemName: String, list: String) {
        val item = ShoppingListItem(
            0,
            itemName,
            false,
            list
        )
        val addItemTsk = AddShoppingListItemTask(
            this,
            item
        ) { _ -> }
        addItemTsk.execute(true)
        selectiveShoppingListItems[list]!!.add(item)
        runOnUiThread {
            shoppingListAdapter.add(
                selectiveShoppingListItems[list]!![selectiveShoppingListItems[list]!!.size - 1]
            )
        }
    }

    override fun onFinishCreateShoppingListDialog(listName: String) {
        val shoppingList = ShoppingListProjection(listName)
        val createShoppingListTsk = CreateShoppingListTask(
            this,
            ShoppingList(
                Objects.requireNonNullElse<Int?>(shoppingList.id, 0),
                shoppingList.name
            )
        ) { _ -> }
        createShoppingListTsk.execute(true)

        runOnUiThread {
            if (shoppingListsNames.size >= 10) {
                val removeListTask = RemoveShoppingListTask(
                    this,
                    shoppingListsNames[0]
                ) { _ -> }
                removeListTask.execute(true)
                selectiveShoppingListItems.remove(shoppingListsNames[0])
                shoppingLists.removeAt(0)
                shoppingListsNames.removeAt(0)
            }
            selectiveShoppingListItems[listName] = ArrayList()
            shoppingLists.add(ShoppingList(0, listName))
            shoppingListsNames.add(listName)
            shoppingListsTV.setAdapter(
                ArrayAdapter(
                    this,
                    R.layout.shopping_lists_dropdown_item,
                    shoppingListsNames
                )
            )
            shoppingListsTV.setText(listName, false)
            shoppingListsTV.performClick()
            shoppingListAdapter.removeAll()
            shoppingListAdapter.addAll(selectiveShoppingListItems[listName]!!)
        }
    }

    override fun onPause() {
        super.onPause()

        saveLastSelectedList()
    }

    private fun getLastSelectedList(): Optional<ShoppingList> {
        val lastSelectedId = getSharedPreferences(
            getString(R.string.preferences_file),
            MODE_PRIVATE
        ).getInt(
            getString(R.string.last_selected_shopping_list_id),
            -1
        )

        if (shoppingLists.isEmpty()) return Optional.empty()

        var selectedList: ShoppingList = shoppingLists[0]
        for (sl in shoppingLists) {
            if (sl.id == lastSelectedId) {
                selectedList = sl
                break
            }
        }

        return Optional.of(selectedList)
    }

    private fun saveLastSelectedList() {
        val selectedList = shoppingListsTV.getText().toString()
        val selectedId = AtomicInteger(-1)
        for (sl in shoppingLists) {
            if (sl.name == selectedList) {
                selectedId.set(sl.id)
                break
            }
        }

        getSharedPreferences(
            getString(R.string.preferences_file),
            MODE_PRIVATE
        ).edit()
            .putInt(
                getString(R.string.last_selected_shopping_list_id),
                selectedId.get()
            )
            .apply()
    }

}