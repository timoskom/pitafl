package com.timkom.pitafl.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.core.view.ViewGroupCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Lifecycle
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.work.WorkManager
import com.google.android.material.imageview.ShapeableImageView
import com.timkom.pitafl.AdsConsentManager
import com.timkom.pitafl.BuildConfig
import com.timkom.pitafl.ProductsWatcherWorker
import com.timkom.pitafl.R
import com.timkom.pitafl.adapter.ImagedCategoriesAdapter
import com.timkom.pitafl.adapter.ImagedCategoriesAdapter.ImagedCategory
import com.timkom.pitafl.data.User
import com.timkom.pitafl.data.room.ProductCategory
import com.timkom.pitafl.databinding.ActivityMainBinding
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.dialog.AddCategoryDialogFragment
import com.timkom.pitafl.dialog.AddCategoryDialogFragment.AddCategoryListener
import com.timkom.pitafl.dialog.ChangePictureDialogFragment
import com.timkom.pitafl.dialog.ChangePictureDialogFragment.ChangePictureListener
import com.timkom.pitafl.dialog.CreateUserDialogFragment
import com.timkom.pitafl.dialog.CreateUserDialogFragment.CreateUserListener
import com.timkom.pitafl.handleAdsForLifecycleState
import com.timkom.pitafl.initializeAds
import com.timkom.pitafl.task.coroutine.AddProductCategoryTask
import com.timkom.pitafl.task.coroutine.ChangeCategoryPictureTask
import com.timkom.pitafl.task.coroutine.LocalDBInitTask
import com.timkom.pitafl.task.coroutine.ProductCategoriesTask
import com.timkom.pitafl.task.coroutine.RemoveProductCategoryTask
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks
import com.timkom.pitafl.task.coroutine.base.Result
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.SVGFileParser
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.lang.ref.WeakReference
import java.nio.file.Paths
import java.text.MessageFormat
import java.time.Duration
import java.util.Date
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicReference

class MainActivity : AppCompatActivity(), AddCategoryListener, ChangePictureListener, CreateUserListener {

    private lateinit var binding: ActivityMainBinding
    private var firstTimeRunning: Boolean? = null
    private val categories: ArrayList<ProductCategory> = ArrayList()
    private var imagedCategoriesAdapter: ImagedCategoriesAdapter = ImagedCategoriesAdapter()
    private var thisUser: User? = null
    private val defaultExecutor: ExecutorService = Executors.newFixedThreadPool(4)

    @SuppressLint("SwitchIntDef")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            binding = ActivityMainBinding.inflate(layoutInflater)
            setContentView(binding.root)

            val adsConsentManager = AdsConsentManager.getInstance(applicationContext)
            adsConsentManager.gatherConsent(this) { error ->
                error?.let { Log.w("@MainActivity", "${it.errorCode}: ${it.message}") }
                if (adsConsentManager.canRequestAds) {
                    // there is a bug in Google AdMob on Android 15, so don't display ads
                    if (GenericUtils.canUseAds(this@MainActivity)) {
                        this.initializeAds()
                    }
                }
            }

            if (adsConsentManager.canRequestAds) {
                if (GenericUtils.canUseAds(this@MainActivity)) {
                    this.initializeAds()
                    this.handleAdsForLifecycleState(Lifecycle.Event.ON_CREATE)
                }
            }

            setSupportActionBar(binding.mainTopAppbar)

            val productCategoriesTask = ProductCategoriesTask(
                this,
                CoroutineTasks.createResultListener({ productCategories ->
                    categories.clear()
                    categories.addAll(productCategories)
                    imagedCategoriesAdapter.removeAll()
                    imagedCategoriesAdapter.addAll(ArrayList(categories.map {
                        ImagedCategory(
                            it.picture.toAbsolutePath().toString(),
                            it.name
                        )
                    }))
                })
            )

            // First Time Code
            if (isFirstTime) {
                LocalDBInitTask(
                    this,
                    CoroutineTasks.createResultListener({ productCategoriesTask.execute(true) })
                ).execute(true)
                if (GenericUtils.isDebug(false)) {
                    Toast.makeText(applicationContext, "First Time Toast!", Toast.LENGTH_SHORT).show()
                }
            } else {
                productCategoriesTask.execute(true)
                checkForUnusedPrefs()
            }
            PreferenceManager.setDefaultValues(this, R.xml.preferences, false)

            binding.navigationView.setNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    findViewById<View>(R.id.shopping_list_item_nav_drawer).id -> {
                        val shoppingListIntent = Intent(
                            applicationContext,
                            ShoppingListActivity::class.java
                        )
                        startActivity(shoppingListIntent)
                    }
                    findViewById<View>(R.id.settings_item_nav_drawer).id -> {
                        val settingsIntent = Intent(applicationContext, SettingsActivity::class.java)
                        startActivity(settingsIntent)
                    }
                    findViewById<View>(R.id.about_item_nav_drawer).id -> {
                        val aboutIntent = Intent(applicationContext, AboutActivity::class.java)
                        startActivity(aboutIntent)
                    }
                }
                false
            }

            val bottomAppBar = binding.bottomAppBar
            bottomAppBar.setNavigationOnClickListener {
                // Open Drawer
                binding.drawerLayout.openDrawer(binding.navigationView)
            }

            val bufScreenSize =
                resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
            val screenSize = when (bufScreenSize) {
                Configuration.SCREENLAYOUT_SIZE_SMALL -> 'S'
                Configuration.SCREENLAYOUT_SIZE_NORMAL -> 'N'
                Configuration.SCREENLAYOUT_SIZE_LARGE -> 'L'
                Configuration.SCREENLAYOUT_SIZE_XLARGE -> 'X'
                else -> 'U'
            }

            val bufScreenOrientation = resources.configuration.orientation
            val screenOrientation = when (bufScreenOrientation) {
                Configuration.ORIENTATION_PORTRAIT -> 'P'
                Configuration.ORIENTATION_LANDSCAPE -> 'L'
                else -> 'U'
            }

            val imagedCategories = ArrayList<ImagedCategory>()
            for ((_, name, picture) in categories) {
                imagedCategories.add(
                    ImagedCategory(
                        picture.toAbsolutePath().toString(),
                        name
                    )
                )
            }

            val imagedCategoriesList = binding.imagedCategoriesList

            ViewGroupCompat.installCompatInsetsDispatch(binding.drawerLayout)

            ViewCompat.setOnApplyWindowInsetsListener(imagedCategoriesList) { v, insets ->
                val ins = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                // Apply the insets as a margin to the view. This solution sets only the
                // bottom, left, and right dimensions, but you can apply whichever insets are
                // appropriate to your layout. You can also update the view padding if that's
                // more appropriate.
                val mlp = v.layoutParams as ViewGroup.MarginLayoutParams
                mlp.leftMargin = ins.left
                mlp.bottomMargin = ins.bottom
                mlp.rightMargin = ins.right
                mlp.topMargin = ins.top
                v.layoutParams = mlp

                // Return CONSUMED if you don't want want the window insets to keep passing
                // down to descendant views.
                WindowInsetsCompat.CONSUMED
            }

            imagedCategoriesList.setItemViewCacheSize(14)
            imagedCategoriesAdapter = ImagedCategoriesAdapter(imagedCategories)
            if (screenSize == 'S') imagedCategoriesList.layoutManager =
                GridLayoutManager(this, 1)
            else if (screenSize == 'N' && screenOrientation == 'P') imagedCategoriesList.layoutManager =
                GridLayoutManager(this, 2)
            else if ((screenSize == 'N' && screenOrientation == 'L') || (screenSize == 'L' && screenOrientation == 'P')) imagedCategoriesList.layoutManager =
                GridLayoutManager(this, 3)
            else if ((screenSize == 'L' && screenOrientation == 'L') || (screenSize == 'X' && screenOrientation == 'P')) imagedCategoriesList.layoutManager =
                GridLayoutManager(this, 4)
            else if (screenSize == 'X' && screenOrientation == 'L') imagedCategoriesList.layoutManager =
                GridLayoutManager(this, 5)
            else imagedCategoriesList.layoutManager = GridLayoutManager(this, 2)
            imagedCategoriesList.adapter = imagedCategoriesAdapter

            registerForContextMenu(imagedCategoriesList)

            bottomAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    (R.id.search_menu_item) -> {
                        onSearchRequested()
                        true
                    }

                    (R.id.settings_menu_item) -> {
                        val settingsIntent =
                            Intent(applicationContext, SettingsActivity::class.java)
                        startActivity(settingsIntent)
                        true
                    }

                    else -> false
                }
            }

            binding.addCategoryFab.setOnClickListener {
                val addCategoryDF = AddCategoryDialogFragment.newInstance()
                addCategoryDF.show(supportFragmentManager, "newCategory")
            }

            if (isFirstTime) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    if (!GenericUtils.checkPostNotificationsPermission(this)) {
                        GenericUtils.requestPostNotificationsPermission(
                            WeakReference(this),
                            POST_NOTIFICATIONS_PERMISSION_REQUEST_CODE
                        )
                    } else {
                        createProductsWatcher()
                    }
                } else {
                    createProductsWatcher()
                }
                val createUserDF = CreateUserDialogFragment.newInstance()
                createUserDF.show(supportFragmentManager, "createUser")
            } else {
                thisUser = User.getUser(applicationContext)
                thisUser?.let { user ->
                    val userUserTV: TextView = binding.navigationView.getHeaderView(0)
                        .findViewById(R.id.user_user)
                    userUserTV.text =
                        MessageFormat.format("{0} {1}", user.name, user.surname)

                    val userIconSIV = binding.navigationView.getHeaderView(0)
                        .findViewById<ShapeableImageView>(R.id.user_icon)
                    if (user.pictureFullPath.toString().endsWith(".svg")) {
                        userIconSIV.setImageDrawable(
                            SVGFileParser(user.pictureFullPath).getAsPictureDrawable()
                        )
                    } else {
                        userIconSIV.setImageBitmap(
                            BitmapFactory.decodeFile(
                                user.pictureFullPath.toString()
                            )
                        )
                    }
                } ?: CreateUserDialogFragment.newInstance().show(supportFragmentManager, "createUser")
            }
        } catch (e: Exception) {
            GenericUtils.showErrorDialog(supportFragmentManager, e)
        }
    }

    private val isFirstTime: Boolean
        /**
         * Checks whether is the first time that the app opens or not.
         * @return [firstTimeRunning] with value true if
         * is the first `time` else `false`
         */
        get() {
            if (firstTimeRunning == null) {
                val sharedPrefs =
                    getSharedPreferences(
                        this.getString(R.string.preferences_file),
                        MODE_PRIVATE
                    )
                val firstTimeDefault = resources.getBoolean(R.bool.first_time_running)
                firstTimeRunning = sharedPrefs.getBoolean(
                    getString(R.string.is_first_time_running),
                    firstTimeDefault
                )

                if (firstTimeRunning!!) {
                    val spEditor = sharedPrefs.edit()
                    spEditor.putBoolean(getString(R.string.is_first_time_running), false)
                    spEditor.apply()
                }
            }
            return firstTimeRunning!!
        }

    private fun checkForUnusedPrefs() {
        CoroutineTasks.createWithContext(
            this,
            { ctx: Context ->
                val prefs = ctx.getSharedPreferences(
                    ctx.getString(R.string.preferences_file),
                    MODE_PRIVATE
                )
                val shouldCheckForUnusedPrefsPref = prefs.getBoolean(
                    ctx.getString(R.string.should_check_for_unused_prefs_pref_key),
                    true
                )
                if (shouldCheckForUnusedPrefsPref) {
                    val unusedPrefs = ctx.resources.getStringArray(R.array.unused_prefs)
                    val prefsEditor = prefs.edit()
                    for (unused in unusedPrefs) {
                        if (prefs.contains(unused)) prefsEditor.remove(unused)
                    }
                    prefsEditor.apply()
                }
                Result.Success(Unit)
            }
        ).execute()
    }

    override fun onFinishAddCategoryDialog(categoryImage: Bitmap, categoryName: String) {
        defaultExecutor.execute {
            val dataFolder = getDir(getString(R.string.app_data_folder), MODE_PRIVATE)
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val imageFl = File(dataFolder, "private-$timeStamp-$categoryName.png")
            try {
                val fl = FileOutputStream(imageFl)
                categoryImage.compress(Bitmap.CompressFormat.WEBP, 40, fl)
                val addProductCategoryTask = AddProductCategoryTask(
                    this,
                    ProductCategory(
                        0,
                        categoryName,
                        Paths.get(imageFl.absolutePath)
                    )
                ) { result ->
                    if (result is Result.Success<Boolean>) {
                        if (result.data) {
                            categories.add(
                                ProductCategory(
                                    0,
                                    categoryName,
                                    Paths.get(imageFl.absolutePath)
                                )
                            )
                            runOnUiThread {
                                imagedCategoriesAdapter.add(
                                    ImagedCategory(
                                        categories[categories.size - 1].picture.toString(),
                                        categories[categories.size - 1].name
                                    )
                                )
                                imagedCategoriesAdapter.notifyItemInserted(imagedCategoriesAdapter.itemCount)
                            }
                        } else {
                            runOnUiThread {
                                val builder =
                                    AlertDialog.Builder(this)
                                        .setCancelable(true)
                                        .setPositiveButton(
                                            android.R.string.ok
                                        ) { dialog, _ -> dialog.dismiss() }
                                        .setMessage("Category \"$categoryName\" already exists!")
                                builder.create().show()
                            }
                        }
                    } else if (result is Result.Error) {
                        result.exception.printStackTrace()
                    }
                }
                runOnUiThread { addProductCategoryTask.execute(true) }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val position: Int
        try {
            position = imagedCategoriesAdapter.position
        } catch (e: Exception) {
            e.localizedMessage?.let { Log.d("ContextMenu-EXCEPTION: ", it) }
            return super.onContextItemSelected(item)
        }
        when (item.itemId) {
            (R.id.changePicCategoryCM) -> {
                // change image of category
                ChangePictureDialogFragment.newInstance().apply {
                    val args = Bundle()
                    args.putString(
                        GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_CALL_SRC,
                        categories[position].name
                    )
                    args.putString(
                        GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_TABLE_SRC,
                        LocalDB.TABLE_NAME_PRODUCT_CATEGORIES
                    )
                    arguments = args
                }.show(supportFragmentManager, "changePictureOfCategory")
            }

            (R.id.deleteCategoryCM) -> {
                if (imagedCategoriesAdapter.itemCount != 1) {
                    imagedCategoriesAdapter.remove(position)
                    val cat = categories[position]
                    RemoveProductCategoryTask(
                        this,
                        cat.name,
                        CoroutineTasks.createResultListener({
                            CoroutineTasks.createWithContext(
                                this@MainActivity,
                                {
                                    val imageDel = File(cat.picture.toString())
                                    if (!imageDel.delete()) {
                                        if (!imageDel.renameTo(
                                                File(imageDel.absolutePath + taskId)
                                        )) {
                                            applicationContext.deleteFile(imageDel.name)
                                        }
                                    }
                                    categories.removeAt(position)
                                    Result.Success(Unit)
                                }
                            )
                            Unit
                        })
                    ).execute(true)
                } else {
                    val bufBuilder = AlertDialog.Builder(this@MainActivity)
                        .setCancelable(false)
                        .setPositiveButton(
                            "OK"
                        ) { dialog, _ -> dialog.dismiss() }
                        .setMessage("Can't remove a category when there is only one")
                    bufBuilder.create().show()
                }
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onFinishChangePictureDialog(newImage: Bitmap, fromWhat: String) {
        defaultExecutor.execute {
            val dataFolder = getDir(getString(R.string.app_data_folder), MODE_PRIVATE)
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val imageFl = File(dataFolder, "private-$timeStamp-$fromWhat.png")
            try {
                val fl = FileOutputStream(imageFl)
                newImage.compress(Bitmap.CompressFormat.WEBP, 40, fl)
                val changeCategoryPictureTask = ChangeCategoryPictureTask(
                    this,
                    imageFl.absolutePath,
                    fromWhat
                ) { result ->
                    if (result is Result.Success<Unit>) {
                        runOnUiThread {
                            var i = 0
                            for ((_, name) in categories) {
                                if (name == fromWhat) {
                                    categories[i].picture = Paths.get(imageFl.absolutePath)
                                    imagedCategoriesAdapter.notifyItemChanged(i) //not working
                                    break
                                }
                                i++
                            }
                        }
                    } else if (result is Result.Error) {
                        result.exception.printStackTrace()
                    }
                }
                runOnUiThread { changeCategoryPictureTask.execute(true) }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onFinishCreateUserDialog(
        userName: String,
        userSurname: String,
        userUsername: String,
        userImage: Bitmap?
    ) {
        if (userImage == null) {
            thisUser = User(
                userName,
                userSurname,
                userUsername,
                File(
                    getDir(getString(R.string.app_data_folder), MODE_PRIVATE),
                    getString(R.string.predefined_icon_user_avatar)
                ).toPath().toString()
            )
        } else {
            val dataFolder = getDir(getString(R.string.app_data_folder), MODE_PRIVATE)
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val imageFl = File(
                dataFolder,
                "private-$timeStamp-user-picture.png"
            )
            try {
                val fl = FileOutputStream(imageFl)
                thisUser = User(
                    userName,
                    userSurname,
                    userUsername,
                    imageFl.toPath().toString()
                )
                val atomicActivity = AtomicReference(this)
                defaultExecutor.execute {
                    userImage.compress(Bitmap.CompressFormat.PNG, 80, fl)
                    atomicActivity.get().runOnUiThread {
                        try {
                            val userIconSIV: ShapeableImageView = binding.navigationView
                                .getHeaderView(0)
                                .findViewById(R.id.user_icon)
                            if (thisUser!!.pictureFullPath.toString().endsWith(".svg")) {
                                userIconSIV.setImageDrawable(
                                    SVGFileParser(thisUser!!.pictureFullPath).getAsPictureDrawable()
                                )
                            } else {
                                userIconSIV.setImageBitmap(
                                    BitmapFactory.decodeFile(
                                        thisUser!!.pictureFullPath.toString()
                                    )
                                )
                            }
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        }
                    }
                }
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
        }
        User.createUser(thisUser!!, applicationContext)

        val userUserTV: TextView =
            binding.navigationView.getHeaderView(0).findViewById(R.id.user_user)

        userUserTV.text = MessageFormat.format("{0} {1}", thisUser!!.name, thisUser!!.surname)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == POST_NOTIFICATIONS_PERMISSION_REQUEST_CODE) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createProductsWatcher()
            } else {
                val workManager = WorkManager.getInstance(this)
                workManager.cancelAllWorkByTag(ProductsWatcherWorker.PRODUCTS_WATCHER_WORKER_TAG)
                Toast.makeText(this, "No notifications will be shown", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun createProductsWatcher() {
        ProductsWatcherWorker.create(this, Duration.ofDays(1))
    }

    override fun onStart() {
        super.onStart()
        if (BuildConfig.IS_MONET) this.handleAdsForLifecycleState(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.IS_MONET) this.handleAdsForLifecycleState(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        if (BuildConfig.IS_MONET) this.handleAdsForLifecycleState(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        if (BuildConfig.IS_MONET) this.handleAdsForLifecycleState(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroy() {
        if (BuildConfig.IS_MONET) this.handleAdsForLifecycleState(Lifecycle.Event.ON_DESTROY)
        super.onDestroy()
    }

    companion object {
        private const val POST_NOTIFICATIONS_PERMISSION_REQUEST_CODE = 12001
    }

}