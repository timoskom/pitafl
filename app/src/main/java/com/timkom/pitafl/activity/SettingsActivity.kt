package com.timkom.pitafl.activity

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat
import androidx.work.Data
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.timkom.pitafl.ProductsWatcherWorker
import com.timkom.pitafl.R
import java.time.Duration

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment())
                .commit()
        }

        setSupportActionBar(findViewById(R.id.settings_top_appbar))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment : PreferenceFragmentCompat(), OnSharedPreferenceChangeListener {

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            preferenceManager.sharedPreferencesName = getString(R.string.preferences_file)

            setPreferencesFromResource(R.xml.preferences, rootKey)
        }

        override fun onResume() {
            super.onResume()
            preferenceScreen.sharedPreferences?.registerOnSharedPreferenceChangeListener(this)
        }

        override fun onPause() {
            super.onPause()
            preferenceScreen.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(this)
        }

        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String?) {
            when (key) {
                getString(R.string.expiration_notifications_key) -> {
                    val workManager = WorkManager.getInstance(requireContext().applicationContext)
                    workManager.cancelAllWorkByTag(ProductsWatcherWorker.PRODUCTS_WATCHER_WORKER_TAG)
                    val request: PeriodicWorkRequest = PeriodicWorkRequest.Builder(
                            ProductsWatcherWorker::class.java,
                            Duration.ofDays(1L)
                    )
                        .addTag(ProductsWatcherWorker.PRODUCTS_WATCHER_WORKER_TAG)
                        .setInputData(Data.Builder().putInt(
                            "NOTIFY_IF_DAYS",
                            sharedPreferences.getString(
                                key,
                                getString(R.string.expiration_notifications_default)
                            )!!.toInt()
                        ).build()).build()
                    workManager.enqueue(request)
                }
                getString(R.string.user_name),
                getString(R.string.user_surname),
                getString(R.string.user_username) -> {
                    val value = sharedPreferences.getString(key, "")
                    value?.let {
                        sharedPreferences.edit().putString(
                            key,
                            if (it.isBlank()) "demo" else it.trim()
                        ).apply()
                    }
                }
                else -> return
            }
        }

    }

}