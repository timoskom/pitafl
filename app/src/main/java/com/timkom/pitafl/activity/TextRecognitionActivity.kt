package com.timkom.pitafl.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.util.Rational
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.core.UseCaseGroup
import androidx.camera.core.ViewPort
import androidx.camera.lifecycle.ProcessCameraProvider
import com.google.android.gms.common.util.concurrent.HandlerExecutor
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.timkom.pitafl.R
import com.timkom.pitafl.analyzer.TextAnalyzer
import com.timkom.pitafl.databinding.ActivityTextRecognitionBinding
import com.timkom.pitafl.dialog.ErrorDialogFragment
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.tryMultiCatch
import java.util.concurrent.ExecutionException
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class TextRecognitionActivity : AppCompatActivity() {

    private val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
    private lateinit var textRecognitionBinding: ActivityTextRecognitionBinding
    private lateinit var acceptTextExFAB: ExtendedFloatingActionButton
    private lateinit var flashToggleIB: AppCompatImageButton
    private val textData = TextData()
    private var camera: Camera? = null
    private var isFlashOn = false

    @Suppress("unused")
    class TextData {
        private var mTextOfElements: Array<String>
        private var mCornerPointsOfElements: Array<Array<Point?>?>
        private var mBoundingBoxOfElements: Array<Rect?>

        init {
            mTextOfElements = emptyArray()
            mCornerPointsOfElements = arrayOfNulls(0)
            mBoundingBoxOfElements = arrayOfNulls(0)
        }

        fun getTextOfElements(): Array<String> {
            return mTextOfElements
        }

        fun getCornerPointsOfElements(): Array<Array<Point?>?> {
            return mCornerPointsOfElements
        }

        fun getBoundingBoxOfElements(): Array<Rect?> {
            return mBoundingBoxOfElements
        }

        fun setTextOfElements(pmTextOfElements: Array<String>) {
            mTextOfElements = pmTextOfElements
        }

        fun setCornerPointsOfElements(pmCornerPointsOfElements: Array<Array<Point?>?>) {
            mCornerPointsOfElements = pmCornerPointsOfElements
        }

        fun setBoundingBoxOfElements(pmBoundingBoxOfElements: Array<Rect?>) {
            mBoundingBoxOfElements = pmBoundingBoxOfElements
        }

        fun setAll(
            pmTextOfElements: Array<String>,
            pmCornerPointsOfElements: Array<Array<Point?>?>,
            pmBoundingBoxOfElements: Array<Rect?>
        ) {
            setTextOfElements(pmTextOfElements)
            setCornerPointsOfElements(pmCornerPointsOfElements)
            setBoundingBoxOfElements(pmBoundingBoxOfElements)
        }
    }

    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            textRecognitionBinding = ActivityTextRecognitionBinding.inflate(layoutInflater)
            setContentView(textRecognitionBinding.getRoot())

            setSupportActionBar(textRecognitionBinding.textRecognitionTopAppbar)

            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setTitle(R.string.text_recognition_activity_title)
            }

            acceptTextExFAB = textRecognitionBinding.acceptTextFromRecognition
            acceptTextExFAB.setOnClickListener {
                val result = Intent()
                var resultText = ""
                for (txt in textData.getTextOfElements()) {
                    resultText = "$resultText$txt "
                }
                result.putExtra(
                    GenericUtils.IntentBundleKeys.TEXT_RECOGNITION_ACTIVITY_TEXT_KEY,
                    resultText
                )
                setResult(TEXT_RECOGNITION_ACTIVITY_RESULT_CODE, result)
                finish()
            }

            flashToggleIB = textRecognitionBinding.flashToggleTextRecognition
            flashToggleIB.setOnClickListener {
                camera?.cameraControl?.enableTorch(!isFlashOn)
                flashToggleIB.setImageResource(if ((isFlashOn)) R.drawable.baseline_flash_off_black_36 else R.drawable.baseline_flash_on_black_36)
                isFlashOn = !isFlashOn
            }

            val providerListenableFuture = ProcessCameraProvider.getInstance(this)
            providerListenableFuture.addListener(
                {
                    tryMultiCatch(
                        runThis = {
                            val cameraProvider = providerListenableFuture.get()

                            val preview = Preview.Builder().build()
                            preview.surfaceProvider = textRecognitionBinding.cameraPreviewView.getSurfaceProvider()

                            val imageAnalysis = ImageAnalysis.Builder()
                                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                                .build()
                            imageAnalysis.setAnalyzer(
                                cameraExecutor,
                                TextAnalyzer(this, textData, acceptTextExFAB)
                            )

                            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

                            @SuppressLint("UnsafeExperimentalUsageError") val viewPort =
                                ViewPort.Builder(
                                    Rational(4, 3),
                                    preview.targetRotation
                                ).build()

                            @SuppressLint("UnsafeExperimentalUsageError") val useCaseGroup =
                                UseCaseGroup.Builder()
                                    .addUseCase(preview)
                                    .addUseCase(imageAnalysis)
                                    .setViewPort(viewPort)
                                    .build()

                            cameraProvider.unbindAll()
                            camera = cameraProvider.bindToLifecycle(this, cameraSelector, useCaseGroup)
                            //cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalysis);
                        },
                        onCatch = { it.printStackTrace() },
                        ExecutionException::class,
                        InterruptedException::class,
                        IllegalStateException::class,
                        IllegalArgumentException::class
                    )
                },
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
                    mainExecutor
                else
                    HandlerExecutor(mainLooper)
            )
        } catch (e: Exception) {
            ErrorDialogFragment.newInstance().apply {
                val errorArgs = Bundle()
                val exceptionClass: Class<out Exception?> = e.javaClass
                errorArgs.putString(
                    GenericUtils.IntentBundleKeys.ERROR_DIALOG_FRAGMENT_CLASS_TYPE_NAME_KEY,
                    exceptionClass.getTypeName()
                )
                errorArgs.putString(
                    GenericUtils.IntentBundleKeys.ERROR_DIALOG_FRAGMENT_MESSAGE_KEY,
                    e.message
                )
                setArguments(errorArgs)
            }.show(supportFragmentManager, "ERROR_DIALOG_FRAGMENT")
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        cameraExecutor.shutdown()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val TEXT_RECOGNITION_ACTIVITY_RESULT_CODE: Int = 20010
    }

}