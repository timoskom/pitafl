package com.timkom.pitafl.activity

import android.graphics.Bitmap
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.ViewGroup.MarginLayoutParams
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.core.view.ViewGroupCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.FragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.timkom.pitafl.AdsConsentManager.Companion.getInstance
import com.timkom.pitafl.BuildConfig
import com.timkom.pitafl.ExpirationMark
import com.timkom.pitafl.ProductUnit
import com.timkom.pitafl.R
import com.timkom.pitafl.adapter.AdSupportingAdapter.Companion.mapToItems
import com.timkom.pitafl.adapter.AdSupportingAdapter.Item.NormalItem
import com.timkom.pitafl.adapter.ProductsAdapter
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.data.room.ShoppingListItem
import com.timkom.pitafl.databinding.ActivityProductsBinding
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.dialog.AddProductDialogFragment
import com.timkom.pitafl.dialog.AddProductDialogFragment.AddProductListener
import com.timkom.pitafl.dialog.ChangeNameDialogFragment
import com.timkom.pitafl.dialog.ChangeNameDialogFragment.ChangeNameListener
import com.timkom.pitafl.dialog.ChangePictureDialogFragment
import com.timkom.pitafl.dialog.ChangePictureDialogFragment.ChangePictureListener
import com.timkom.pitafl.dialog.ChangeProductBarcodeDialogFragment
import com.timkom.pitafl.dialog.ChangeProductBarcodeDialogFragment.ChangeProductBarcodeListener
import com.timkom.pitafl.dialog.DatePickerDialogFragment
import com.timkom.pitafl.task.coroutine.AddProductTask
import com.timkom.pitafl.task.coroutine.AddShoppingListItemTask
import com.timkom.pitafl.task.coroutine.ChangeProductBarcodeTask
import com.timkom.pitafl.task.coroutine.ChangeProductExpDateTask
import com.timkom.pitafl.task.coroutine.ChangeProductNameTask
import com.timkom.pitafl.task.coroutine.ChangeProductPictureTask
import com.timkom.pitafl.task.coroutine.ProductsTask
import com.timkom.pitafl.task.coroutine.RemoveAllProductsTask
import com.timkom.pitafl.task.coroutine.RemoveProductTask
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.GenericUtils.showErrorDialog
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Paths
import java.util.Calendar
import java.util.Date

class ProductsActivity : AppCompatActivity(), AddProductListener, ChangePictureListener,
    ChangeProductBarcodeListener, ChangeNameListener {

    private enum class OrderBy {
        NAME,
        EXP_DATE
    }

    private var category: String? = null
    private val products = ArrayList<Product>()
    private val productsAdapter = ProductsAdapter()
    private var orderBy = OrderBy.NAME
    private lateinit var comparator: Comparator<Product>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            val binding = ActivityProductsBinding.inflate(layoutInflater)
            setContentView(binding.getRoot())
            val options = intent.extras
            category =
                options!!.getString(GenericUtils.IntentBundleKeys.PRODUCTS_ACTIVITY_CATEGORY_KEY)

            setSupportActionBar(binding.productsTopAppbar)

            supportActionBar?.apply {
                title = category
            }

            val adsConsentManager = getInstance(applicationContext)

            if (adsConsentManager.canRequestAds) {
                if (GenericUtils.canUseAds(this@ProductsActivity)) {
                    productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_CREATE)
                }
            }

            val prefs = getSharedPreferences(getString(R.string.preferences_file), MODE_PRIVATE)
            val notifyIfDays: String = prefs.getString(
                getString(R.string.expiration_notifications_key),
                getString(R.string.expiration_notifications_default)
            )!!
            val prefOrderBy: String = prefs.getString(
                getString(R.string.products_order_by_key),
                getResources().getStringArray(R.array.products_order_by_entries_values)[0]
            )!!

            orderBy = if (prefOrderBy == OrderBy.NAME.toString()) {
                OrderBy.NAME
            } else {
                OrderBy.EXP_DATE
            }

            comparator = if (orderBy == OrderBy.NAME)
                Comparator.comparing(Product::name)
            else
                Comparator.comparing { p -> p.expirationDate.time }

            ProductsTask(
                this,
                category!!,
                notifyIfDays.toInt(),
                CoroutineTasks.createResultListener({ productList ->
                    products.clear()
                    products.addAll(productList)
                    products.sortWith(comparator)
                    productsAdapter.removeAll()
                    productsAdapter.notifyDataSetChanged()
                    productsAdapter.addAll(
                        products.mapToItems(
                            productsAdapter.itemsPerAd,
                            this,
                            productsAdapter.javaClass.kotlin
                        )
                    )
                    Log.d("@ProductsActivity#ProductsTask", products.toString())
                })
            ).execute(true)

            val productsList = binding.productsList

            ViewGroupCompat.installCompatInsetsDispatch(binding.productsConstraintLayout)

            ViewCompat.setOnApplyWindowInsetsListener(
                productsList
            ) { v, insets ->
                val ins = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                // Apply the insets as a margin to the view. This solution sets only the
                // bottom, left, and right dimensions, but you can apply whichever insets are
                // appropriate to your layout. You can also update the view padding if that's
                // more appropriate.
                val mlp = v.layoutParams as MarginLayoutParams
                mlp.leftMargin = ins.left
                mlp.bottomMargin = ins.bottom
                mlp.rightMargin = ins.right
                mlp.topMargin = ins.top
                v.setLayoutParams(mlp)

                // Return CONSUMED if you don't want want the window insets to keep passing
                // down to descendant views.
                WindowInsetsCompat.CONSUMED
            }

            productsList.setHasFixedSize(true)
            productsList.setLayoutManager(LinearLayoutManager(this))
            productsList.setItemViewCacheSize(20)
            Log.d("@ProductsActivity#ProductsTask", products.toString())
            productsList.setAdapter(productsAdapter)
            registerForContextMenu(productsList)

            val bottomAppBarPA = binding.bottomAppBarPA
            bottomAppBarPA.setOnMenuItemClickListener(Toolbar.OnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    (R.id.delete_products) -> {
                        run {
                            val bufBuilder = AlertDialog.Builder(this@ProductsActivity)
                                .setPositiveButton(
                                    android.R.string.ok
                                ) { dialog, _ ->
                                    val warnBuilder = AlertDialog.Builder(this@ProductsActivity)
                                        .setPositiveButton(
                                            android.R.string.ok
                                        ) { _, _ ->
                                            RemoveAllProductsTask(
                                                this,
                                                category!!,
                                                CoroutineTasks.createResultListener({ _ -> finish() })
                                            ).execute(true)
                                        }
                                        .setNegativeButton(
                                            android.R.string.cancel
                                        ) { warnDialog, _ -> warnDialog.dismiss() }
                                        .setMessage(
                                            "Are you sure you want to delete all the products for the " +
                                                    category + " category? This action is not reversible!"
                                        )
                                        .setIcon( //(!nightMode) ?
                                            R.drawable.baseline_warning_black_24 // :
                                            //R.drawable.baseline_warning_white_24
                                        )
                                        .setTitle("WARNING")
                                    warnBuilder.create().show()
                                    dialog!!.dismiss()
                                }
                                .setNegativeButton(
                                    android.R.string.cancel
                                ) { dialog, _ -> dialog.dismiss() }
                                .setMessage("Remove all the products from this category?")
                            bufBuilder.create().show()
                        }
                        return@OnMenuItemClickListener true
                    }

                    (R.id.search_product) -> {
                        onSearchRequested()
                        return@OnMenuItemClickListener true
                    }
                }
                false
            })
            binding.addProductFab.setOnClickListener {
                val dfData = Bundle()
                dfData.putString(
                    GenericUtils.IntentBundleKeys.ADD_PRODUCT_DIALOG_FRAGMENT_CATEGORY_KEY,
                    category
                )
                val addProductDF = AddProductDialogFragment.newInstance()
                addProductDF.setArguments(dfData)
                addProductDF.show(supportFragmentManager, "newProduct")
            }
            supportFragmentManager.setFragmentResultListener(
                GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_REQUEST_KEY,
                this,
                FragmentResultListener { _, result ->
                    val trueResult =
                        result.getBundle(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_RESULT_KEY)
                    val year =
                        trueResult!!.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_YEAR_KEY)
                    val month =
                        trueResult.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_MONTH_KEY)
                    val day =
                        trueResult.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_DAY_KEY)
                    val expDate = (day.toString() + "/" + (month + 1) + "/" + year)
                    val fromWhat =
                        result.getString(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_CALL_SRC)
                    if (fromWhat == null) {
                        showErrorDialog(
                            supportFragmentManager,
                            NullPointerException("Source item cannot be identified!")
                        )
                        return@FragmentResultListener
                    }
                    val cExpDate = Calendar.Builder()
                        .setDate(year, month, day)
                        .build()
                    ChangeProductExpDateTask(
                        this,
                        cExpDate.time.toString(),
                        fromWhat,
                        CoroutineTasks.createResultListener({
                            runOnUiThread {
                                var i = 0
                                for (prod in products) {
                                    if (prod.name == fromWhat) {
                                        products[i].changeExpirationDate(expDate)
                                        productsAdapter.notifyItemChanged(i)
                                        break
                                    }
                                    i++
                                }
                                if (orderBy == OrderBy.EXP_DATE) {
                                    products.sortWith(comparator)
                                    productsAdapter.removeAll()
                                    productsAdapter.addAll(
                                        products.mapToItems(
                                            productsAdapter.itemsPerAd,
                                            this,
                                            productsAdapter.javaClass.kotlin
                                        )
                                    )
                                }
                            }
                        })
                    ).execute(true)
                })
        } catch (e: Exception) {
            showErrorDialog(supportFragmentManager, e)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        var position: Int
        try {
            position = productsAdapter.position
        } catch (e: Exception) {
            e.localizedMessage?.let { Log.d("ContextMenu-EXCEPTION: ", it) }
            return super.onContextItemSelected(item)
        }
        when (item.itemId) {
            (R.id.changePicProductCM) -> {
                // change product of product
                ChangePictureDialogFragment.newInstance().apply {
                    val args = Bundle()
                    args.putString(
                        GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_CALL_SRC,
                        products[position].name
                    )
                    args.putString(
                        GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_TABLE_SRC,
                        LocalDB.TABLE_NAME_PRODUCTS
                    )
                    setArguments(args)
                }.show(supportFragmentManager, "changePictureOfProduct")
            }

            (R.id.changeExpDateProductCM) -> {
                // change expiration date of product
                DatePickerDialogFragment().apply {
                    val args = Bundle()
                    args.putString(
                        GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_CALL_SRC,
                        products[position].name
                    )
                    setArguments(args)
                }.show(supportFragmentManager, "datePicker")
            }

            (R.id.changeBarcodeProductCM) -> {
                // change barcode of product
                ChangeProductBarcodeDialogFragment.newInstance().apply {
                    val args = Bundle()
                    args.putString(
                        GenericUtils.IntentBundleKeys.CHANGE_PRODUCT_BARCODE_DIALOG_FRAGMENT_CALL_SRC,
                        products[position].name
                    )
                    setArguments(args)
                }.show(supportFragmentManager, "changeBarcodeOfProduct")
            }

            (R.id.changeNameProductCM) -> {
                ChangeNameDialogFragment.newInstance().apply {
                    val args = Bundle()
                    val product = products[position]
                    args.putInt(
                        GenericUtils.IntentBundleKeys.CHANGE_NAME_DIALOG_FRAGMENT_CALL_SRC,
                        product.id
                    )
                    args.putString(
                        GenericUtils.IntentBundleKeys.CHANGE_NAME_DIALOG_FRAGMENT_CURRENT_NAME_KEY,
                        product.name
                    )
                    setArguments(args)
                }.show(supportFragmentManager, "changeNameOfProduct")
            }

            (R.id.deleteProductCM) -> {
                //productsAdapter.remove(position);
                //Product prod = products.get(position);
                val prod: Product = (productsAdapter.removePeek(position) as NormalItem<Product>).value
                val shoppingListItem = ShoppingListItem(
                    0,
                    prod.name,
                    false,
                    "DELETED"
                )
                RemoveProductTask(
                    this,
                    prod,
                    CoroutineTasks.createResultListener({
                        val addShoppingListItemTask = AddShoppingListItemTask(
                            this,
                            shoppingListItem,
                            CoroutineTasks.createResultListener()
                        )
                        addShoppingListItemTask.execute(true)
                        products.remove(prod)
                        Unit
                    })
                ).execute(true)
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onFinishAddProductDialog(
        productImage: Bitmap, productName: String, productCategory: String,
        productExpDate: String, productQuantity: Int, productUnitOfQuantity: ProductUnit,
        productBarcode: String
    ) {
        val dataFolder = getDir(getString(R.string.app_data_folder), MODE_PRIVATE)
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val productNameCopy = productName.replace(
            "[\\s\\.\\,\\*;\\?/<>\\|`~!@#$%\\^\\&\\=\\+\\[\\]\\{\\}\\(\\)\\\\]".toRegex(),
            "_"
        )
        val imageFl = File(dataFolder, "private-$timeStamp-$productNameCopy.png")
        try {
            val fl = FileOutputStream(imageFl)
            productImage.compress(Bitmap.CompressFormat.WEBP, 50, fl)
            val cBuilder = Calendar.Builder()
            cBuilder.set(
                Calendar.YEAR,
                productExpDate.substring(productExpDate.lastIndexOf('/') + 1).toInt()
            )
            cBuilder.set(
                Calendar.MONTH,
                productExpDate.substring(
                    productExpDate.indexOf('/') + 1,
                    productExpDate.lastIndexOf('/')
                ).toInt()
            )
            cBuilder.set(
                Calendar.DAY_OF_MONTH,
                productExpDate.substring(0, productExpDate.indexOf('/')).toInt()
            )
            val prod = Product(
                0,
                productName,
                productCategory,
                productBarcode,
                cBuilder.build(),
                Paths.get(imageFl.absolutePath),
                productQuantity,
                productUnitOfQuantity,
                ExpirationMark.NOT_EXPIRED
            )
            AddProductTask(
                this,
                prod,
                CoroutineTasks.createResultListener({
                    products.add(prod)
                    products.sortWith(comparator)
                    runOnUiThread {
                        productsAdapter.removeAll()
                        productsAdapter.addAll(
                            products.mapToItems(
                                productsAdapter.itemsPerAd,
                                this,
                                productsAdapter.javaClass.kotlin
                            )
                        )
                    }
                })
            ).execute()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onFinishChangePictureDialog(newImage: Bitmap, fromWhat: String) {
        val dataFolder = getDir(getString(R.string.app_data_folder), MODE_PRIVATE)
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFl = File(dataFolder, "private-$timeStamp-$fromWhat.png")
        try {
            val fl = FileOutputStream(imageFl)
            newImage.compress(Bitmap.CompressFormat.WEBP, 50, fl)
            ChangeProductPictureTask(
                this,
                imageFl.absolutePath,
                fromWhat,
                CoroutineTasks.createResultListener({
                    runOnUiThread {
                        var i = 0
                        for (prod in products) {
                            if (prod.name == fromWhat) {
                                products[i].picture = Paths.get(imageFl.absolutePath)
                                productsAdapter.notifyItemChanged(i)
                                break
                            }
                            i++
                        }
                    }
                })
            ).execute(true)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onFinishChangeProductBarcodeDialog(newBarcode: String, fromWhat: String) {
        ChangeProductBarcodeTask(
            this,
            newBarcode,
            fromWhat,
            CoroutineTasks.createResultListener({
                runOnUiThread {
                    var i = 0
                    for (prod in products) {
                        if (prod.name == fromWhat) {
                            products[i].barcode = newBarcode
                            productsAdapter.notifyItemChanged(i)
                            break
                        }
                        i++
                    }
                }
            })
        ).execute(true)
    }

    override fun onFinishChangeNameListener(newName: String, fromWhat: Int) {
        ChangeProductNameTask(
            this,
            newName,
            fromWhat,
            CoroutineTasks.createResultListener({
                runOnUiThread {
                    for (i in products.indices) {
                        if (products[i].id == fromWhat) {
                            products[i].name = newName
                            productsAdapter.notifyItemChanged(i)
                            break
                        }
                    }
                }
            })
        ).execute(true)
    }

    override fun onStart() {
        super.onStart()
        if (BuildConfig.IS_MONET)
            productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.IS_MONET)
            productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        if (BuildConfig.IS_MONET)
            productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        if (BuildConfig.IS_MONET)
            productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroy() {
        if (BuildConfig.IS_MONET)
            productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_DESTROY)
        super.onDestroy()
    }

}