package com.timkom.pitafl.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Rational
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.core.UseCaseGroup
import androidx.camera.core.ViewPort
import androidx.camera.lifecycle.ProcessCameraProvider
import com.google.android.gms.common.util.concurrent.HandlerExecutor
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.timkom.pitafl.R
import com.timkom.pitafl.analyzer.BarcodeAnalyzer
import com.timkom.pitafl.databinding.ActivityBarcodeScannerBinding
import com.timkom.pitafl.dialog.ErrorDialogFragment
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.tryMultiCatch
import java.util.concurrent.ExecutionException
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class BarcodeScannerActivity : AppCompatActivity() {

    private val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
    private lateinit var scannerBinding: ActivityBarcodeScannerBinding
    private val barcodeData = BarcodeData()
    private lateinit var acceptBarcodeExFAB: ExtendedFloatingActionButton
    private lateinit var flashToggleIB: AppCompatImageButton
    private var camera: Camera? = null
    private var isFlashOn = false

    class BarcodeData {
        private var displayValue: String
        private var rawValue: String
        private val rawBytes: ArrayList<Byte>
        private var format: Int

        init {
            displayValue = ""
            rawValue = ""
            rawBytes = ArrayList()
            format = 0
        }

        fun getDisplayValue(): String {
            return displayValue
        }

        fun getRawValue(): String {
            return rawValue
        }

        fun getRawBytes(): ArrayList<Byte> {
            return rawBytes
        }

        fun getFormat(): Int {
            return format
        }

        fun setDisplayValue(pmDisplayValue: String?) {
            displayValue = pmDisplayValue ?: ""
        }

        fun setRawValue(pmRawValue: String?) {
            rawValue = pmRawValue ?: ""
        }

        fun setRawBytes(pmRawBytes: ByteArray?) {
            rawBytes.clear()
            if (pmRawBytes != null) {
                for (rawByte in pmRawBytes) {
                    rawBytes.add(rawByte)
                }
            }
        }

        fun setFormat(pmFormat: Int) {
            format = pmFormat
        }

        fun setAll(
            pmDisplayValue: String?,
            pmRawValue: String?,
            pmRawBytes: ByteArray?,
            pmFormat: Int
        ) {
            setDisplayValue(pmDisplayValue)
            setRawValue(pmRawValue)
            setRawBytes(pmRawBytes)
            setFormat(pmFormat)
        }
    }

    @SuppressLint("RestrictedApi", "UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            scannerBinding = ActivityBarcodeScannerBinding.inflate(layoutInflater)
            setContentView(scannerBinding.getRoot())

            setSupportActionBar(scannerBinding.barcodeScannerTopAppbar)

            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setTitle(R.string.barcode_scanner_activity_title)
            }

            acceptBarcodeExFAB = scannerBinding.acceptBarcodeFromScanner
            acceptBarcodeExFAB.setOnClickListener {
                val result = Intent()
                result.putExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_DISPLAY_VALUE_KEY,
                    barcodeData.getDisplayValue()
                )
                result.putExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_VALUE_KEY,
                    barcodeData.getRawValue()
                )
                var i = 0
                val rawBytes = ByteArray(barcodeData.getRawBytes().size)
                for (rawByte in barcodeData.getRawBytes()) {
                    rawBytes[i++] = rawByte
                }
                result.putExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_RAW_BYTES_KEY,
                    rawBytes
                )
                result.putExtra(
                    GenericUtils.IntentBundleKeys.BARCODE_SCANNER_ACTIVITY_BARCODE_FORMAT_KEY,
                    barcodeData.getFormat()
                )
                setResult(BARCODE_SCANNER_ACTIVITY_RESULT_CODE, result)
                finish()
            }

            flashToggleIB = scannerBinding.flashToggleBarcodeScanner
            flashToggleIB.setOnClickListener {
                camera!!.cameraControl.enableTorch(!isFlashOn)
                flashToggleIB.setImageResource(if (isFlashOn) R.drawable.baseline_flash_off_black_36 else R.drawable.baseline_flash_on_black_36)
                isFlashOn = !isFlashOn
            }

            val providerListenableFuture = ProcessCameraProvider.getInstance(this)
            providerListenableFuture.addListener(
                {
                    tryMultiCatch(
                        runThis = {
                            val cameraProvider = providerListenableFuture.get()

                            val preview = Preview.Builder().build()
                            preview.surfaceProvider = scannerBinding.cameraPreviewView.getSurfaceProvider()

                            val imageAnalysis = ImageAnalysis.Builder()
                                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                                .build()
                            imageAnalysis.setAnalyzer(
                                cameraExecutor,
                                BarcodeAnalyzer(this, barcodeData, acceptBarcodeExFAB)
                            )

                            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

                            @SuppressLint("UnsafeExperimentalUsageError") val viewPort =
                                ViewPort.Builder(
                                    Rational(4, 3),
                                    preview.targetRotation
                                ).build()

                            @SuppressLint("UnsafeExperimentalUsageError") val useCaseGroup =
                                UseCaseGroup.Builder()
                                    .addUseCase(preview)
                                    .addUseCase(imageAnalysis)
                                    .setViewPort(viewPort)
                                    .build()

                            cameraProvider.unbindAll()
                            camera = cameraProvider.bindToLifecycle(this, cameraSelector, useCaseGroup)
                            //camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalysis);
                        },
                        onCatch = { it.printStackTrace() },
                        ExecutionException::class,
                        InterruptedException::class,
                        IllegalStateException::class,
                        IllegalArgumentException::class
                    )
                },
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
                    mainExecutor
                else
                    HandlerExecutor(mainLooper)
            )
        } catch (e: Exception) {
            ErrorDialogFragment.newInstance().apply {
                val errorArgs = Bundle()
                val exceptionClass: Class<out Exception?> = e.javaClass
                errorArgs.putString(
                    GenericUtils.IntentBundleKeys.ERROR_DIALOG_FRAGMENT_CLASS_TYPE_NAME_KEY,
                    exceptionClass.getTypeName()
                )
                errorArgs.putString(
                    GenericUtils.IntentBundleKeys.ERROR_DIALOG_FRAGMENT_MESSAGE_KEY,
                    e.message
                )
                setArguments(errorArgs)
            }.show(supportFragmentManager, "ERROR_DIALOG_FRAGMENT")
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        cameraExecutor.shutdown()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val BARCODE_SCANNER_ACTIVITY_RESULT_CODE: Int = 10010
    }

}