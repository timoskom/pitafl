package com.timkom.pitafl.activity

import android.app.SearchManager
import android.content.Intent
import android.graphics.Bitmap
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.ViewGroup.MarginLayoutParams
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.ViewCompat
import androidx.core.view.ViewGroupCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.timkom.pitafl.AdsConsentManager
import com.timkom.pitafl.BuildConfig
import com.timkom.pitafl.R
import com.timkom.pitafl.adapter.AdSupportingAdapter
import com.timkom.pitafl.adapter.AdSupportingAdapter.Companion.mapToItems
import com.timkom.pitafl.adapter.ProductsAdapter
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.db.room.LocalDB
import com.timkom.pitafl.dialog.ChangeNameDialogFragment
import com.timkom.pitafl.dialog.ChangeNameDialogFragment.ChangeNameListener
import com.timkom.pitafl.dialog.ChangePictureDialogFragment
import com.timkom.pitafl.dialog.ChangePictureDialogFragment.ChangePictureListener
import com.timkom.pitafl.dialog.ChangeProductBarcodeDialogFragment
import com.timkom.pitafl.dialog.ChangeProductBarcodeDialogFragment.ChangeProductBarcodeListener
import com.timkom.pitafl.dialog.DatePickerDialogFragment
import com.timkom.pitafl.task.coroutine.ChangeProductBarcodeTask
import com.timkom.pitafl.task.coroutine.ChangeProductExpDateTask
import com.timkom.pitafl.task.coroutine.ChangeProductNameTask
import com.timkom.pitafl.task.coroutine.ChangeProductPictureTask
import com.timkom.pitafl.task.coroutine.RemoveProductTask
import com.timkom.pitafl.task.coroutine.SearchTask
import com.timkom.pitafl.task.coroutine.base.CoroutineTasks.createResultListener
import com.timkom.pitafl.util.GenericUtils
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Paths
import java.util.Calendar
import java.util.Date

class SearchableActivity : AppCompatActivity(), ChangePictureListener, ChangeProductBarcodeListener,
    ChangeNameListener {

    private val products = ArrayList<Product>()
    private val productsAdapter = ProductsAdapter()
    private var query: String? = ""
    private lateinit var adsConsentManager: AdsConsentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            setContentView(R.layout.activity_searchable)

            val intent = intent

            adsConsentManager = AdsConsentManager.getInstance(applicationContext)

            if (Intent.ACTION_SEARCH == intent.action) {
                query = intent.getStringExtra(SearchManager.QUERY)
                SearchTask(
                    this,
                    query,
                    createResultListener({ searchResults: List<Product> ->
                        products.clear()
                        products.addAll(searchResults)
                        productsAdapter.removeAll()
                        productsAdapter.addAll(products.mapToItems(
                            productsAdapter.itemsPerAd,
                            this,
                            productsAdapter::class
                        ))
                    })
                ).execute(true)
            }

            setSupportActionBar(findViewById(R.id.searchable_top_appbar))

            val actionBar = supportActionBar
            actionBar?.setDisplayHomeAsUpEnabled(true)

            if (adsConsentManager.canRequestAds) {
                if (GenericUtils.canUseAds(this@SearchableActivity)) {
                    productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_CREATE)
                }
            }

            supportFragmentManager.setFragmentResultListener(
                GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_REQUEST_KEY,
                this
            ) { _: String, result: Bundle ->
                val trueResult =
                    result.getBundle(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_RESULT_KEY)
                val year =
                    trueResult!!.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_YEAR_KEY)
                val month =
                    trueResult.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_MONTH_KEY)
                val day =
                    trueResult.getInt(GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_TRUE_RESULT_DAY_KEY)
                val expDate = (day.toString() + "/" + (month + 1) + "/" + year)
                val fromWhat =
                    result.getString(GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_CALL_SRC)
                if (fromWhat == null) {
                    GenericUtils.showErrorDialog(
                        supportFragmentManager,
                        NullPointerException("Source item cannot be identified!")
                    )
                    return@setFragmentResultListener
                }
                val cExpDate = Calendar.Builder()
                    .setDate(year, month, day)
                    .build()
                ChangeProductExpDateTask(
                    this,
                    cExpDate.time.toString(),
                    fromWhat,
                    createResultListener({
                        runOnUiThread {
                            var i = 0
                            for ((_, name) in products) {
                                if (name == fromWhat) {
                                    products[i].changeExpirationDate(expDate)
                                    //productsAdapter.notifyItemChanged(i);
                                    break
                                }
                                i++
                            }
                        }
                    })
                ).execute(true)
            }

            productsAdapter.addAll(products.mapToItems(
                productsAdapter.itemsPerAd,
                this,
                productsAdapter::class
            ))
            val productsList: RecyclerView = findViewById(R.id.searchableItemsList)

            ViewGroupCompat.installCompatInsetsDispatch(findViewById(R.id.searchable_constraint_layout))

            ViewCompat.setOnApplyWindowInsetsListener(
                productsList
            ) { v, insets ->
                val ins = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                // Apply the insets as a margin to the view. This solution sets only the
                // bottom, left, and right dimensions, but you can apply whichever insets are
                // appropriate to your layout. You can also update the view padding if that's
                // more appropriate.
                val mlp = v.layoutParams as MarginLayoutParams
                mlp.leftMargin = ins.left
                mlp.bottomMargin = ins.bottom
                mlp.rightMargin = ins.right
                mlp.topMargin = ins.top
                v.layoutParams = mlp

                // Return CONSUMED if you don't want want the window insets to keep passing
                // down to descendant views.
                WindowInsetsCompat.CONSUMED
            }

            productsList.layoutManager = LinearLayoutManager(this)
            productsList.setItemViewCacheSize(20)
            productsList.adapter = productsAdapter
            registerForContextMenu(productsList)
            val searchQueryLabel: AppCompatTextView = findViewById(R.id.searchQueryLabel)
            searchQueryLabel.text = searchQueryLabel.text.toString().replace(
                "_QUERY_",
                query ?: ""
            )
        } catch (e: Exception) {
            GenericUtils.showErrorDialog(supportFragmentManager, e)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val position: Int
        try {
            position = productsAdapter.position
        } catch (e: Exception) {
            e.localizedMessage?.let { Log.d("ContextMenu-EXCEPTION: ", it) }
            return super.onContextItemSelected(item)
        }
        val args: Bundle
        when (item.itemId) {
            (R.id.changePicProductCM) -> {
                // change product of product
                args = Bundle()
                args.putString(
                    GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_CALL_SRC,
                    products[position].name
                )
                args.putString(
                    GenericUtils.IntentBundleKeys.CHANGE_PICTURE_DIALOG_FRAGMENT_TABLE_SRC,
                    LocalDB.TABLE_NAME_PRODUCTS
                )
                val changePictureDF = ChangePictureDialogFragment.newInstance()
                changePictureDF.arguments = args
                changePictureDF.show(supportFragmentManager, "changePictureOfProduct")
            }

            (R.id.changeExpDateProductCM) -> {
                // change expiration date of product
                args = Bundle()
                args.putString(
                    GenericUtils.IntentBundleKeys.DATE_PICKER_DIALOG_FRAGMENT_CALL_SRC,
                    products[position].name
                )
                val datePicker = DatePickerDialogFragment()
                datePicker.arguments = args
                datePicker.show(supportFragmentManager, "datePicker")
            }

            (R.id.changeBarcodeProductCM) -> {
                // change barcode of product
                args = Bundle()
                args.putString(
                    GenericUtils.IntentBundleKeys.CHANGE_PRODUCT_BARCODE_DIALOG_FRAGMENT_CALL_SRC,
                    products[position].name
                )
                val changeProductBarcodeDF = ChangeProductBarcodeDialogFragment.newInstance()
                changeProductBarcodeDF.arguments = args
                changeProductBarcodeDF.show(supportFragmentManager, "changeBarcodeOfProduct")
            }

            (R.id.changeNameProductCM) -> {
                args = Bundle()
                val product = products[position]
                args.putInt(
                    GenericUtils.IntentBundleKeys.CHANGE_NAME_DIALOG_FRAGMENT_CALL_SRC,
                    product.id
                )
                args.putString(
                    GenericUtils.IntentBundleKeys.CHANGE_NAME_DIALOG_FRAGMENT_CURRENT_NAME_KEY,
                    product.name
                )
                val changeNameDialog = ChangeNameDialogFragment.newInstance()
                changeNameDialog.arguments = args
                changeNameDialog.show(supportFragmentManager, "changeNameOfProduct")
            }

            (R.id.deleteProductCM) -> {
                //productsAdapter.remove(position)
                //val prod = products[position]
                val prod =
                    (productsAdapter.removePeek(position) as AdSupportingAdapter.Item.NormalItem<Product>).value
                RemoveProductTask(
                    this,
                    prod,
                    createResultListener({ products.remove(prod) })
                ).execute(true)
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onFinishChangePictureDialog(newImage: Bitmap, fromWhat: String) {
        val dataFolder = getDir(getString(R.string.app_data_folder), MODE_PRIVATE)
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFl = File(dataFolder, "private-$timeStamp-$fromWhat.png")
        try {
            val fl = FileOutputStream(imageFl)
            newImage.compress(Bitmap.CompressFormat.PNG, 100, fl)
            ChangeProductPictureTask(
                this,
                imageFl.absolutePath,
                fromWhat,
                createResultListener({
                    runOnUiThread {
                        var i = 0
                        for ((_, name) in products) {
                            if (name == fromWhat) {
                                products[i].picture = Paths.get(imageFl.absolutePath)
                                productsAdapter.notifyItemChanged(i)
                                break
                            }
                            i++
                        }
                    }
                })
            ).execute(true)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onFinishChangeProductBarcodeDialog(newBarcode: String, fromWhat: String) {
        ChangeProductBarcodeTask(
            this,
            newBarcode,
            fromWhat,
            createResultListener({
                runOnUiThread {
                    var i = 0
                    for ((_, name) in products) {
                        if (name == fromWhat) {
                            products[i].barcode = newBarcode
                            productsAdapter.replaceAt(i, AdSupportingAdapter.Item.NormalItem(products[i]))
                            productsAdapter.notifyItemChanged(i)
                            break
                        }
                        i++
                    }
                }
            })
        ).execute(true)
    }

    override fun onFinishChangeNameListener(newName: String, fromWhat: Int) {
        ChangeProductNameTask(
            this,
            newName,
            fromWhat,
            createResultListener({
                runOnUiThread {
                    for (i in products.indices) {
                        if (products[i].id == fromWhat) {
                            products[i].name = newName
                            productsAdapter.notifyItemChanged(i)
                            break
                        }
                    }
                }
            })
        ).execute(true)
    }

    override fun onStart() {
        super.onStart()
        if (BuildConfig.IS_MONET) productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.IS_MONET) productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        if (BuildConfig.IS_MONET) productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        if (BuildConfig.IS_MONET) productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroy() {
        if (BuildConfig.IS_MONET) productsAdapter.handleAdsForLifecycleState(Lifecycle.Event.ON_CREATE)
        super.onDestroy()
    }

}