package com.timkom.pitafl.activity

import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.timkom.pitafl.AdsConsentManager
import com.timkom.pitafl.BuildConfig
import com.timkom.pitafl.R
import com.timkom.pitafl.handleAdsForLifecycleState
import com.timkom.pitafl.initializeAds
import com.timkom.pitafl.util.GenericUtils
import com.timkom.pitafl.util.tryMultiCatch

class AboutActivity : AppCompatActivity() {

    private lateinit var aboutTextTV: TextView
    private lateinit var adsConsentManager: AdsConsentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        setSupportActionBar(findViewById(R.id.help_top_appbar))

        supportActionBar?.apply {
            this@apply.setDisplayHomeAsUpEnabled(true)
            this@apply.setTitle(R.string.about_activity_title)
        }

        tryMultiCatch(
            runThis = {
                adsConsentManager = AdsConsentManager.getInstance(applicationContext)
                // there is a bug in Google AdMob on Android 15, so don't display ads
                if (adsConsentManager.canRequestAds) {
                    if (GenericUtils.canUseAds(this@AboutActivity)) {
                        initializeAds()
                        handleAdsForLifecycleState(Lifecycle.Event.ON_CREATE)
                    }
                }

                val pkgInfo = applicationContext.packageManager.getPackageInfo(
                    applicationContext.packageName, 0
                )

                aboutTextTV = findViewById(R.id.aboutText)
                val txt = Html.fromHtml(
                    "<h1>${getString(R.string.app_name)}</h1>" +
                            "<p><b>App Full Name: ${getString(R.string.app_name)}</b>" +
                            "<br/><b>App Short Name: PITAFL</b>" +
                            "<br/>App Version: ${pkgInfo.versionName}" +
                            "<br/>Database Version: ${BuildConfig.DB_VERSION}</p>" +
                            "<br/><h3>About PITAFL</h3>" +
                            "<p>PITAFL is a simple pantry managing app with ease of use in mind. " +
                            "PITAFL is also open source and distributed under the " +
                            "<a href=\"https://gitlab.com/timoskom/pitafl/-/blob/master/LICENSE?ref_type=heads\">MIT License</a> " +
                            "(You can access the GitLab repository from " +
                            "<a href=\"https://gitlab.com/timoskom/pitafl\">here</a>).</p>" +
                            "<br/><h5>Disclaimer</h5>" +
                            "<p>All UI icons are from the android studio's internal <i>Resource Manager</i>" +
                            " and from the site of the " +
                            "<a href=\"https://fonts.google.com/icons\">Google Fonts</a>." +
                            "<br/>All other images/icons are supplied from " +
                            "<a href=\"https://www.svgrepo.com/\">SVG REPO</a>.</p>" +
                            "<p><a href=\"https://timoskom.gitlab.io/pitafl-pages/privacy.html\"><b>Privacy Policy</b></a></p>",
                    Html.FROM_HTML_MODE_LEGACY
                )
                aboutTextTV.text = txt
                aboutTextTV.movementMethod = LinkMovementMethod.getInstance()
            },
            onCatch = {
                findViewById<View>(R.id.aboutActivityConstraintLayout).setBackgroundColor(getColor(R.color.design_default_color_error))
                aboutTextTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22f)
                aboutTextTV.setTextColor(getColor(R.color.teal_200))
                aboutTextTV.text = "AN INTERNAL ERROR OCCURRED!"
            },
            PackageManager.NameNotFoundException::class,
            NullPointerException::class
        )
    }

    override fun onStart() {
        super.onStart()
        if (BuildConfig.IS_MONET) handleAdsForLifecycleState(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.IS_MONET) handleAdsForLifecycleState(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        if (BuildConfig.IS_MONET) handleAdsForLifecycleState(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        if (BuildConfig.IS_MONET) handleAdsForLifecycleState(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroy() {
        if (BuildConfig.IS_MONET) handleAdsForLifecycleState(Lifecycle.Event.ON_DESTROY)
        super.onDestroy()
    }

}
