package com.timkom.pitafl

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationChannelGroupCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.Data
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.timkom.pitafl.data.room.Product
import com.timkom.pitafl.data.room.util.toData
import com.timkom.pitafl.db.room.LocalDB.Companion.getInstance
import com.timkom.pitafl.db.room.getExpiresSoonUpdated
import com.timkom.pitafl.util.GenericUtils
import java.text.SimpleDateFormat
import java.time.Duration
import java.util.Locale
import java.util.Random
import java.util.UUID

class ProductsWatcherWorker(
    private val context: Context,
    workerParams: WorkerParameters
) : Worker(context, workerParams) {

    private val mNotifyIfDays = inputData.getInt("NOTIFY_IF_DAYS", 5)

    @SuppressLint("MissingPermission")
    override fun doWork(): Result {
        if (GenericUtils.isDebug(false)) Log.d("WORKER", "HELLO")

        val db = getInstance(context)
        val products = ArrayList<Product>(
            db.productDao().getExpiresSoonUpdated(mNotifyIfDays).map { it.toData() }
        )


        if (products.isNotEmpty()) {
            val channelGroup = NotificationChannelGroupCompat.Builder(NOTIFICATION_GROUP_ID)
                .setDescription("The Main PITAFL Notification Channel Group")
                .setName("PITAFL Channel Group")
                .build()

            val notificationChannelCompat = NotificationChannelCompat.Builder(
                NOTIFICATION_CHANNEL_ID, NotificationManagerCompat.IMPORTANCE_DEFAULT
            )
                .setDescription("The Main PITAFL Notification Channel")
                .setName("PITAFL Channel")
                .setGroup(NOTIFICATION_GROUP_ID)
                .build()

            val notificationManagerCompat = NotificationManagerCompat.from(context)
            notificationManagerCompat.createNotificationChannelGroup(channelGroup)
            notificationManagerCompat.createNotificationChannel(notificationChannelCompat)

            val idGen = Random()
            for ((_, name, _, _, expirationDate) in products) {
                if (GenericUtils.isDebug(true)) {
                    Log.e("WORKER", expirationDate.time.toString())
                }

                val builder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setContentTitle("Expires Soon")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setGroup(NOTIFICATION_GROUP_KEY)
                    .setContentText(name)
                    .setStyle(
                        NotificationCompat.BigTextStyle().bigText(
                            "Expires at: " + SimpleDateFormat(
                                "EEEE dd/MM/yyyy",
                                Locale.getDefault()
                            ).format(expirationDate.time)
                        )
                    )
                    .setCategory(NotificationCompat.CATEGORY_REMINDER)
                    .setContentInfo("Expires at: " + expirationDate.time.toString())
                notificationManagerCompat.notify(idGen.nextInt(), builder.build())
            }
        } else {
            Log.d("FROM[ProductsWatcherWorker]", "None product is close to expiring")
        }

        if (GenericUtils.isDebug(false)) {
            for ((_, _, _, _, _, _, _, _, expirationMark) in products) {
                Log.d("WORKER", expirationMark.toString())
            }
        }

        return Result.success()
    }

    companion object {
        const val PRODUCTS_WATCHER_WORKER_TAG: String = "PRODUCTS_WATCHER_WORKER"
        private const val NOTIFICATION_GROUP_KEY = "PITAFL-NOTIFICATION-GROUP-KEY"
        private val NOTIFICATION_GROUP_ID = UUID.randomUUID().toString()
        private val NOTIFICATION_CHANNEL_ID = UUID.randomUUID().toString()

        @JvmStatic
        fun create(ctx: Context, duration: Duration) {
            val workManager = WorkManager.getInstance(ctx)
            val request: PeriodicWorkRequest = PeriodicWorkRequest.Builder(ProductsWatcherWorker::class.java, duration)
                .addTag(PRODUCTS_WATCHER_WORKER_TAG)
                .setInputData(
                    Data.Builder().putInt(
                        "NOTIFY_IF_DAYS",
                        ctx.getSharedPreferences(
                            ctx.getString(R.string.preferences_file),
                            Context.MODE_PRIVATE
                        ).getString(
                            ctx.getString(R.string.expiration_notifications_key),
                            ctx.getString(R.string.expiration_notifications_default)
                        )!!.toInt()
                    ).build()
                ).build()
            workManager.enqueue(request)
        }
    }

}