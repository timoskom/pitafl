package com.timkom.pitafl

import android.app.Activity
import android.content.Context
import com.google.android.ump.ConsentForm
import com.google.android.ump.FormError
import com.timkom.pitafl.activity.MainActivity

sealed interface AdsConsentManager {
    fun interface OnConsentGatheringCompleteListener {
        fun consentGatheringComplete(error: FormError?)
    }

    val canRequestAds: Boolean

    val isPrivacyOptionsRequired: Boolean

    fun gatherConsent(
        activity: Activity,
        onConsentGatheringCompleteListener: OnConsentGatheringCompleteListener,
    )

    fun showPrivacyOptionsForm(
        activity: MainActivity,
        onConsentFormDismissedListener: ConsentForm.OnConsentFormDismissedListener,
    )

    companion object {
        private var instance: AdsConsentManager? = null

        @JvmStatic
        fun getInstance(context: Context): AdsConsentManager {
            return instance ?: synchronized(this) {
                instance ?: provideInstance(context).also { instance = it }
            }
        }
    }
}