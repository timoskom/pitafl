package com.timkom.pitafl

import android.content.Context
import android.view.View
import androidx.activity.ComponentActivity
import androidx.annotation.UiThread
import androidx.lifecycle.Lifecycle
import com.timkom.pitafl.activity.AboutActivity
import com.timkom.pitafl.activity.MainActivity
import com.timkom.pitafl.activity.ProductsActivity
import com.timkom.pitafl.adapter.AdSupportingAdapter
import com.timkom.pitafl.adapter.AdSupportingAdapter.Item
import com.timkom.pitafl.adapter.AdSupportingAdapter.ViewHolder
import com.timkom.pitafl.adapter.ProductsAdapter
import com.timkom.pitafl.data.room.Product
import kotlin.reflect.KClass

@UiThread
fun MainActivity.initializeAds() { }

fun ComponentActivity.handleAdsForLifecycleState(event: Lifecycle.Event) { }

@UiThread
fun AboutActivity.initializeAds() { }

@UiThread
fun ProductsActivity.initializeAds() { }

fun ProductsAdapter.bindAdView(holder: ViewHolder.AdViewHolder, item: Item.AdItem<Product>) { }

fun <T, VW: ViewHolder, A : AdSupportingAdapter<T, VW>> AdSupportingAdapter.Companion.createAdView(
    context: Context,
    forAdapter: KClass<A>
): Item.AdItem<T> {
    return Item.AdItem(View(context))
}

fun <T, VW: ViewHolder> AdSupportingAdapter<T, VW>.handleAdsForLifecycleStateImpl(event: Lifecycle.Event, view: View) { }