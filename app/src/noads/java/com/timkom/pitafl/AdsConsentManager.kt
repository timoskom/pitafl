@file:Suppress("UNUSED_PARAMETER")

package com.timkom.pitafl

import android.app.Activity
import android.content.Context
import com.google.android.ump.ConsentForm
import com.timkom.pitafl.activity.MainActivity

private class AdsConsentManagerImpl(context: Context) : AdsConsentManager {

    override val canRequestAds: Boolean
        get() = false
    override val isPrivacyOptionsRequired: Boolean
        get() = false

    override fun gatherConsent(
        activity: Activity,
        onConsentGatheringCompleteListener: AdsConsentManager.OnConsentGatheringCompleteListener
    ) { }

    override fun showPrivacyOptionsForm(
        activity: MainActivity,
        onConsentFormDismissedListener: ConsentForm.OnConsentFormDismissedListener
    ) { }

}

internal fun AdsConsentManager.Companion.provideInstance(context: Context): AdsConsentManager {
    return AdsConsentManagerImpl(context)
}