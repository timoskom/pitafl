This folder can contain any pair of properties and jks files with the same name to be used for
the app signing process.

The properties file will have the bellow structure:

```properties
debugKeyAlias=DEBUG_KEY_ALIAS
debugKeyPassword=DEBUG_KEY_PASSWORD
debugStorePassword=DEBUG_STORE_PASSWORD
releaseKeyAlias=RELEASE_KEY_ALIAS
releaseKeyPassword=RELEASE_KEY_PASSWORD
releaseStorePassword=RELEASE_STORE_PASSWORD
sameStoreFile=true
```

If `sameStoreFile` is `false` it will try to read two files, one with suffix "-release" and one
with suffix "-debug". If no `sameStoreFile` is specified it will be assumed as `true` by default.
To get the value of `sameStoreFile` Groovy's
[String.toBoolean()](
https://docs.groovy-lang.org/docs/groovy-3.0.0/html/groovy-jdk/java/lang/String.html#toBoolean())
method will be used, so any value acceptable (for `true` value) by that method is also acceptable
in this property.
