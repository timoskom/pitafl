# Pantry, Is There Any Food Left? (PITAFL)
## Pantry Manager
**PITAFL is an android app for pantry management & smart (food) shopping list creation.**
## Pronunciation
**paɪtɑːfl**
(GR: πάιταφλ)
## About
This app was created as a research objective for my academic thesis, titled ***"Mobile app for food warehouse management and smart shopping list"***
(GR: Αυτή η εφαρμογή δημιουργήθηκε ως ερευνητικός στόχος για την ακαδημαϊκή μου διατριβή, με τίτλο ***"Εφαρμογή κινητής συσκευής για διαχείριση
αποθήκης τροφίμων και δημιουργία έξυπνης
λίστας για ψώνια"***)

It's a native android application that was originally written in Java, but at the time of writing (03/02/2025), most of its codebase has transitioned to Kotlin, with the goal of making it a Kotlin only codebase.

PITAFL is available for download in Google Play Store. As of 08 of January of 2025, PITAFL includes ads (with Google AdMob) in its _normal_ version on Google Play Store. For an ads-free version you can get the noads variation from the pipeline artifacts.

Also, if you really like what I'm "baking" you could also _buy me a coffee_ (or a pizza...I don't mind!🤭🥱).

## Get the app
[<img src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png"  
alt="Get it on Google Play" height="100"/> ](https://play.google.com/store/apps/details?id=com.timkom.pitafl&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)
## Buy me a coffee
[<img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png"
alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" >](https://www.buymeacoffee.com/timos.komni)